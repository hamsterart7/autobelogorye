/* global $, i18n */

import 'formdata-polyfill';
import 'svgxuse';
import objectFitImages from 'object-fit-images';
import './lazyload.js';
import './jquery.fancybox.min.js';
import Swiper from 'swiper';
import insertionQ from 'insertion-query';
import 'simplebar';
import SimpleBar from 'simplebar';
import Cookies from 'js-cookie';
import anime from 'animejs';
import animate from './animate';
import IMask from 'imask';

$(document).ready(function() {
	const $container = $('.content');

	/* Add hover class to clickable elements if not mobile */
	if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
		$('a').addClass('has-hover');
		$('button').addClass('has-hover');
	}

	/* Hide default select if not mobile */
	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
		$('.select__input').addClass('hidden');
	}

	/* Check Safari*/
	if (!!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/)) {
		$('body').addClass('safari');
	}

	/* Check iOS*/
	if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
		$('body').addClass('ios');
	}

	/* Disable take match button */
	const hash = window.location.hash;
	if (hash.includes('taken')) {
		$('.js-take-match').prop('disabled', true);
	}

	/* Simple bar */
	if ($('.messages').length > 0) {
		const simpleBar = new SimpleBar(document.querySelector('.messages'), {});
	}

	/* Parallax coins */
	if ($('.coin').length > 0) {
		function isInViewport(node) {
			let rect = node.getBoundingClientRect();
			return (
				(rect.height > 0 || rect.width > 0) &&
				rect.bottom >= 0 &&
				rect.right >= 0 &&
				rect.top <= (window.innerHeight || document.documentElement.clientHeight) &&
				rect.left <= (window.innerWidth || document.documentElement.clientWidth)
			);
		}

		function parallaxCoin(scrolled, coin, r) {
			let initY = coin.offset().top;
			let height = coin.height();

			let visible = isInViewport(coin.get(0));
			if (visible) {
				let diff = scrolled - initY;
				let ratio = Math.round((diff / height) * 100);
				coin.css('top', parseInt(-(ratio * r)) + 'px');
			}
		}

		$(window).scroll(function() {
			let scrolled = $(window).scrollTop();
			parallaxCoin(scrolled, $('.coin-1'), 0.5);
			parallaxCoin(scrolled, $('.coin-2'), 0.3);
		});
	}

	/* Sticky Header */
	function stickyHeader() {
		const scroll = $(window).scrollTop();
		if (scroll >= 50) {
			$('.header').addClass('header--fixed');
		} else {
			$('.header').removeClass('header--fixed');
		}
	}

	stickyHeader();
	$(window).scroll(function() {
		stickyHeader();
	});

	/* Init Selects */
	$('#js-select-sample').select2({
		width: 'resolve',
		dropdownParent: $('.js-select-sample-holder'),
		minimumResultsForSearch: Infinity,
	});
	$('#js-select-server').select2({
		width: 'resolve',
		dropdownParent: $('.js-select-server-holder'),
		minimumResultsForSearch: Infinity,
	});
	$('#js-select-platform').select2({
		width: 'resolve',
		dropdownParent: $('.js-select-platform-holder'),
		minimumResultsForSearch: Infinity,
	});
	$('#js-select-type').select2({
		width: 'resolve',
		dropdownParent: $('.js-select-type-holder'),
		minimumResultsForSearch: Infinity,
	});
	$('#js-select-gamers').select2({
		width: 'resolve',
		dropdownParent: $('.js-select-gamers-holder'),
		minimumResultsForSearch: Infinity,
	});
	$('#js-select-team').select2({
		width: 'resolve',
		dropdownParent: $('.js-select-team-holder'),
		minimumResultsForSearch: Infinity,
	});
	$('#js-select-contribution').select2({
		width: 'resolve',
		dropdownParent: $('.js-select-contribution-holder'),
		minimumResultsForSearch: Infinity,
	});
	$('#js-select-type-popup').select2({
		width: 'resolve',
		dropdownParent: $('.js-select-type-popup-holder'),
		minimumResultsForSearch: Infinity,
	});
	$('#js-select-match-up').select2({
		width: 'resolve',
		dropdownParent: $('.js-select-match-up-holder'),
		minimumResultsForSearch: Infinity,
	});
	$('#js-select-server-popup').select2({
		width: 'resolve',
		dropdownParent: $('.js-select-server-popup-holder'),
		minimumResultsForSearch: Infinity,
	});
	$('#js-select-platform-popup').select2({
		width: 'resolve',
		dropdownParent: $('.js-select-platform-popup-holder'),
		minimumResultsForSearch: Infinity,
	});
	$('#js-select-reason').select2({
		width: 'resolve',
		dropdownParent: $('.js-select-reason-holder'),
		minimumResultsForSearch: Infinity,
	});
	$('#js-select-team-2').select2({
		width: 'resolve',
		dropdownParent: $('.js-select-team-2-holder'),
		minimumResultsForSearch: Infinity,
	});
	$('#js-select-result').select2({
		width: 'resolve',
		dropdownParent: $('.js-select-result-holder'),
		minimumResultsForSearch: Infinity,
	});
	$('#js-select-members').select2({
		width: 'resolve',
		dropdownParent: $('.js-select-members-holder'),
		minimumResultsForSearch: Infinity,
	});

	/* Emoji */
	$(function() {
		// Initializes and creates emoji set from sprite sheet
		window.emojiPicker = new EmojiPicker({
			emojiable_selector: '[data-emojiable=true]',
			assetsPath: './styles/images/lib/',
			popupButtonClasses: 'fa fa-smile-o',
		});
		window.emojiPicker.discover();
	});

	/* Simplebar */
	insertionQ('.select2-dropdown').every(function(element) {
		new SimpleBar($('.select2-results__options')[0], {autoHide: false});
	});
	insertionQ('.emoji-wysiwyg-editor').every(function(element) {
		new SimpleBar($('.emoji-wysiwyg-editor')[0], {autoHide: false});
	});
	insertionQ('.emoji-menu').every(function(element) {
		new SimpleBar($('.emoji-menu .emoji-items-wrap')[0], {autoHide: false});
	});

	/* Init Datepickers */
	let calendar = new flatpickr('.js-calendar-from', {
		disableMobile: 'true',
		altInput: true,
		altFormat: 'j M yy',
		locale: 'ru',
		appendTo: document.getElementById('js-calendar-from-holder'),
	});
	let date = new flatpickr('.js-calendar-filter', {
		enableTime: true,
		disableMobile: 'true',
		altInput: true,
		altFormat: 'd.m.Y H:i',
		locale: 'ru',
		appendTo: document.getElementById('js-calendar-filter-holder'),
	});
	let cart = new flatpickr('.js-calendar-cart', {
		disableMobile: 'true',
		altInput: true,
		altFormat: 'm/yy',
		locale: 'ru',
		appendTo: document.getElementById('js-calendar-cart-holder'),
	});
	let cart2 = new flatpickr('.js-calendar-cart2', {
		disableMobile: 'true',
		altInput: true,
		altFormat: 'm/yy',
		locale: 'ru',
		appendTo: document.getElementById('js-calendar-cart2-holder'),
	});

	/* Mask */
	$('.js-cvv-field').each((i, el) => {
		new IMask(el, {
			mask: '000',
		});
	});
	$('.js-cart-num-field').each((i, el) => {
		new IMask(el, {
			mask: '0000 0000 0000 0000',
		});
	});
	$('.js-money-field').each((i, el) => {
		new IMask(el, {
			mask: '$num',
			blocks: {
				num: {
					mask: Number,
					thousandsSeparator: ' ',
				},
			},
		});
	});
	$('.js-phone-field').each((i, el) => {
		new IMask(el, {
			mask: i18n.masks.tel,
		});
	});

	/* Progress */
	function startProgress(duration, timeBox, progressBar, progressBox) {
		let timer = duration,
			minutes,
			seconds;
		const interval = setInterval(function() {
			minutes = parseInt(timer / 60, 10);
			seconds = parseInt(timer % 60, 10);

			minutes = minutes < 10 ? '0' + minutes : minutes;
			seconds = seconds < 10 ? '0' + seconds : seconds;
			timeBox.text(minutes + ':' + seconds);
			let w = (100 * timer) / duration;
			progressBar.css('width', w + '%');
			if (minutes < 10 && minutes > 3) {
				progressBox.removeClass('progress--wait');
				progressBox.addClass('progress--warning');
			}
			if (minutes <= 3) {
				progressBox.removeClass('progress--warning');
				progressBox.addClass('progress--out');
			}
			if (--timer < 0) {
				timer = 0;
				clearTimeout(interval);
				progressBox.trigger('progress:finished');
			}
		}, 1000);
	}

	let _day = 86400000;

	function startProgressDay(duration, timeBox, progressBar, progressBox) {
		duration = Date.parse(duration);
		let end = new Date(duration);
		console.log(duration);
		console.log(end);
		let timer = end - new Date();
		const interval = setInterval(function() {
			let now = new Date();
			let distance = end - now;
			if (distance < 0) {
				timer = 0;
				clearTimeout(interval);
			}
			let days = Math.floor(distance / _day);
			timeBox.text(days + ' дней');
			let w = (100 * distance) / timer;
			progressBar.css('width', w + '%');
			if (--timer < 0) {
				clearInterval(timer);
				progressBox.trigger('progress:finished');
			}
		}, 1000);
	}

	const $progress = $('.progress');
	$progress.each((i, el) => {
		const timeInputValue = $(el)
			.find('.progress-value')
			.val();
		let timeBox = $(el).find('.progress__time');
		let progressBar = $(el).find('.progress__bar');
		if ($progress.hasClass('progress-day')) {
			startProgressDay(timeInputValue, timeBox, progressBar, $(el));
		} else if (!$progress.hasClass('progress-day')) {
			startProgress(timeInputValue, timeBox, progressBar, $(el));
		}
	});

	/* Timer */
	function startTimer(duration, timeBox, timerBox) {
		let timer = duration,
			minutes;
		let timerPath = timerBox.find('.timer__remaining');
		let fightBtnHolder = timerBox.parents('.fight').find('.fight-btn');
		let fightBtn = fightBtnHolder.find('button');
		const interval = setInterval(function() {
			minutes = Math.ceil(timer / 60);
			timeBox.text(minutes);
			const rawTimeFraction = timer / duration;
			const timeFraction = rawTimeFraction - (1 / duration) * (1 - rawTimeFraction);
			const circleDasharray = `${(timeFraction * 283).toFixed(0)} 283`;
			timerPath.attr('stroke-dasharray', circleDasharray);
			if (minutes >= 10) {
				timerBox.addClass('timer--wait');
			}
			if (minutes < 10 && minutes > 3) {
				timerBox.removeClass('timer--wait');
				timerBox.addClass('timer--warning');
			}
			if (minutes <= 3) {
				timerBox.removeClass('timer--wait');
				timerBox.removeClass('timer--warning');
				timerBox.addClass('timer--out');
			}
			if (--timer < 0) {
				timer = 0;
				timerPath.attr('stroke-dasharray', '283 283');
				fightBtn.prop('disabled', true);
				clearTimeout(interval);
				timerBox.trigger('timer:finished');
			}
		}, 1000);
	}

	const $timer = $('.timer');
	$timer.each((i, el) => {
		const timeInputValue = $(el)
			.find('.timer-value')
			.val();
		let timeBox = $(el).find('.timer__time');
		startTimer(timeInputValue, timeBox, $(el));
	});

	/* Sliders */
	if ($container.find('.h-slider').length > 0) {
		$container.find('.h-slider').each((i, el) => {
			const $slider = $(el);
			new Swiper($slider.find('.h-slider__container').get(0), {
				direction: 'horizontal',
				slidesPerView: 'auto',
				autoplay: true,
				speed: 3000,
			});
		});
	}

	/* Upload Help Form Files */
	function FileListItem(a) {
		a = [].slice.call(Array.isArray(a) ? a : arguments);
		for (var c, b = (c = a.length), d = !0; b-- && d; ) d = a[b] instanceof File;
		if (!d) throw new TypeError('expected argument to FileList is File or array of File objects');
		for (b = new ClipboardEvent('').clipboardData || new DataTransfer(); c--; ) b.items.add(a[c]);
		return b.files;
	}

	let fl = [];
	const $inp = $('.upload-file');

	$('.upload-file').on('click', function(e) {
		e.target.value = null;
	});

	function uploadFiles(files) {
		const $list = $inp.parents('.attach').find('.file-list');
		$('.file-list').text('');
		for (let i = 0; i < files.length; i++) {
			let uploadedDiv = $('<div>', {class: 'file'}).appendTo($list);
			$('<div class="file__name">' + files[i].name + '</div>').appendTo(uploadedDiv);
			$('<button>', {class: 'file__delete-btn', type: 'button'}).appendTo(uploadedDiv);
		}
		$('.file').each(function(i, cont) {
			$(cont).attr('data-ind', i);
		});
	}

	$('.upload-file').on('change', function(e) {
		if (fl.length > 0) {
			uploadFiles(this.files);
			fl = $.merge($.merge([], fl), this.files);
		} else {
			fl = Array.from(this.files);
			uploadFiles(this.files);
		}
		$inp.files = new FileListItem(fl);
		$inp.prop('files', $inp.files);
	});
	$(document).on('click', '.file__delete-btn', function(e) {
		e.preventDefault();
		const $del = $(e.currentTarget);
		const $file = $del.parents('.file');
		const ind = $file.attr('data-ind');
		$file.remove();
		let val = fl[ind];
		fl = fl.filter((item) => item !== val);
		$('.uploaded-image').each(function(i, cont) {
			if (i > ind) {
				$(cont).attr('data-ind', i - 1);
			}
		});
		$inp.files = new FileListItem(fl);
		$inp.prop('files', $inp.files);
		$('.file-list').text('Файл не выбран');
	});
});

$(() => {
	/* Notification dropdown */
	$('.js-notf-btn').click(function() {
		$('.notification-dropdown').slideToggle();
	});

	/* Notification mobile */
	$('.js-mobile-notf-btn').click(function() {
		$('.header__item-notification').toggleClass('header__item-notification--active');
		$('.profile-dropdown__item:not(.profile-dropdown__item--mobile)').toggleClass('profile-dropdown__item--hide');
	});

	/* Delete notification */
	$(document).on('click', '.js-delete-notf-btn', function(e) {
		e.preventDefault();
		const $btn = $(e.currentTarget);
		const $notf = $btn.parents('.notification-dropdown__item');
		$notf.slideUp();
		$notf.addClass('notification-hidden');
		$notf.css('border-bottom', '0');

		if ($('.notification-hidden').length === $('.notification-dropdown__item').length) {
			$('.notification-btn').removeClass('notification-btn--has-notf');
		}
	});
	$('.notification-dropdown__item').swipe({
		swipeLeft: function(event, direction, distance, duration, fingerCount, fingerData) {
			$(this).slideUp();
		},
	});

	/* Profile dropdown */
	$('.js-profile-btn').click(function() {
		if ($('.profile').hasClass('profile--active')) {
			$('.profile-dropdown').slideUp();
			setTimeout(function() {
				$('.profile-btn').removeClass('profile-btn--active');
				$('.profile').removeClass('profile--active');
			}, 500);
		} else {
			$('.profile-btn').addClass('profile-btn--active');
			$('.profile').addClass('profile--active');
			setTimeout(function() {
				$('.profile-dropdown').slideDown();
			}, 500);
		}
	});

	/* Mobile menu */
	$('.js-menu-btn').click(function() {
		$('.header__box').toggleClass('header__box--active');
		$('body').toggleClass('locked');
	});
	$('.header__box').swipe({
		swipeRight: function(event, direction, distance, duration, fingerCount, fingerData) {
			$('.header__box').removeClass('header__box--active');
			$('body').removeClass('locked');
		},
	});
	$(document).mouseup((e) => {
		if (!$('.header__box').is(e.target) && $('.header__box').has(e.target).length === 0) {
			$('.header__box').removeClass('header__box--active');
			$('body').removeClass('locked');
		}
	});

	/* Filter mobile */
	$('.js-filter-btn').click(function() {
		$('.filter__inner').toggleClass('filter__inner--active');
		$('body').toggleClass('locked');
	});

	/* Registration */
	$('.js-politics-check').change(function() {
		if ($('.js-politics-check').is(':checked')) {
			$('.js-reg-btn').prop('disabled', false);
		} else {
			$('.js-reg-btn').prop('disabled', true);
		}
	});

	/* Close popup */
	$('.js-close-popup-btn').click(function(e) {
		const $btn = $(e.currentTarget);
		const $popup = $btn.parents('.popup-holder');
		$popup.removeClass('popup-holder--active');
		$('body').removeClass('locked');
	});
	$(document).mouseup((e) => {
		if (!$('.popup').is(e.target) && $('.popup').has(e.target).length === 0) {
			$('.popup-holder').removeClass('popup-holder--active');
			$('body').removeClass('locked');
		}
	});

	/* Open funds popup */
	$('.js-open-funds-btn').click(function(e) {
		e.preventDefault();
		$('.js-funds-popup-holder').addClass('popup-holder--active');
		$('body').addClass('locked');
	});

	/* Open admit defeat popup */
	$('.js-open-admit-defeat-btn').click(function(e) {
		e.preventDefault();
		$('.js-admit-defeat-popup-holder').addClass('popup-holder--active');
		$('body').addClass('locked');
	});

	/* Open cancel match popup */
	$('.js-open-cancel-match-btn').click(function(e) {
		e.preventDefault();
		$('.js-cancel-match-popup-holder').addClass('popup-holder--active');
		$('body').addClass('locked');
	});

	/* Open join match popup */
	$('.js-open-join-match-btn').click(function(e) {
		e.preventDefault();
		$('.js-join-match-popup-holder').addClass('popup-holder--active');
		$('body').addClass('locked');
	});

	/* Active join match btn */
	$('.js-match-check').change(function() {
		let checked = $('.popup-form .js-match-check:checked').length;
		let all = $('.popup-form .js-match-check').length;
		if (checked === all) {
			$('.js-submit-match').prop('disabled', false);
		} else {
			$('.js-submit-match').prop('disabled', true);
		}
	});

	/* Open private popup */
	$('.js-open-private-btn').click(function(e) {
		e.preventDefault();
		$('.js-private-match-popup-holder').addClass('popup-holder--active');
		$('.js-join-match-popup-holder').removeClass('popup-holder--active');
		$('body').addClass('locked');
	});

	/* Open choose team popup */
	$('.js-open-choose-team-btn').click(function(e) {
		e.preventDefault();
		$('.js-private-match-popup-holder').removeClass('popup-holder--active');
		$('.js-choose-team-popup-holder').addClass('popup-holder--active');
		$('body').addClass('locked');
	});

	/* Open complaint popup */
	$('.js-open-complaint-btn').click(function(e) {
		e.preventDefault();
		$('.js-complaint-popup-holder').addClass('popup-holder--active');
		$('body').addClass('locked');
	});

	/* Open complaint done popup */
	$('.js-open-complaint-done-btn').click(function(e) {
		e.preventDefault();
		$('.js-complaint-done-popup-holder').addClass('popup-holder--active');
		$('.js-complaint-popup-holder').removeClass('popup-holder--active');
	});

	/* Open create match popup */
	$('.js-open-create-match-btn').click(function(e) {
		e.preventDefault();
		$('.js-create-match-popup-holder').addClass('popup-holder--active');
		$('body').addClass('locked');
	});

	/* Choose team members */
	$('.js-member-one-radio').change(function() {
		$('.create-match-options').slideUp();
	});
	$('.js-member-multi-radio').change(function() {
		$('.create-match-options').slideDown();
	});

	/* Open fight popup */
	$('.js-open-create-fight-btn').click(function(e) {
		e.preventDefault();
		$('.js-create-fight-popup-holder').addClass('popup-holder--active');
		$('.js-create-match-popup-holder').removeClass('popup-holder--active');
	});

	/* Back to create match popup */
	$('.js-back-to-create-match-btn').click(function(e) {
		e.preventDefault();
		$('.js-create-fight-popup-holder').removeClass('popup-holder--active');
		$('.js-create-match-popup-holder').addClass('popup-holder--active');
	});

	/* Open rules popup */
	$('.js-open-rules-btn').click(function(e) {
		e.preventDefault();
		$('.js-rules-popup-holder').addClass('popup-holder--active');
		$('body').addClass('locked');
	});

	/* Open reject match popup */
	$('.js-open-reject-match-btn').click(function(e) {
		e.preventDefault();
		$('.js-reject-match-popup-holder').addClass('popup-holder--active');
		$('body').addClass('locked');
	});

	/* Activate finish btn  */
	function activateFinishMatchBtn() {
		let radioChecked = $('.popup-form .js-finish-radio:checked').length;
		let checked = $('.popup-form .js-finish-check:checked').length;
		let all = $('.popup-form .js-finish-check').length;
		if (checked === all && radioChecked === 1) {
			$('.js-confirm-finish-match-btn').prop('disabled', false);
		} else {
			$('.js-confirm-finish-match-btn').prop('disabled', true);
		}
	}

	$('.js-finish-check').change(function() {
		activateFinishMatchBtn();
	});
	$('.js-finish-radio').change(function() {
		activateFinishMatchBtn();
	});

	/* Open finish match popup */
	$('.js-open-finish-match-btn').click(function(e) {
		e.preventDefault();
		$('.js-finish-match-popup-holder').addClass('popup-holder--active');
		$('body').addClass('locked');
	});

	/* Open proof popup */
	$('.js-open-proof-btn').click(function(e) {
		e.preventDefault();
		$('.js-proof-popup-holder').addClass('popup-holder--active');
		$('body').addClass('locked');
	});

	/* Open create team popup */
	$('.js-open-create-team-btn').click(function(e) {
		e.preventDefault();
		$('.js-create-team-popup-holder').addClass('popup-holder--active');
		$('body').addClass('locked');
	});

	/* Open exclude member popup */
	$('.js-open-exclude-member-btn').click(function(e) {
		e.preventDefault();
		$('.js-exclude-member-popup-holder').addClass('popup-holder--active');
		$('body').addClass('locked');
	});

	/* Exit team popup */
	$('.js-open-exit-team-btn').click(function(e) {
		e.preventDefault();
		$('.js-exit-team-popup-holder').addClass('popup-holder--active');
		$('body').addClass('locked');
	});

	/* Open proof */
	$('.js-see-proof-btn').click(function(e) {
		e.preventDefault();
		$('.gamers').toggleClass('gamers--hidden');
		$('.match-status-box').toggleClass('match-status-box--hidden');
		$('.proof-box').toggleClass('proof-box--hidden');
	});

	/* Take match */
	$('.js-take-match').click(function() {
		const hash = 'taken';
		window.location.hash = hash;
	});

	/* Add team member */
	$('.js-add-member-btn').click(function(e) {
		e.preventDefault();
		const $btn = $(e.currentTarget);
		const $memberList = $btn.parents('.popup-form').find('.added-users');
		const $memberField = $btn.parents('.popup-form').find('.js-member-field');
		const $memberText = $memberField.val();
		console.log($memberText);
		if ($memberText.length > 0) {
			console.log($memberText.length > 0);
			let memberItem = $('<div class="added-users__item">').appendTo($memberList);
			let member = $('<div class="added-user">').appendTo(memberItem);
			let memberInput = $('<input type="hidden" value="' + $memberText + '">').appendTo(member);
			let memberName = $('<span>').appendTo(member);
			memberName.text($memberText);
			$('<button>', {class: 'added-user__btn', type: 'button'}).appendTo(member);
			$memberField.val('');
		}
	});

	/* Delete added member */
	$(document).on('click', '.added-user__btn', function(e) {
		e.preventDefault();
		const $del = $(e.currentTarget);
		const $member = $del.parents('.added-users__item');
		$member.remove();
	});

	/* Add new cart */
	$('.js-add-cart-btn').click(function(e) {
		e.preventDefault();
		const $btn = $(e.currentTarget);
		const $newCart = $btn.parents('.tab-content').find('.js-new-cart-form');
		const $sum = $btn.parents('.tab-content').find('.sum-box');
		const $carts = $btn.parents('.tab-content').find('.carts-box');
		$newCart.css('top', $carts.height() + 32);

		if ($newCart.css('display') === 'none') {
			$sum.css('margin-top', $newCart.height() + 32);
		} else {
			$sum.css('margin-top', 0);
		}
		$newCart.slideDown();
		$btn.css('display', 'none');
	});

	/* Close message bar */
	$(document).on('click', '.js-close-message-bar-btn', function(e) {
		const $btn = $(e.currentTarget);
		const $messageBar = $btn.parents('.message-bar');
		$messageBar.slideUp();
	});

	/* Activate promo cart */
	$('.js-activate-btn').click(function(e) {
		e.preventDefault();
		const $btn = $(e.currentTarget);
		const $btnIcon = $btn.find('svg');
		const $btntext = $btn.find('.btn__text');
		$btnIcon.css('display', 'inline-block');
		$btntext.text('Промокод активирован');
	});

	/* Tabs */
	$('.js-tab').click(function(e) {
		const $btn = $(e.currentTarget);
		const $id = $btn.attr('data-tab');
		$('.tab-content').removeClass('tab-content--active');
		$('.tab').removeClass('tab--active');
		$($id).addClass('tab-content--active');
		$btn.addClass('tab--active');
	});

	/* Copy */
	$('.js-copy-btn').click(function() {
		let $temp = $('<input>');
		$('body').append($temp);
		$temp.val($('.js-copy-text').text()).select();
		document.execCommand('copy');
		$('.js-copy-btn').addClass('copied');
		$temp.remove();
	});

	/* Cookies */
	const $notice = $('.cookies-notice');

	// Показываем элемент, если нет соответствующей куки
	if (Cookies.get('cookies-notice') === undefined) {
		$notice.prop('hidden', false);
	}

	$notice.find('.cookies-notice__close').on('click', () => {
		// Ставим куку
		Cookies.set('cookies-notice', true);
		// Анимируем закрытие
		anime({
			targets: $notice.get(0),
			opacity: 0,
			translateY: $notice.outerHeight(),
			duration: animate.duration.leave,
			easing: animate.easing.acceleration,
		}).finished.then(() => {
			// Удаляем элемент
			$notice.remove();
		});
	});

	// IE11 detection
	if (/MSIE/.test(window.navigator.userAgent) || /Trident/.test(window.navigator.userAgent)) {
		$('body').addClass('msie');
	}

	// Polyfills
	objectFitImages();

	// https://www.thecssninja.com/javascript/pointer-events-60fps
	let peTimer;
	$(window).on('scroll', () => {
		if (peTimer) {
			clearTimeout(peTimer);
			peTimer = undefined;
		}
		const $body = $('body');
		if (!$body.hasClass('disable-hover')) {
			$body.addClass('disable-hover');
		}
		peTimer = setTimeout(() => {
			$body.removeClass('disable-hover');
		}, 100);
	});
});

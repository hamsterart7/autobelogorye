/* global $ */

import anime from 'animejs';
import Cookies from 'js-cookie';
import animate from './../animate.js';

$(() => {
	const $notice = $('.cookies-notice');

	// Показываем элемент, если нет соответствующей куки
	if (Cookies.get('cookies-notice') === undefined) {
		$notice.prop('hidden', false);
	}

	$notice.find('.cookies-notice__close').on('click', () => {
		// Ставим куку
		Cookies.set('cookies-notice', true);
		// Анимируем закрытие
		anime({
			targets: $notice.get(0),
			opacity: 0,
			translateY: $notice.outerHeight(),
			duration: animate.duration.leave,
			easing: animate.easing.acceleration,
		}).finished.then(() => {
			// Удаляем элемент
			$notice.remove();
		});
	});
});

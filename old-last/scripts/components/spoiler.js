/* global $ */

import anime from 'animejs';
import animate from './../animate.js';

$(() => {
	$('.spoiler').each((i, el) => {
		const $container = $(el);
		const $wrapper = $container.find('.spoiler__wrapper');
		const $show = $container.find('.spoiler__show');
		$wrapper.removeClass('spoiler__wrapper_active');
		if ($wrapper.outerHeight() > window.innerHeight / 2) {
			$wrapper.addClass('spoiler__wrapper_active spoiler__wrapper_gradient');
			$show.prop('hidden', false);
		}
		$show.on('click', () => {
			const prevHeight = $wrapper.outerHeight();
			$wrapper.removeClass('spoiler__wrapper_active');
			const nextHeight = $wrapper.outerHeight();
			$show.remove();
			anime({
				targets: $wrapper.get(0),
				height: [prevHeight, nextHeight],
				duration: animate.duration.effect,
				easing: animate.easing.standard,
			}).finished.then(() => {
				$wrapper.removeClass('spoiler__wrapper_gradient').css({
					height: '',
				});
			});
		});
	});
});

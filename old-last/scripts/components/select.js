/* global $ */

$(() => {
	$('.select').each((i, el) => {
		const $el = $(el);

		$el.find('.select__input').on('focusin focusout', () => {
			$el.toggleClass('select_focus');
		});
	});
});

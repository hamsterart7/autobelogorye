/* global $, i18n */

import 'formdata-polyfill';
import 'svgxuse';
import objectFitImages from 'object-fit-images';
import './lazyload.js';
import './jquery.fancybox.min';
import noUiSlider from './nouislider.js';
import insertionQ from 'insertion-query';
import shave from 'shave';
import 'simplebar';
import SimpleBar from 'simplebar';
import Swiper from 'swiper';
import animate from './animate';
import anime from 'animejs';
import Cookies from 'js-cookie';
import IMask from 'imask';

const mm = (device) => {
	let width = null;
	switch (device) {
		case 'tabletLandscapeUp':
			width = '(min-width: 960px)';
			break;
		case 'tabletLandscapeDown':
			width = '(max-width: 959px)';
			break;
	}
	return window.matchMedia(width).matches;
};

const numberWithSpaces = function(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
};

$(document).ready(function() {
	const $container = $('.content');

	/* Add hover class to clickable elements if not mobile */
	if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
		$('a').addClass('has-hover');
		$('button').addClass('has-hover');
	}

	/* Hide default select if not mobile */
	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
		$('.select__input').addClass('hidden');
		$('.select-filter__input').addClass('hidden');
	}

	/* Sticky Header */
	const stickyHeader = function() {
		const scroll = $(window).scrollTop();
		if (scroll >= 50) {
			$('.header').addClass('header--fixed');
		} else {
			$('.header').removeClass('header--fixed');
		}
	};
	stickyHeader();
	$(window).scroll(function() {
		stickyHeader();
	});

	/* Truncated text */
	shave('.js-truncated', 125);

	/* Init Selects */
	$('.js-select').each((i, el) => {
		$(el)
			.select2({
				width: 'resolve',
				minimumResultsForSearch: Infinity,
				dropdownParent: $(el)
					.parent()
					.parent(),
			})
			.on('select2:open', function(e) {
				$('.select2-dropdown').hide();
				setTimeout(function() {
					$('.select2-dropdown').slideDown(300);
				});
			});
	});
	insertionQ('.select2-dropdown').every(function(element) {
		new SimpleBar($('.select2-results__options')[0], {autoHide: false});
	});

	/* Mask */
	let phoneMask = function() {
		$('.js-cvv-field').each((i, el) => {
			new IMask(el, {
				mask: '000',
			});
		});
	};
	phoneMask();
	/* Dom changes */
	const observer = new MutationObserver((mutations) => {
		if ($('.js-phone-field').length) {
			phoneMask();
		}
	});

	observer.observe(document.body, {
		characterDataOldValue: true,
		subtree: true,
		childList: true,
		characterData: true,
	});

	$('.js-index-field').each((i, el) => {
		new IMask(el, {
			mask: '000000',
		});
	});
	$('.js-cart-num-field').each((i, el) => {
		new IMask(el, {
			mask: '0000 0000 0000 0000',
		});
	});
	$('.js-phone-field').each((i, el) => {
		new IMask(el, {
			mask: i18n.masks.tel,
		});
	});

	/* Range slider */
	$('.js-range-slider-large').each((i, el) => {
		const range = el;
		const rangeFields = [
			$(el)
				.parents('.range')
				.find('.js-range-lower-field')
				.get(0),
			$(el)
				.parents('.range')
				.find('.js-range-upper-field')
				.get(0),
		];

		noUiSlider.create(range, {
			start: [1000000, 3000000],
			range: {
				min: [0],
				max: [10000000],
			},
			step: 1000,
			connect: true,
		});

		range.noUiSlider.on('update', function(values, handle) {
			rangeFields[handle].value = numberWithSpaces(values[handle].split('.')[0]) + ' ₽';
		});
	});
	$('.js-range-slider-small').each((i, el) => {
		const range = el;
		const rangeFields = [
			$(el)
				.parents('.range')
				.find('.js-range-lower-field')
				.get(0),
			$(el)
				.parents('.range')
				.find('.js-range-upper-field')
				.get(0),
		];

		noUiSlider.create(range, {
			start: [1000, 18000],
			range: {
				min: [0],
				max: [100000],
			},
			step: 100,
			connect: true,
		});

		range.noUiSlider.on('update', function(values, handle) {
			rangeFields[handle].value = numberWithSpaces(values[handle].split('.')[0]) + ' ₽';
		});
	});
	$('.js-range-percent').each((i, el) => {
		const range = el;
		const rangeFields = [
			$(el)
				.parents('.range')
				.find('.js-range-upper-field')
				.get(0),
		];

		noUiSlider.create(range, {
			start: 80,
			connect: 'lower',
			range: {
				min: 10,
				max: 80,
			},
			step: 1,
		});

		const onePerc = $(el)
			.parents('.range')
			.find('.range__values')
			.attr('data-one-percent');

		range.noUiSlider.on('update', function(values, handle) {
			const result = parseInt(values[handle].split('.')[0]) * parseInt(onePerc);
			rangeFields[handle].value = numberWithSpaces(result) + ' ₽';
		});
	});
	$('.js-range-month').each((i, el) => {
		const range = el;
		const rangeFields = [
			$(el)
				.parents('.range')
				.find('.js-range-upper-field')
				.get(0),
		];

		noUiSlider.create(range, {
			start: 36,
			connect: 'lower',
			range: {
				min: 12,
				max: 60,
			},
			step: 1,
		});

		range.noUiSlider.on('update', function(values, handle) {
			rangeFields[handle].value = values[handle].split('.')[0] + ' месяцев';
		});
	});

	/* Number field */
	$(function() {
		$('.number-field__input').append(
			'<button type="button" class="number-field__btn number-field__btn--dec">-</button><button type="button" class="number-field__btn number-field__btn--inc">+</button>'
		);
		$('.number-field__btn').on('click', function() {
			const $button = $(this);
			const $incButton = $button.parent().find('.number-field__btn--inc');
			const oldValue = $button
				.parent()
				.find('input')
				.val();
			let newVal;
			if ($button.hasClass('number-field__btn--inc')) {
				newVal = parseFloat(oldValue) + 1;
				if ($('.number-field__input').hasClass('js-num-rooms') && newVal === 15) {
					$incButton.attr('disabled', true);
				}
			} else {
				if (oldValue > 0) {
					newVal = parseFloat(oldValue) - 1;
					if ($('.number-field__input').hasClass('js-num-rooms') && newVal < 15) {
						$incButton.attr('disabled', false);
						console.log(1);
					}
				} else {
					newVal = 0;
				}
			}
			$button
				.parent()
				.find('input')
				.val(newVal);
			$button
				.parent()
				.find('.number-field__value')
				.text(newVal);
		});
	});

	/* Comparison */
	const compHeight = function() {
		const comH = $('.js-com-head-get-height').height();
		$('.js-com-head-set-height').css('height', comH);
	};
	if ($container.find('.js-com-head-get-height').length > 0) {
		compHeight();
		$(window).on('resize', () => {
			compHeight();
		});
	}

	/* Fancybox */
	$('[data-fancybox]').fancybox({
		infobar: false,
		buttons: ['close'],
	});

	/* Parallax comparison tile */
	const isInViewport = function(node) {
		let rect = node.getBoundingClientRect();
		return (
			(rect.height > 0 || rect.width > 0) &&
			rect.bottom >= 0 &&
			rect.right >= 0 &&
			rect.top <= (window.innerHeight || document.documentElement.clientHeight) &&
			rect.left <= (window.innerWidth || document.documentElement.clientWidth)
		);
	};
	$(window).scroll(function() {
		if ($('.tab-content--active').find('.scrolled-tiles').length !== 0) {
			const scrolledTiles = $('.tab-content--active .scrolled-tiles');
			const tiles = $('.tab-content--active .tile--comparison');
			let visible = isInViewport(tiles.get(0));
			if (visible) {
				scrolledTiles.css('opacity', 0);
				scrolledTiles.css('z-index', -1);
			} else {
				scrolledTiles.css('opacity', 1);
				scrolledTiles.css('z-index', 103);
			}
		}
	});

	/* Sliders */
	if ($container.find('.comparison-slider-holder').length > 0) {
		$container.find('.comparison-slider-holder').each((i, el) => {
			const $slider = $(el);
			const compSliderMini = new Swiper($slider.find('.comparison-slider-mini__container').get(0), {
				direction: 'horizontal',
				slidesPerView: 'auto',
			});
			const compSlider = new Swiper($slider.find('.comparison-slider__container').get(0), {
				direction: 'horizontal',
				slidesPerView: 'auto',
			});
			compSlider.controller.control = compSliderMini;
			compSliderMini.controller.control = compSlider;
		});
	}
	if ($container.find('.h-slider').length > 0) {
		$container.find('.h-slider').each((i, el) => {
			const $slider = $(el);
			new Swiper($slider.find('.h-slider__container').get(0), {
				direction: 'horizontal',
				slidesPerView: 'auto',
			});
		});
	}
	if ($container.find('.banner-slider').length > 0) {
		$container.find('.banner-slider').each((i, el) => {
			const $slider = $(el);
			new Swiper($slider.find('.banner-slider__container').get(0), {
				direction: 'horizontal',
				slidesPerView: 1,
				autoplay: true,
				speed: 3000,
				effect: 'fade',
				pagination: {
					clickable: true,
					el: '.banner-slider-pagination',
				},
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				},
			});
		});
	}
	const mSliderFunc = function() {
		if (mm('tabletLandscapeDown')) {
			if ($container.find('.m-slider').length > 0) {
				$container.find('.m-slider').each((i, el) => {
					const $slider = $(el);
					new Swiper($slider.find('.m-slider__container').get(0), {
						direction: 'horizontal',
						slidesPerView: 'auto',
					});
				});
			}
		}
	};
	mSliderFunc();
	$(window).on('resize', () => {
		mSliderFunc();
	});

	if ($container.find('.slider-fraction-4').length > 0) {
		$container.find('.slider-fraction-4').each((i, el) => {
			const $slider = $(el);
			new Swiper($slider.find('.slider-fraction-4__container').get(0), {
				direction: 'horizontal',
				slidesPerView: 'auto',
				spaceBetween: 16,
				pagination: {
					el: '.slider-nav__pagination',
					type: 'fraction',
				},
				navigation: {
					nextEl: '.slider-nav__next',
					prevEl: '.slider-nav__prev',
				},
				breakpoints: {
					960: {
						slidesPerView: 4,
						slidesPerGroup: 4,
						spaceBetween: 24,
					},
				},
			});
		});
	}
	if ($container.find('.slider-fraction-3').length > 0) {
		$container.find('.slider-fraction-3').each((i, el) => {
			const $slider = $(el);
			new Swiper($slider.find('.slider-fraction-3__container').get(0), {
				direction: 'horizontal',
				slidesPerView: 'auto',
				spaceBetween: 16,
				pagination: {
					el: '.slider-nav__pagination',
					type: 'fraction',
				},
				navigation: {
					nextEl: '.slider-nav__next',
					prevEl: '.slider-nav__prev',
				},
				breakpoints: {
					960: {
						slidesPerView: 3,
						slidesPerGroup: 3,
						spaceBetween: 24,
					},
				},
			});
		});
	}
	if ($container.find('.product-slider').length > 0) {
		$container.find('.product-slider').each((i, el) => {
			const $slider = $(el);
			const galleryThumbs = new Swiper('.product-slider-thumbs__container', {
				spaceBetween: 32,
				slidesPerView: 'auto',
				watchSlidesVisibility: true,
				watchSlidesProgress: true,
			});
			new Swiper($slider.find('.product-slider-gallery__container').get(0), {
				direction: 'horizontal',
				slidesPerView: 1,
				pagination: {
					el: '.product-slider-gallery__pagination',
				},
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				},
				thumbs: {
					swiper: galleryThumbs,
				},
			});
		});
	}
	if ($container.find('.tile-slider').length > 0) {
		$container.find('.tile-slider').each((i, el) => {
			const $slider = $(el);
			new Swiper($slider.find('.tile-slider__container').get(0), {
				direction: 'horizontal',
				slidesPerView: 1,
				pagination: {
					el: '.tile-slider__pagination',
					clickable: true,
				},
			});
		});
	}

	/* On hover show next tile slide */
	$('[data-tile-hover=hover]')
		.on('mouseover', function() {
			const $elem = $(this);
			$elem.find('.swiper-slide-active:not(:last-child)').css('display', 'none');
		})
		.on('mouseout', function() {
			$(this)
				.find('.swiper-slide')
				.css('display', 'block');
		});

	/* Address map */
	const addressesPlacemark = [[50.569299, 36.526892], [50.569093, 36.526164]];
	ymaps.ready(function() {
		if ($container.find('.map-box').length > 0) {
			const map = new ymaps.Map(
				'address-map',
				{
					center: [50.569299, 36.526892],
					zoom: 17,
					controls: [],
				},
				{
					suppressMapOpenBlock: true,
				}
			);
			for (let i = 0; i < addressesPlacemark.length; i++) {
				const myPlacemark1 = new ymaps.Placemark(
					addressesPlacemark[i],
					{
						hintContent: 'г.Москва, Дмитровское шоссе,  д.182Б стр.1',
						iconContent: '19',
					},
					{
						iconLayout: 'default#image',
						iconImageHref: '/images/pin.png',
						iconImageSize: [28, 40],
						iconImageOffset: [-14, -40],
					}
				);
				map.geoObjects.add(myPlacemark1);
			}

			map.controls.remove('routeButtonControl');
			map.controls.remove('zoomControl');
			map.controls.remove('geolocationControl');
			map.controls.remove('searchControl');
			map.controls.remove('trafficControl');
			map.controls.remove('typeSelector');
			map.controls.remove('fullscreenControl');
			map.controls.remove('rulerControl');
			map.behaviors.disable(['scrollZoom']);
		}
	});

	/* Upload file */
	const FileListItem = function(a) {
		a = [].slice.call(Array.isArray(a) ? a : arguments);
		let b, c, d;
		for (c, b = c = a.length, d = !0; b-- && d; ) d = a[b] instanceof File;
		if (!d) throw new TypeError('expected argument to FileList is File or array of File objects');
		for (b = new ClipboardEvent('').clipboardData || new DataTransfer(); c--; ) b.items.add(a[c]);
		return b.files;
	};

	let fl = [];
	$('.upload-file').each((i, el) => {
		const $inp = $(el);

		$inp.on('click', function(e) {
			e.target.value = null;
		});

		const uploadFiles = function(files) {
			const $list = $inp.parents('.attach').find('.file-list');
			$list.text('');
			for (let i = 0; i < files.length; i++) {
				const uploadedDiv = $('<div>', {class: 'file'}).appendTo($list);
				$('<div class="file__name">' + files[i].name + '</div>').appendTo(uploadedDiv);
				$('<button>', {class: 'file__delete-btn', type: 'button'}).appendTo(uploadedDiv);
			}
			$('.file').each(function(i, cont) {
				$(cont).attr('data-ind', i);
			});
		};

		$inp.on('change', function(e) {
			if (fl.length > 0) {
				uploadFiles(this.files);
				fl = $.merge($.merge([], fl), this.files);
			} else {
				fl = Array.from(this.files);
				uploadFiles(this.files);
			}
			$inp.files = new FileListItem(fl);
			$inp.prop('files', $inp.files);
		});

		/* Dom changes */
		const observerFiles = new MutationObserver((mutations) => {
			$(document).on('click', '.file__delete-btn', function(e) {
				e.preventDefault();
				const $del = $(e.currentTarget);
				const $file = $del.parents('.file');
				const $fileList = $del.parents('.file-list');
				const ind = $file.attr('data-ind');
				$file.remove();
				const val = fl[ind];
				fl = fl.filter((item) => item !== val);
				$('.file').each(function(i, cont) {
					if (i > ind) {
						$(cont).attr('data-ind', i - 1);
					}
				});
				$inp.files = new FileListItem(fl);
				$inp.prop('files', $inp.files);
				$fileList.text('Файл не выбран');
			});
		});
		observerFiles.observe(document.body, {
			characterDataOldValue: true,
			subtree: true,
			childList: true,
			characterData: true,
		});
	});
});

$(() => {
	/* Cookies */
	const $notice = $('.cookies-notice');

	if (Cookies.get('cookies-notice') === undefined) {
		$notice.prop('hidden', false);
	}

	$notice.find('.cookies-notice__close').on('click', () => {
		Cookies.set('cookies-notice', true);
		anime({
			targets: $notice.get(0),
			opacity: 0,
			translateY: $notice.outerHeight(),
			duration: animate.duration.leave,
			easing: animate.easing.acceleration,
		}).finished.then(() => {
			// Удаляем элемент
			$notice.remove();
		});
	});

	/* Filter form */
	$('.js-filter-form').submit(function(e) {
		$('.js-slide-up').addClass('slide-up');
		e.preventDefault();
	});

	/* Promocode */
	$('.fav-btn').click(function(e) {
		e.preventDefault();
		$(this)
			.closest('.fav-btn')
			.toggleClass('fav-btn--added');
	});

	/* Cart price */
	$(document).on('click', '.js-item-num-field button', function(e) {
		e.preventDefault();
		const $btn = $(e.currentTarget);
		const $parentDiv = $btn.parents('.js-item-price-box');
		const itemsNum = $parentDiv.find('.js-items-num-val').val();
		const itemPriceVal = $parentDiv.find('.js-item-price-val').val();
		const itemOldPriceVal = $parentDiv.find('.js-item-old-price-val').val();
		$parentDiv.find('.js-item-price-sum').text(numberWithSpaces(itemsNum * itemPriceVal) + ' ₽');
		$parentDiv.find('.js-item-old-price-sum').text(numberWithSpaces(itemsNum * itemOldPriceVal) + ' ₽');
	});

	/* Favourite button */
	$('.js-promocode-btn').click(function(e) {
		e.preventDefault();
		$('.js-promocode').addClass('hidden');
		$('.js-promocode-field').removeClass('hidden');
	});

	/* Tabs */
	$('.js-tab').click(function(e) {
		const $btn = $(e.currentTarget);
		$('.js-tab').removeClass('active');
		$('.tab-content').removeClass('tab-content--active');
		const $tabId = $btn.attr('data-tab');
		const tabContent = $("[id='" + $tabId + "']");
		$("[data-tab='" + $tabId + "']").addClass('active');
		tabContent.addClass('tab-content--active');
	});

	/* Spoiler */
	$('.js-spoiler-btn').click(function(e) {
		const $btn = $(e.currentTarget);
		const $container = $btn.parents('.spoiler');
		const $wrapper = $container.find('.spoiler__inner');
		const $show = $container.find('.spoiler__show');
		const prevHeight = $wrapper.outerHeight();
		$wrapper.removeClass('spoiler__inner--active');
		$show.addClass('hidden');
		const nextHeight = $wrapper.outerHeight();
		anime({
			targets: $wrapper.get(0),
			height: [prevHeight, nextHeight],
			duration: animate.duration.effect,
			easing: animate.easing.standard,
		}).finished.then(() => {
			$wrapper.css({
				height: '',
			});
		});
	});

	/* Give feedback */
	$('.js-star')
		.on('mouseover', function() {
			const onStar = parseInt($(this).data('value'), 10);
			$(this)
				.parent()
				.children('.js-star')
				.each(function(e) {
					if (e < onStar) {
						$(this).addClass('star--hover');
					} else {
						$(this).removeClass('star--hover');
					}
				});
		})
		.on('mouseout', function() {
			$(this)
				.parent()
				.children('.js-star')
				.each(function(e) {
					$(this).removeClass('star--hover');
				});
		});
	$('.js-star').on('click', function() {
		const onStar = parseInt($(this).data('value'), 10);
		const stars = $(this)
			.parent()
			.children('.js-star');
		for (let i = 0; i < stars.length; i++) {
			$(stars[i]).removeClass('star--active');
		}
		for (let i = 0; i < onStar; i++) {
			$(stars[i]).addClass('star--active');
		}
	});

	/* Accordion */
	$('.accordion-set > button').on('click', function() {
		if ($(this).hasClass('accordion-set__link--active')) {
			$(this).removeClass('accordion-set__link--active');
			$(this)
				.parents('.accordion-set')
				.removeClass('accordion-set--active');
			$(this)
				.siblings('.accordion-set__content')
				.slideUp(200);
		} else {
			$('.accordion-set > button').removeClass('accordion-set__link--active');
			$('.accordion-set').removeClass('accordion-set--active');
			$(this)
				.parents('.accordion-set')
				.addClass('accordion-set--active');
			$(this).addClass('accordion-set__link--active');
			$('.accordion-set__content').slideUp(200);
			$(this)
				.siblings('.accordion-set__content')
				.slideDown(200);
		}
	});

	/* Credit toggle checkbox */
	$('.js-credit-toggle').change(function(e) {
		const $check = $(e.currentTarget);
		const $notCheckedLbl = $check.parents('.toggle').find('.js-credit-toggle-label-not-checked');
		const $checkedLbl = $check.parents('.toggle').find('.js-credit-toggle-label-checked');
		if ($check.is(':checked')) {
			$('.js-hidden-by-toggle').removeClass('hidden');
			$notCheckedLbl.removeClass('toggle__label--checked');
			$checkedLbl.addClass('toggle__label--checked');
		} else {
			$('.js-hidden-by-toggle').addClass('hidden');
			$notCheckedLbl.addClass('toggle__label--checked');
			$checkedLbl.removeClass('toggle__label--checked');
		}
	});

	/* Leasing calculate form */
	$('.js-calculate-btn').click(function(e) {
		e.preventDefault();
		$('.js-calculated-img').removeClass('hidden');
		$('.js-no-price').addClass('hidden');
		$('.js-price').removeClass('hidden');
		$('.js-leasing-info').addClass('hidden');
		$('.js-leasing-info-2').removeClass('hidden');
	});

	/* Header search */
	$('.js-header-search-btn').click(function(e) {
		e.preventDefault();
		$('.search-box').addClass('search-box--active');
		$('.search-results').slideDown();
		$('.content').addClass('content--search');
	});
	$(document).mouseup((e) => {
		if (
			!$('.login-popup__box').is(e.target) &&
			$('.login-popup__box').has(e.target).length === 0 &&
			!$('.popup').is(e.target) &&
			$('.popup').has(e.target).length === 0 &&
			!$('.search-box').is(e.target) &&
			$('.search-box').has(e.target).length === 0
		) {
			$('.search-results').slideUp();
			$('.content').removeClass('content--search');
			$('.header-search').removeClass('header-search--active');
			$('.js-mobile-options').removeClass('hidden');
			$('body').removeClass('locked');
			setTimeout(function() {
				$('.search-box').removeClass('search-box--active');
			}, 500);
		}
	});

	/* Open mobile search */
	$('.js-open-mobile-search-btn').click(function() {
		$('.header-search').toggleClass('header-search--active');
		$('.js-mobile-options').toggleClass('hidden');
		$('body').toggleClass('locked');
		$('.content').addClass('content--search');
	});

	/* Open mobile footer nav */
	$('.js-open-mobile-footer-nav-btn').click(function() {
		$('.footer-nav__col--mobile').slideToggle();
		$('.copyright').toggleClass('hide-on-mobile');
		$('.politics').toggleClass('hide-on-mobile');
		setTimeout(function() {
			$('.js-open-mobile-footer-nav-btn')
				.parents('.footer-nav__item')
				.addClass('hidden');
		}, 200);
	});

	/* Open mobile nav */
	$('.js-open-mobile-nav-btn').click(function() {
		$('body').removeClass('locked');
		$('.navigation').toggleClass('navigation--active');
		$('.header').toggleClass('header--active');
		$('.js-open-mobile-search-btn').toggleClass('hidden');
		$('.js-open-mobile-nav-btn').toggleClass('menu-button--active');
		$('.desktop-logo').toggleClass('hidden');
		$('.mobile-logo').toggleClass('hidden');
		if ($('.header').hasClass('header--active')) {
			$('body').addClass('locked');
		}
	});

	/* Open mobile catalog filter */
	$('.js-open-catalog-filter-btn').click(function() {
		$('.catalog-filter').addClass('catalog-filter--active');
		$('body').addClass('locked');
		$('.js-mobile-options').addClass('hidden');
		$('.header').addClass('header--active');
		$('.desktop-logo').addClass('hidden');
		$('.mobile-logo').removeClass('hidden');
	});

	/* Close mobile catalog filter */
	$('.js-close-catalog-filter-btn').click(function() {
		$('.catalog-filter').removeClass('catalog-filter--active');
		$('body').removeClass('locked');
		$('.js-mobile-options').removeClass('hidden');
		$('.header').removeClass('header--active');
		$('.desktop-logo').removeClass('hidden');
		$('.mobile-logo').addClass('hidden');
	});

	/* Add cart */
	$('.js-show-new-cart-form-btn').click(function(e) {
		e.preventDefault();
		$('.js-new-cart-form').removeClass('hidden');
		$('.js-show-new-cart-form-btn').addClass('hidden');
	});
	$('.js-hide-new-cart-form-btn').click(function(e) {
		e.preventDefault();
		$('.js-new-cart-form').addClass('hidden');
		$('.js-show-new-cart-form-btn').removeClass('hidden');
	});

	/* Agreement */
	$('.js-personal-check').change(function() {
		const ind = $(this).attr('data-personal-check');
		if ($(this).is(':checked')) {
			$("[data-personal-btn='" + ind + "']").prop('disabled', false);
		} else {
			$("[data-personal-btn='" + ind + "']").prop('disabled', true);
		}
	});

	/* Dropdown */
	$('.js-dropdown-btn').click(function(e) {
		e.preventDefault();
		const btn = $(e.currentTarget);
		const dropdown = btn.parents('.dropdown-holder').find('.js-dropdown');
		if (btn.hasClass('dropdown-btn--active')) {
			btn.removeClass('dropdown-btn--active');
			dropdown.removeClass('dropdown--active');
		} else {
			$('.js-dropdown').removeClass('dropdown--active');
			$('.js-dropdown-btn').removeClass('dropdown-btn--active');
			btn.addClass('dropdown-btn--active');
			dropdown.addClass('dropdown--active');
		}
	});
	$('.js-dropdown-list li').click(function(e) {
		e.preventDefault();
		const btn = $(e.currentTarget);
		const dropdown = btn.parents('.dropdown-holder').find('.js-dropdown');
		const dropdownBtn = btn.parents('.dropdown-holder').find('.js-dropdown-btn');
		dropdown.removeClass('dropdown--active');
		dropdownBtn.removeClass('dropdown-btn--active');
	});

	/* Open login popup */
	$('.js-open-login-popup-btn').click(function() {
		$('.login-popup').addClass('login-popup--active');
		$('body').addClass('locked');
		$('.js-mobile-options').addClass('hidden');
		$('.header').addClass('header--active');
		$('.desktop-logo').addClass('hidden');
		$('.mobile-logo').removeClass('hidden');
		$('.navigation').removeClass('navigation--active');
		$('.js-open-mobile-nav-btn').removeClass('menu-button--active');
	});

	/* Open registration popup */
	$('.js-open-reg-popup-btn').click(function() {
		$('.login-popup').addClass('login-popup--active');
		$('body').addClass('locked');
		$('.js-login').addClass('hidden');
		$('.js-registration').removeClass('hidden');
		$('.js-mobile-options').addClass('hidden');
		$('.header').addClass('header--active');
		$('.desktop-logo').addClass('hidden');
		$('.mobile-logo').removeClass('hidden');
		$('.navigation').removeClass('navigation--active');
		$('.js-open-mobile-nav-btn').removeClass('menu-button--active');
	});

	/* Close login popup */
	$('.js-close-login-popup-btn').click(function() {
		$('.login-popup').removeClass('login-popup--active');
		$('body').removeClass('locked');
		$('.js-mobile-options').removeClass('hidden');
		$('.header').removeClass('header--active');
		$('.desktop-logo').removeClass('hidden');
		$('.mobile-logo').addClass('hidden');
		$('.js-open-mobile-search-btn').removeClass('hidden');
	});

	if (mm('tabletLandscapeUp')) {
		$(document).mouseup((e) => {
			if (
				!$('.login-popup__box').is(e.target) &&
				$('.login-popup__box').has(e.target).length === 0 &&
				!$('.popup').is(e.target) &&
				$('.popup').has(e.target).length === 0 &&
				!$('.search-box').is(e.target) &&
				$('.search-box').has(e.target).length === 0
			) {
				$('.login-popup').removeClass('login-popup--active');
				$('body').removeClass('locked');
			}
		});
	}

	if (mm('tabletLandscapeDown')) {
		if ($('.login-popup').hasClass('login-popup--active')) {
			$('.js-mobile-options').addClass('hidden');
			$('.header').addClass('header--active');
			$('.desktop-logo').addClass('hidden');
			$('.mobile-logo').removeClass('hidden');
		}
	}

	/* Show registration */
	$('.js-show-registration-btn').click(function(e) {
		e.preventDefault();
		$('.js-login').addClass('hidden');
		$('.js-registration').removeClass('hidden');
	});

	/* Show password */
	$('.js-show-password-btn').click(function(e) {
		e.preventDefault();
		$('.js-login').addClass('hidden');
		$('.js-password').removeClass('hidden');
	});

	/* Show login */
	$('.js-show-login-btn').click(function(e) {
		e.preventDefault();
		$('.js-login').removeClass('hidden');
		$('.js-registration').addClass('hidden');
		$('.js-password').addClass('hidden');
	});

	/* Close popup */
	const toggleMobilePopup = function() {
		$('.header').toggleClass('header--active');
		$('.desktop-logo').toggleClass('hidden');
		$('.mobile-logo').toggleClass('hidden');
		$('.js-mobile-options').toggleClass('hidden');
	};

	$('.popup-box-close-box').click(function(e) {
		e.preventDefault();
		$('.popup-box').removeClass('popup-box--active');
		$('body').removeClass('locked');
	});

	$('.js-close-popup-btn').click(function() {
		$('.popup-box').removeClass('popup-box--active');
		$('body').removeClass('locked');
		$('.header').removeClass('header--active');
		$('.desktop-logo').removeClass('hidden');
		$('.mobile-logo').addClass('hidden');
		$('.js-mobile-options').removeClass('hidden');
	});

	/* Offer popup */
	$('.js-open-offer-popup-btn').click(function() {
		$('.js-offer-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			toggleMobilePopup();
		}
	});

	/* Order popup */
	$('.js-open-order-popup-btn').click(function() {
		$('.js-order-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			toggleMobilePopup();
		}
	});

	/* Credit calculator popup */
	$('.js-open-credit-calculator-popup-btn').click(function() {
		$('.js-credit-calculator-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			toggleMobilePopup();
		}
	});

	/* Leasing calculator popup */
	$('.js-open-leasing-calculator-popup-btn').click(function() {
		$('.js-leasing-calculator-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			toggleMobilePopup();
		}
	});

	/* Request call popup */
	$('.js-open-request-call-btn').click(function() {
		$('.js-request-call-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			toggleMobilePopup();
		}
	});

	/* Sign up popup */
	$('.js-sign-up-btn').click(function() {
		$('.js-sign-up-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			toggleMobilePopup();
		}
	});

	/* Feedback popup */
	$('.js-open-feedback-popup-btn').click(function(e) {
		e.preventDefault();
		$('.js-feedback-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			toggleMobilePopup();
		}
	});

	/* Give feedback popup */
	$('.js-open-give-feedback-popup-btn').click(function(e) {
		e.preventDefault();
		$('.js-give-feedback-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			toggleMobilePopup();
		}
	});

	/* Question popup */
	$('.js-open-question-popup-btn').click(function(e) {
		e.preventDefault();
		$('.js-question-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			toggleMobilePopup();
		}
	});

	/* Vacancy popup */
	$('.js-open-vacancy-popup-btn').click(function(e) {
		e.preventDefault();
		$('.js-vacancy-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			toggleMobilePopup();
		}
	});

	/* Vacancy accepted popup */
	$('.js-vacancy-accepted-btn').click(function(e) {
		e.preventDefault();
		$('.popup-box').removeClass('popup-box--active');
		$('.js-vacancy-accepted-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			$('.header').removeClass('header--active');
			$('.desktop-logo').removeClass('hidden');
			$('.mobile-logo').addClass('hidden');
			$('.js-mobile-options').addClass('hidden');
		}
	});

	/* Feedback sent popup */
	$('.js-open-feedback-sent-popup-btn').click(function(e) {
		e.preventDefault();
		$('.popup-box').removeClass('popup-box--active');
		$('.js-feedback-sent-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			$('.header').removeClass('header--active');
			$('.desktop-logo').removeClass('hidden');
			$('.mobile-logo').addClass('hidden');
			$('.js-mobile-options').addClass('hidden');
		}
	});

	/* Leasing application popup */
	$('.js-open-leasing-app-popup-btn').click(function(e) {
		e.preventDefault();
		$('.js-leasing-app-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			toggleMobilePopup();
		}
	});

	/* Credit application popup */
	$('.js-open-credit-app-popup-btn').click(function(e) {
		e.preventDefault();
		$('.js-credit-app-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			toggleMobilePopup();
		}
	});

	/* Application accepted popup */
	$('.js-application-accepted-btn').click(function(e) {
		e.preventDefault();
		$('.popup-box').removeClass('popup-box--active');
		$('.js-application-accepted-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			$('.header').removeClass('header--active');
			$('.desktop-logo').removeClass('hidden');
			$('.mobile-logo').addClass('hidden');
			$('.js-mobile-options').addClass('hidden');
		}
	});

	/* Request form */
	$('.js-leave-request-btn').click(function(e) {
		e.preventDefault();
		$('.js-request-form').addClass('hidden');
		$('.js-request-box').removeClass('hidden');
	});

	// IE11 detection
	if (/MSIE/.test(window.navigator.userAgent) || /Trident/.test(window.navigator.userAgent)) {
		$('body').addClass('msie');
	}

	// Polyfills
	objectFitImages();

	// https://www.thecssninja.com/javascript/pointer-events-60fps
	let peTimer;
	$(window).on('scroll', () => {
		if (peTimer) {
			clearTimeout(peTimer);
			peTimer = undefined;
		}
		const $body = $('body');
		if (!$body.hasClass('disable-hover')) {
			$body.addClass('disable-hover');
		}
		peTimer = setTimeout(() => {
			$body.removeClass('disable-hover');
		}, 100);
	});
});

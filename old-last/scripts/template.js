export default (str, values) => {
	if (toString.call(values) === '[object Object]') {
		Object.keys(values).forEach((key) => {
			const regexp = new RegExp(`{{${key}}}`);
			str = str.replace(regexp, values[key]);
		});
	}
	return str;
};

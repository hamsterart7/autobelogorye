# Установка на Битрикс

Всё необходимое - в папке `www`.

# Коротко о главном

- HTML-шаблоны хранятся в `templates` и собираются в `www`
- Скрипты хранятся в `scripts` и собираются в `www/scripts`
- Стили хранятся в `styles` и собираются в `www/styles`
- Шрифты хранятся в `styles/fonts` и собираются в `www/styles/fonts`
- Иконки хранятся в `styles/icons` и собираются в `www/styles/icons/icons.svg`
- Изображения, ***не* изменяемые через Битрикс** и являющиеся частью дизайна, хранятся в `styles/images` и собираются в `www/styles/images`
- Изображения, **изменяемые через Битрикс**, хранятся в `www/images`

# Установка и использование

## Git

Все проекты хранятся в [нашем GitLab](http://gitlab.dev-parus.ru/). Из внешней сети в него можно зайти через порт `8080`.

### Установка и настройка

Регистрания в GitLab проиходит через администратора - после того, как он добавит человека в сервис, на рабочую почту будет выслана секретная ссылка на активацию аккаунта и установку пароля.

Скачать Git и найти инструкции по его установке можно на [официальном сайте](https://git-scm.com/downloads).

Чтобы настроить Git, в терминале нужно ввести следующие команды, подставив свои данные:

```
git config --global user.name "Имя Фамилия"
git config --global user.email "example@ra-parus.ru"
```

Теперь нужно сгенерировать и добавить SSH-ключ в GitLab:

```
ssh-keygen -q -t rsa -b 4096 -f ~/.ssh/id_rsa -N ""
cat ~/.ssh/id_rsa.pub
```

Заходим в [настройку SSH-ключей](http://gitlab.dev-parus.ru/profile/keys), в поле `Key` вставляем то, что вывел терминал и нажимаем кнопку `Add Key`.

После этого выполняем комманду:

```
ssh -T git@gitlab.dev-parus.ru
```

В ответ на вопрос `Are you sure you want to continue connecting (yes/no)?` вводим `yes`. При успешном подключении выведится сообщение `Welcome to GitLab, Имя Фамилия!`, где `Имя Фамилия` - данные, указанные при настройке Git.

### Клонирование репозитория

Для работы над проектом, нужно склонировать репозиторий из хранилища:

```
git clone git@gitlab.dev-parus.ru:frontend/layout.git
cd layout
git checkout dev
```

Первая команда клонирует репозиторий из GitLab на комьютер, вторая переходит в папку с репозиторием, третья - переходит с master-ветки на dev. **Мы никогда не разрабатываем на master-ветке.**

Если над проектом одновременно работает несколько людей, лучше всего от dev-ветки создать свою собственную ветку:

```
git checkout -b example
```

### Фиксирование изменений и их отправка в хранилище

После того, как работы завершены, нужно зафиксировать изменнеия и отправить их в хранилище.

Фиксация измненений:

```
git add -A
git commit -m "Initial commit"
```

Вместо `Initial commit` нужно лаконично описать сделанные изменения (соблюдая правила орфографии и пунктуации).

Отправка изменений:

```
git push origin example
```

## Запуск сервера для разработки

`yarn run dev`

## Сборка файлов для релиза (форматирование, линтинг, оптимизация)

`yarn run build`

# Соглашения и рекомедации

## HTML

- Код должен проходить проверку [Nu Html Checker](https://github.com/validator/validator)

### Основные файлы

#### `templates/layout.njk`

Основной шаблон, на основе которого строятся все страницы.

#### `templates/index.njk`

Шаблон главной страницы - пример того, как должны выглядеть все остальные шаблоны. Именно этот файл используется для создания других страниц.

Содержит следующие блоки, используемые шаблонизатором:

- `title` - заголовок страницы, при сборке помещается в тег `<title></title>`
- `content` - собственно, сам код страницы; обратите внимание, что каждая страница должна начинаться с `<div class="content"></div>`

#### `templates/components`

Здесь хранятся повторяющиеся блоки, которые используются сразу на нескольких страницах. Импортировать их можно директивой `{% include "./components/example.njk" %}`

Так же в здесь хранятся хедер и футер сайта.

### Иконки

Для иконок используютя SVG-спрайты, которые автоматически генерируются из файлов, расположенных в `styles/icons`. Файл спрайта - `www/styles/icons/icons.svg`. Названия идентификаторов - названия файлов иконок.

Если в `styles/icons` есть вложенная папка, иконки из неё соберутся в отдельный спрайт, который будет назван по имени этой папки (например, иконки из папки `styles/icons/special` соберутся в `www/styles/icons/special.svg`).

Пример использования:

```html
<svg><use xlink:href="./styles/icons/icons.svg#add" /></svg>
```

Специально для IE11 по-умолчанию установлен [полофилл](https://github.com/Keyamoon/svgxuse).

Рекомендуемые для использования сеты иконок можно найти в репозитории [frontend/icons](http://gitlab.dev-parus.ru/frontend/icons).

### Favicon

Все иконки сайта генерируются автоматически из файла `favicon.png`, лежащего в корне репозитория. Размер файла по ширине и высоте должен быть не менее 512 пикселей.

Материалы по теме:

- [Everything you always wanted to know about touch icons](https://mathiasbynens.be/notes/touch-icons)
- [Configuring Web Applications](https://developer.apple.com/library/archive/documentation/AppleApplications/Reference/SafariWebContent/ConfiguringWebApplications/ConfiguringWebApplications.html)
- [Web App Manifest](https://www.w3.org/TR/appmanifest/)

## CSS

- Код должен проходить проверку [Stylelint](https://stylelint.io/)
- Используем стандартные [БЭМ-наименования](https://ru.bem.info/methodology/naming-convention/)
- Избегать использования атрибута `style` (кроме случаев, когда какие-то стили будут задаваться человеком через Битрикс)
- Код форматируется с помощью [Prettier](https://prettier.io/)

### Файлы

- `styles/main.css` - основные стили базового шаблона (+ переменные, кастомные медиа-выражения, миксины)

Стили каждого блока хранятся в отдельном файле, расположенном в `styles/components` и названном именем этого блока. Например, стили блока `.breadcrumbs` хранятся в `styles/components/breadcrumbs.css`. Все файлы из `styles/components` автоматически подключаются к `styles/main.css`.

### Основные рекомендации по вёрстке

#### Layout

Для создания "сетки" сайта мы пользуемся [Flexbox](https://developer.mozilla.org/ru/docs/Learn/CSS/CSS_layout/Flexbox) или [Grid Layout](https://developer.mozilla.org/ru/docs/Web/CSS/CSS_Grid_Layout/Basic_Concepts_of_Grid_Layout) - никаких float'ов и, тем более, таблиц. **Bootstap мы также не испольузем.**

#### z-index

- 1...100 - все основные объекты
- 100...200 - выпадающие меню
- 900...999 - оверлеи и модальные окна

#### Скрытие элементов

Для скрытия элементов со страницы вместо добавления свойства `display: none;` нужно использовать атрибут `hidden`, например `<div hidden></div>`.

### WYSIWYG

Текст, редактируемый через визуальный редактор Битрикса, должен помещаться в блок `<div class="wysiwyg"></div>`.

Стили, применяемые к этому блоку, содержатся в `styles/components/wysiwyg.css`. Применять стили нужно к имени тега, избегая использования классов.

К сожалению, Битрикс убирает неизвестные ему теги (например, тег `mark`). В этом случае допускается применение классов, являющихся элементами блока `wysiwyg` (`.wysiwyg__`). Так же классы можно использовать для каких-то нестандартных элементов (например, если нужно добавлять стили к определённым изображениям) - главное помнить, что каждый класс не только замедляет установку сайта на Битрикс, но и усложняет процесс оформления текста человеком.

## JS

- Код должен проходить проверку [ESLint](https://eslint.org/)
- Код форматируется с помощью [Prettier](https://prettier.io/)

### Файлы

- `scripts/components` - код независимых блоков (все файлы из `scripts/components` автоматически подключаются к `scripts/main.js`)
- `scripts/animate.js` - переменные для анимаций
- `scripts/pluralize.js` - склонение слов
- `scripts/lazyload.js` - lazyloading изображений
- `scripts/main.js` - основные скрипты шаблона (точка входа для Webpack)
- `scripts/mm.js` - переменные для медиа-выражений
- `scripts/scroll-to.js` - прокрутка до элемента
- `scripts/validate.js` - валидация форм

### jQuery

jQuery подключён через Google CDN, что ускоряет загрузку сайтов и решает проблемы с плагинами, требующими jQuery/$ в качестве глобальной переменной.

### Рекомендуемые плагины

#### [accounting.js](https://github.com/openexchangerates/accounting.js)

Форматирование чисел.

#### [clipboard.js](https://github.com/zenorocha/clipboard.js)

Копирование текста в буфер обмена.

#### [dayjs](https://github.com/iamkun/dayjs)

Работа с датами.

#### [Fanсybox](https://fancyapps.com/fancybox/3/)

Лайтбокс/галерея изображений.

Рекомендованные настройки (помещаются в main.js):

```js
// Fancybox global options Start
$.fancybox.defaults.infobar = false;
$.fancybox.defaults.toolbar = false;
$.fancybox.defaults.clickContent = (current) => {
	if (current.type === 'image') {
		return 'close';
	}
	return false;
};
// Fancybox global options End
```

#### [flatpickr](https://github.com/flatpickr/flatpickr)

Выбор даты и времени.

#### [horsey](https://github.com/bevacqua/horsey)

Подсказки для полей форм.

*Для правильной работы библиотеки, её нужно испортировать так:*

```js
import horsey from 'horsey/dist/horsey.js';
```

#### [in-view](https://github.com/camwiegert/in-view)

Слежение за скроллом и появленияем элементов.

#### [Magic-Grid](https://github.com/e-oj/Magic-Grid)

Masonry layout.

#### [noUiSlider](https://refreshless.com/nouislider/)

Ползунок.

#### [Rellax](https://github.com/dixonandmoe/rellax)

Параллакс при прокрутке страницы.

#### [Select2](https://select2.org/)

Стилизация выпадающих списков.

**Внимание**

До сих пор не найден способ подключения этой библиотеки с помощью JS-модулей, поэтому текущее решение - использование CDN:

```html
<head>
	<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/select2@4.0.6-rc.1/dist/css/select2.min.css">
</head>

<body>
	<!-- После jQuery -->
	<script src="//cdn.jsdelivr.net/npm/select2@4.0.6-rc.1/dist/js/select2.min.js"></script>
</body>
```

#### [ScrollBooster](https://github.com/ilyashubin/scrollbooster)

Скролл с помощью перетаскивания.

#### [Swiper](http://idangero.us/swiper/)

Слайдер.

#### [Tippy.js](https://atomiks.github.io/tippyjs/)

Тултипы.

# Изображения

## Полезные ресурсы

- [Essential Image Optimization](https://images.guide/)

## Файлы

- Изображения, **изменяемые через Битрикс**, хранятся в `www/images`
- Изображения, ***не* изменяемые через Битрикс** и являющиеся частью дизайна, хранятся в `styles/images`

## Оптимизация

Все изображения, лежащие в `styles/images`, при сборке автоматически оптимизируются с помощью MozJPEG/pngquant/SVGO.

## Lazy loading

Ко всем изображениям (включая фоновые), кроме тех, которые находятся в блоке `.wysiwyg`, должна быть применена отложенная загрузка. Для этого в шаблоне по-умолчанию установлена библиотека [lazysizes](https://github.com/aFarkas/lazysizes). Пример использования:

```html
<img class="lazyload" data-src="./images/example.jpg">
<div class="lazyload" data-background="./styles/images/example.jpg">
```

## Responsive images

Кроме отложенной загрузки, везде, где это возможно, следует использовать технику responsive images. Пример рекомендуемого кода:

```html
<picture>
	<source media="--mega-desktop-up" data-srcset="./images/example_600px.png, ./images/example_1200px.png 2x">
	<source media="--big-desktop-up" data-srcset="./images/example_500px.png, ./images/example_1000px.png 2x">
	<source media="--desktop-up" data-srcset="./images/example_400px.png, ./images/example_800px.png 2x">
	<source media="--tablet-landscape-up" data-srcset="./images/example_300px.png, ./images/example_600px.png 2x">
	<source media="--tablet-portrait-up" data-srcset="./images/example_200px.png, ./images/example_400px.png 2x">
	<img class="lazyload" data-src="./images/example_400px.png" data-srcset="./images/example_100px.png, ./images/example_200px.png 2x" alt="">
</picture>
```

Первое изображение в атрибуте `srcset` - максимально возможное по ширине для данного медиа-выражения. Для получения значения максимальной ширины нужно открыть сайт в максимальном для данного медиа-выражения размере (например, 1199 пикселей для `--tablet-landscape-up`) и измерить ширину изображения, округлив полученное значение вверх до 10 (например, если изображение размер 391 пиксель, округлить нужно до 400 пикселей). Так же нужно учитывать наличие полосы прокрутки - если она есть, к изображению нужно прибавить ещё 20 пикселей.

Второе изображение в атрибуте `srcset` должно быть в 2 раза больше первого - оно предназначено для экранов с большим [PPI](https://en.wikipedia.org/wiki/Pixel_density).

Медиа-выражения должны идти от большего к меньшему, так как браузер выбирает первое подходящее условие и останавливается.

Изображение, указываемое в атрибуте `src` тега `img` необходимо для браузеров, не поддерживающих тег `picture` (в нашем случае это Internet Explorer 11). Cамым оптимальным вариантом будет изображение, используемое для медиа-выражения `--desktop-up`.

Для генерации необходимых изображений используется команда `yarn run grunt resize:www/images/example.jpg:100,200,300,400`. В результате сгенерируются следующие файлы:

```
www/images/example_100px.jpg
www/images/example_200px.jpg
www/images/example_300px.jpg
www/images/example_400px.jpg
```

# Шрифты

## Файлы

Все подключаемые шрифты находятся в `./styles/fonts` и подключаются в `./styles/fonts.css`.

## Оптимизация

Все шрифты должны браться из [frontend/fonts](http://gitlab.dev-parus.ru/frontend/fonts). Если нужных шрифтов в нём нет, нужно сначала добавить их в него, следуя инструкциям, размещённым в репозитории.

Для оптимизации загрузки шрифтов, при их подключении используется свойство `font-display` со значением `swap`.

# Интерактивные элементы

## Анимация

### Время и функция плавности

Время анимации и её функция плавности всех интерактивных элементов сайта должны соответстовать [принципам Material Design](https://material.io/design/motion/).

В файлах `scripts/animate.js` и `styles/main.css` содержатся основные значения времени и функций плавности. Для JS-анимаций мы используем [anime.js](https://github.com/juliangarnier/anime/).

Например, для JS-анимации появления блока используются следующие переменные:

```js
import anime from 'animejs';

anime({
	targets: $('.block').get(0),
	opacity: [0, 1],
	duration: animate.duration.enter,
	easing: animate.easing.decelereation
});
```

## Склонение слов

Скрипт функции склонения слов находится в `scripts/ending.js`.

```js
import ending from './ending.js';

const count = 1;

$('.block').text(`${count} ${ending(count, ['слово', 'слова', 'слов'])}`);
```

## Прокрутка до элемента

Скрипт прокрутки до элемента находится в `scripts/components/scroll-to.js`.

Разметка:

```html
<button data-scroll=".block">Блок</button>

<div class="block">
	Hello, world!
</div>
```

Если элементов на странице несколько, можно указать на конкретный индекс:

```html
<button data-scroll=".block" data-scroll-index="1">Блок</button>

<div class="block">
	На меня не перейдёт :-(
</div>

<div class="block">
	А на меня перейдёт :-)
</div>
```

Если нужна программно управляемая прокрутка, можно воспользоваться функцией из `scripts/scroll-to.js`, передав ей в качестве эргумента jQuery-элемент:

```js
import scrollTo from './scroll-to.js';

scrollTo($('.block'));
```

## Модальные окна

Скрипт модальных окон находится в `scripts/components/overlay.js`.

Разметка:

```html
<button data-overlay="#fos">Форма обратной связи</button>

<div id="#fos" class="overlay" hidden>
	<div class="overlay__wrapper">
		Hello, world!
	</div>
</div>
```

Модальные окна можно так же показывать/скрывать программным путём:

```js
import {showOverlay, hideOverlay} from './overlay.js';

showOverlay($('#fos'));
hideOverlay($('#fos'));
```

## Спойлер

Скрипт спойлера находится в `scripts/components/spoiler.js`.

Разметка:
```html
<div class="spoiler">
	<div class="spoiler__wrapper spoiler__wrapper_active">
		Hello, world!
	</div>
	<button class="spoiler__show" type="button" hidden>Показать</button>
</div>
```

## Табы

Пример разметки:

```html
<div class="tabs">
    <ul class="tabs__nav">
        <li class="tabs__nav-item"><button class="tabs__button" type="button" disabled>Tab 1</button></li>
        <li class="tabs__nav-item"><button class="tabs__button" type="button">Tab 2</button></li>
        <li class="tabs__nav-item"><button class="tabs__button" type="button">Tab 3</button></li>
    </ul>
    <div class="tabs__content">Content 1</div>
    <div class="tabs__content" hidden>Content 2</div>
    <div class="tabs__content" hidden>Content 3</div>
</div>
```

Стилизовать блок `.tabs` **не надо** - он отвечает только за работу табов, а не за их внешний вид.

# Формы

## Доступность

### Лейблы

У кажого элемента формы должен быть заголовок (лейбл), описывающий его содержание:

```html
<label>
	Имя
	<input type="text" name="name" value="">
</label>
```

Очень часто вместо использования лейбла, дизайнер использует placeholder:

```html
<input type="text" name="name" value="" placeholder="Имя">
```

И, хотя, [такой подход неверен](https://www.filamentgroup.com/lab/a11y-form-labels.html), мы можем совместить его с лейблом, воспользовавшись классом `.visuallyhidden`:

```html
<label>
	<span class="visuallyhidden">Имя</span>
	<input type="text" name="name" value="" placeholder="Имя">
</label>
```

Этот класс скроет лейбл с экранов, но оставит его видимым для скринридеров.

## Стилизация элементов формы

### Чекбокс

```html
<label class="checkbox">
	<input class="visuallyhidden" type="checkbox">
	<span class="checkbox__input"></span>
	Checkbox
</label>
```

### Переключатель

```html
<label class="radio">
	<input class="visuallyhidden" type="radio" name="radio" value="1">
	<span class="radio__input"></span>
	Radio 1
</label>
<label class="radio">
	<input class="visuallyhidden" type="radio" name="radio" value="2">
	<span class="radio__input"></span>
	Radio 2
</label>
```

### Выпадающий список

Для стилизации выпадающих списков используется класс `.select` со следующей разметкой:

```html
<span class="select">
	<select class="select__input">
		...
	</select>
</span>
```

### Выбор файлов

Для стилизации выбора файлов используется класс `.file-input` со следующей разметкой:

```html
<span class="file-input">
	<input
		name="document"
		type="file"
		required
		accept="image/jpeg, image/png"
		multiple
		data-max-bytes="2097152">
	<span class="file-input__label">Прикрепить документ (DOC, PDF)</span>
</span>
```

Пример JS-кода выбора файлов с помощью drag'n'drop:

```js
import _find from 'lodash/find';
import validate from './../validate.js';
import ending from './../ending.js';

const $form = $('form');
const $fileInput = $form.find('input[type="file"]');
const droppedFiles = [];
const addFiles = (files) => {
	const maxBytes = $fileInput.data('maxBytes');
	const accept = $fileInput.attr('accept').split(', ');
	if ($fileInput.prop('multiple')) {
		Object.values(files).forEach((file) => {
			if (
				!_find(droppedFiles, {name: file.name}) &&
				file.size <= maxBytes &&
				accept.indexOf(file.type) >= 0
			) {
				droppedFiles.push(file);
				$fileInput
					.parents('.file-input')
					.find('.file-input__label')
					.text(
						`${droppedFiles.length} ${ending(droppedFiles.length, [
							'файл',
							'файла',
							'файлов',
						])}`
					);
			}
		});
	} else {
		const file = files[0];
		if (file.size <= maxBytes && accept.indexOf(file.type) >= 0) {
			droppedFiles[0] = file;
			$fileInput
				.parents('.file-input')
				.find('.file-input__label')
				.text(file.name);
		}
	}
};

$form
	.on('dragover', (e) => {
		e.preventDefault();
		e.stopPropagation();
		$(e.currentTarget).addClass('form_dragover');
	})
	.on('dragleave drop', (e) => {
		e.preventDefault();
		e.stopPropagation();
		$(e.currentTarget).removeClass('form_dragover');
	})
	.on('drop', (e) => {
		e.preventDefault();
		e.stopPropagation();
		addFiles(e.originalEvent.dataTransfer.files);
	});

// Очищаем массив с перетащенными файлами при выборе новых через input
$fileInput.on('change', () => {
	droppedFiles.splice(0, droppedFiles.length);
});

$form.on('submit', (e) => {
	e.preventDefault();
	const data = new FormData($form.get(0));
	if (droppedFiles.length > 0) {
		const name = $fileInput.attr('name');
		data.delete(name);
		droppedFiles.forEach((file) => {
			data.append(name, file);
		});
	}
	const validation = validate($form, droppedFiles);
	if (validation.valid) {
		$.ajax({
			// ...
		});
	} else {
		// ...
	}
});
```

## Валидация

Пример разметки формы:

```html
<form action="/" method="POST" novalidate>
	<label>
		Имя
		<input name="name" type="text" required maxlength="100">
	</label>
	<label>
		Телефон
		<input name="tel" type="tel" required>
	</label>
	<label>
		Эл. почта
		<input name="email" type="email" required maxlength="100">
	</label>
	<label>
		Дата рождения
		<input name="birthday" type="date" required min="1918-01-01" max="2004-01-01">
	</label>
	<label>
		Сумма (от 100 до 1000)
		<input name="amount" type="number" required min="100" max="1000">
	</label>
	<label>
		Резюме (макс. 100 КБ)
		<input name="resume" type="file" required accept=".doc, .pdf" data-max-bytes="100000">
	</label>
	<button type="submit">Отправить</button>
</form>
```

Проверка формы на валидность:

```js
import validate from './validate.js';

$('form').on('submit', (e) => {
	e.preventDefault();
	const $form = $(e.currentTarget);
	const validation = validate($form);
});
```

В данном случае переменная `validation` будет иметь следующую структуру:

```
{
	valid: false,
	fields: [
		{
			name: 'name',
			errors: ['Обязательно для заполнения'],
		}
		{
			name: 'tel',
			errors: ['Обязательно для заполнения'],
		}
		...
	]
}
```

## Общение с сервером

При POST-запросах данные формы отправляются на сервер в формате [FormData](https://developer.mozilla.org/en-US/docs/Web/API/FormData). Сервер должен возвратить результат в формате JSON (при любых GET- или POST-запросах).

Для браузеров, не поддерживающих FormData, используется [полифилл](https://github.com/jimmywarting/FormData) (установлен по-умлочанию).

Пример отправки формы и получения данных:

```js
$('form').on('submit', (e) => {
	e.preventDefault();
	const $form = $(e.currentTarget);
	$.ajax({
		url: $form.attr('action'),
		method: $form.attr('method'),
		data: new FormData($form.get(0)),
		processData: false,
		contentType: false,
		dataType: 'json',
		success(result) {
			console.log(result);
		}
	});
});
```

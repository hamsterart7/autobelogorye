const {src, dest, parallel} = require('gulp');
const through2 = require('through2');
const nunjucks = require('nunjucks');

const html = () => {
	const env = new nunjucks.Environment(new nunjucks.FileSystemLoader('templates/'));
	return src(['templates/*.njk', '!templates/layout.njk'])
		.pipe(
			through2.obj((file, encoding, cb) => {
				env.renderString(file.contents.toString(), (err, result) => {
					file.contents = Buffer.from(result);
					file.path = file.path.replace(/\.njk$/, '.html');
					cb(err, file);
				});
			})
		)
		.pipe(dest('www/'));
};

exports.build = parallel(html);

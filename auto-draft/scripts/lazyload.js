import 'lazysizes';

window.lazySizesConfig = window.lazySizesConfig || {};
window.lazySizesConfig.customMedia = {
	'--tablet-portrait-up': '(min-width: 600px)',
	'--tablet-landscape-up': '(min-width: 900px)',
	'--desktop-up': '(min-width: 1200px)',
	'--big-desktop-up': '(min-width: 1500px)',
	'--mega-desktop-up': '(min-width: 2100px)',
};

document.addEventListener('lazybeforeunveil', (e) => {
	const bg = e.target.getAttribute('data-background');
	if (bg) {
		e.target.style.backgroundImage = `url('${bg}')`;
	}
});

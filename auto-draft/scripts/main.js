/* global $, i18n */

import 'formdata-polyfill';
import 'svgxuse';
import objectFitImages from 'object-fit-images';
import './lazyload.js';
import Swiper from 'swiper';
import 'simplebar';
import anime from 'animejs';
import animate from './animate';

const mm = (device) => {
	let width = null;
	switch (device) {
		case 'tabletLandscapeUp':
			width = '(min-width: 960px)';
			break;
		case 'tabletLandscapeDown':
			width = '(max-width: 960px)';
			break;
	}
	return window.matchMedia(width).matches;
};

$(document).ready(function() {
	const $container = $('.content');

	/* Add hover class to clickable elements if not mobile */
	if (!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
		$('a').addClass('has-hover');
		$('button').addClass('has-hover');
	}

	/* Hide default select if mobile */
	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
		$('.select__input').addClass('hidden');
	}

	/* Check iOS */
	if (/iPhone|iPad|iPod/i.test(navigator.userAgent)) {
		$('body').addClass('ios');
	}

	/* Sticky Header */
	const stickyHeader = function() {
		const scroll = $(window).scrollTop();
		if (scroll >= 50) {
			$('.header').addClass('header--fixed');
		} else {
			$('.header').removeClass('header--fixed');
		}
	};
	stickyHeader();
	$(window).scroll(function() {
		stickyHeader();
	});

	/* Init Selects */
	$('.js-select').select2({
		width: 'resolve',
		dropdownParent: $(this).parent('.js-select-holder'),
		minimumResultsForSearch: Infinity,
	});

	/* Range slider */
	/*	$( "div#range-slider" ).rangeslider();*/

	/* Number field */
	$(function() {
		$('.number-field__input').append(
			'<button type="button" class="number-field__btn number-field__btn--dec">-</button><button type="button" class="number-field__btn number-field__btn--inc">+</button>'
		);
		$('.number-field__btn').on('click', function() {
			const $button = $(this);
			const $incButton = $button.parent().find('.number-field__btn--inc');
			const oldValue = $button
				.parent()
				.find('input')
				.val();
			let newVal;
			if ($button.hasClass('number-field__btn--inc')) {
				newVal = parseFloat(oldValue) + 1;
				if ($('.number-field__input').hasClass('js-num-rooms') && newVal === 15) {
					$incButton.attr('disabled', true);
				}
			} else {
				if (oldValue > 0) {
					newVal = parseFloat(oldValue) - 1;
					if ($('.number-field__input').hasClass('js-num-rooms') && newVal < 15) {
						$incButton.attr('disabled', false);
						console.log(1);
					}
				} else {
					newVal = 0;
				}
			}
			$button
				.parent()
				.find('input')
				.val(newVal);
			$button
				.parent()
				.find('.number-field__value')
				.text(newVal);
		});
	});

	/* Sliders */
	if ($container.find('.banner-slider').length > 0) {
		$container.find('.banner-slider').each((i, el) => {
			const $slider = $(el);
			new Swiper($slider.find('.banner-slider__container').get(0), {
				direction: 'horizontal',
				slidesPerView: 1,
				autoplay: true,
				speed: 3000,
				effect: 'fade',
				pagination: {
					el: '.banner-slider__pagination',
				},
			});
		});
	}
	if ($container.find('.h-slider').length > 0) {
		$container.find('.h-slider').each((i, el) => {
			const $slider = $(el);
			new Swiper($slider.find('.h-slider__container').get(0), {
				direction: 'horizontal',
				slidesPerView: 'auto',
			});
		});
	}
	if ($container.find('.tile-slider').length > 0) {
		$container.find('.tile-slider').each((i, el) => {
			const $slider = $(el);
			new Swiper($slider.find('.tile-slider__container').get(0), {
				direction: 'horizontal',
				slidesPerView: 1,
				pagination: {
					el: '.tile-slider__pagination',
				},
			});
		});
	}
	if ($container.find('.product-slider').length > 0) {
		$container.find('.product-slider').each((i, el) => {
			const $slider = $(el);
			const galleryThumbs = new Swiper('.product-slider-thumbs__container', {
				spaceBetween: 40,
				slidesPerView: 5,
				watchSlidesVisibility: true,
				watchSlidesProgress: true,
			});
			new Swiper($slider.find('.product-slider-gallery__container').get(0), {
				direction: 'horizontal',
				slidesPerView: 1,
				pagination: {
					el: '.product-slider-gallery__pagination',
				},
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				},
				thumbs: {
					swiper: galleryThumbs,
				},
			});
		});
	}
	if ($container.find('.news-slider').length > 0) {
		$container.find('.news-slider').each((i, el) => {
			const $slider = $(el);
			new Swiper($slider.find('.news-slider__container').get(0), {
				direction: 'horizontal',
				slidesPerView: 'auto',
				spaceBetween: 16,
				pagination: {
					el: '.slider-nav__pagination',
					type: 'fraction',
				},
				navigation: {
					nextEl: '.slider-nav__next',
					prevEl: '.slider-nav__prev',
				},
				breakpoints: {
					960: {
						slidesPerView: 4,
						slidesPerGroup: 4,
						spaceBetween: 24,
					},
				},
			});
		});
	}
	if ($container.find('.logo-slider').length > 0) {
		$container.find('.logo-slider').each((i, el) => {
			const $slider = $(el);
			new Swiper($slider.find('.logo-slider__container').get(0), {
				direction: 'horizontal',
				slidesPerView: 'auto',
				spaceBetween: 16,
				pagination: {
					el: '.slider-nav__pagination',
					type: 'fraction',
				},
				navigation: {
					nextEl: '.slider-nav__next',
					prevEl: '.slider-nav__prev',
				},
				breakpoints: {
					960: {
						slidesPerView: 3,
						slidesPerGroup: 3,
						spaceBetween: 24,
					},
				},
			});
		});
	}
	if ($container.find('.m-slider').length > 0) {
		$container.find('.m-slider').each((i, el) => {
			const $slider = $(el);
			new Swiper($slider.find('.m-slider__container').get(0), {
				direction: 'horizontal',
				slidesPerView: 'auto',
				breakpoints: {
					960: {
						noSwiping: false,
						allowSlidePrev: false,
						allowSlideNext: false,
						autoplay: false,
						keyboard: false,
					},
				},
			});
		});
	}

	/* Upload files */
	const fileListItem = function(a) {
		a = [].slice.call(Array.isArray(a) ? a : arguments);
		for (let c, b = (c = a.length), d = !0; b-- && d; ) d = a[b] instanceof File;
		if (!d) throw new TypeError('expected argument to FileList is File or array of File objects');
		for (b = new ClipboardEvent('').clipboardData || new DataTransfer(); c--; ) b.items.add(a[c]);
		return b.files;
	};

	let fl = [];
	const $inp = $('.upload-file');

	$('.upload-file').on('click', function(e) {
		e.target.value = null;
	});

	const uploadFiles = function(files) {
		const $list = $inp.parents('.attach').find('.file-list');
		$('.file-list').text('');
		for (let i = 0; i < files.length; i++) {
			let uploadedDiv = $('<div>', {class: 'file'}).appendTo($list);
			$('<div class="file__name">' + files[i].name + '</div>').appendTo(uploadedDiv);
			$('<button>', {class: 'file__delete-btn', type: 'button'}).appendTo(uploadedDiv);
		}
		$('.file').each(function(i, cont) {
			$(cont).attr('data-ind', i);
		});
	};

	$('.upload-file').on('change', function(e) {
		if (fl.length > 0) {
			uploadFiles(this.files);
			fl = $.merge($.merge([], fl), this.files);
		} else {
			fl = Array.from(this.files);
			uploadFiles(this.files);
		}
		$inp.files = new fileListItem(fl);
		$inp.prop('files', $inp.files);
	});
	$(document).on('click', '.file__delete-btn', function(e) {
		e.preventDefault();
		const $del = $(e.currentTarget);
		const $file = $del.parents('.file');
		const ind = $file.attr('data-ind');
		$file.remove();
		let val = fl[ind];
		fl = fl.filter((item) => item !== val);
		$('.uploaded-image').each(function(i, cont) {
			if (i > ind) {
				$(cont).attr('data-ind', i - 1);
			}
		});
		$inp.files = new fileListItem(fl);
		$inp.prop('files', $inp.files);
		$('.file-list').text('Файл не выбран');
	});

	/* Address map */
	let addressesPlacemark = [
		[50.569299, 36.526892],
		[51.339066, 37.864825],
		[50.569093, 36.526164],
		[50.57419, 36.543088],
	];
	ymaps.ready(function() {
		if ($container.find('.map-box').length > 0) {
			let map = new ymaps.Map(
				'address-map',
				{
					center: [50.568818, 36.527754],
					zoom: 17,
					controls: [],
				},
				{
					suppressMapOpenBlock: true,
				}
			);
			for (let i = 0; i < addressesPlacemark.length; i++) {
				let myPlacemark1 = new ymaps.Placemark(
					addressesPlacemark[i],
					{
						hintContent: 'г.Москва, Дмитровское шоссе,  д.182Б стр.1',
						iconContent: '19',
					},
					{
						iconLayout: 'default#image',
						iconImageHref: '/images/pin.png',
						iconImageSize: [28, 40],
						iconImageOffset: [-14, -40],
					}
				);
				map.geoObjects.add(myPlacemark1);
			}

			map.controls.remove('routeButtonControl');
			map.controls.remove('zoomControl');
			map.controls.remove('geolocationControl');
			map.controls.remove('searchControl');
			map.controls.remove('trafficControl');
			map.controls.remove('typeSelector');
			map.controls.remove('fullscreenControl');
			map.controls.remove('rulerControl');
			map.behaviors.disable(['scrollZoom']);
		}
	});
});

$(() => {
	/* Open mobile footer nav */
	$('.js-open-mobile-footer-nav-btn').click(function() {
		$('.footer-nav__col--mobile').slideToggle();
		setTimeout(function() {
			$('.js-open-mobile-footer-nav-btn').addClass('hidden');
		}, 200);
	});

	/* Open mobile nav */
	$('.js-open-mobile-nav-btn').click(function() {
		$('.navigation').toggleClass('navigation--active');
		$('.header').toggleClass('header--active');
		$('.js-open-mobile-search-btn').toggleClass('hidden');
		$('.js-open-mobile-nav-btn').toggleClass('menu-button--active');
		$('.desktop-logo').toggleClass('hidden');
		$('.mobile-logo').toggleClass('hidden');
		$('body').toggleClass('locked');
	});

	/* Open login popup */
	$('.js-open-login-popup-btn').click(function() {
		$('.login-popup').addClass('login-popup--active');
		$('body').addClass('locked');
		$('.js-mobile-options').addClass('hidden');
		$('.header').addClass('header--active');
		$('.desktop-logo').addClass('hidden');
		$('.mobile-logo').removeClass('hidden');
		$('.navigation').removeClass('navigation--active');
		$('.js-open-mobile-nav-btn').removeClass('menu-button--active');
	});

	/* Close login popup */
	$('.js-close-login-popup-btn').click(function() {
		$('.login-popup').removeClass('login-popup--active');
		$('body').removeClass('locked');
		$('.js-mobile-options').removeClass('hidden');
		$('.header').removeClass('header--active');
		$('.desktop-logo').removeClass('hidden');
		$('.mobile-logo').addClass('hidden');
		$('.js-open-mobile-search-btn').removeClass('hidden');
	});

	if (mm('tabletLandscapeUp')) {
		$(document).mouseup((e) => {
			if (!$('.login-popup__box').is(e.target) && $('.login-popup__box').has(e.target).length === 0) {
				$('.login-popup').removeClass('login-popup--active');
				$('body').removeClass('locked');
			}
		});
	}

	/* Show registration */
	$('.js-show-registration-btn').click(function() {
		$('.js-login').addClass('hidden');
		$('.js-registration').removeClass('hidden');
	});

	/* Show password */
	$('.js-show-password-btn').click(function() {
		$('.js-login').addClass('hidden');
		$('.js-password').removeClass('hidden');
	});

	/* Show login */
	$('.js-show-login-btn').click(function() {
		$('.js-login').removeClass('hidden');
		$('.js-registration').addClass('hidden');
		$('.js-password').addClass('hidden');
	});

	/* Open mobile catalog filter */
	$('.js-open-catalog-filter-btn').click(function() {
		$('.catalog-filter').addClass('catalog-filter--active');
		$('body').addClass('locked');
		$('.js-mobile-options').addClass('hidden');
		$('.header').addClass('header--active');
		$('.desktop-logo').addClass('hidden');
		$('.mobile-logo').removeClass('hidden');
	});

	/* Close mobile catalog filter */
	$('.js-close-catalog-filter-btn').click(function() {
		$('.catalog-filter').removeClass('catalog-filter--active');
		$('body').removeClass('locked');
		$('.js-mobile-options').removeClass('hidden');
		$('.header').removeClass('header--active');
		$('.desktop-logo').removeClass('hidden');
		$('.mobile-logo').addClass('hidden');
	});

	/* Favourite button */
	$('.fav-btn').click(function(e) {
		e.preventDefault();
		$(this)
			.closest('.fav-btn')
			.toggleClass('fav-btn--added');
	});

	/* Spoiler */
	$('.js-spoiler-btn').click(function(e) {
		const $btn = $(e.currentTarget);
		const $container = $btn.parents('.spoiler');
		const $wrapper = $container.find('.spoiler__inner');
		const $show = $container.find('.spoiler__show');
		const prevHeight = $wrapper.outerHeight();
		$wrapper.removeClass('spoiler__inner--active');
		$show.addClass('hidden');
		const nextHeight = $wrapper.outerHeight();
		anime({
			targets: $wrapper.get(0),
			height: [prevHeight, nextHeight],
			duration: animate.duration.effect,
			easing: animate.easing.standard,
		}).finished.then(() => {
			$wrapper.css({
				height: '',
			});
		});
	});

	/* Tabs */
	$('.js-tab').click(function(e) {
		const $btn = $(e.currentTarget);
		$('.tab').removeClass('tab--active');
		$('.tab-content').removeClass('tab-content--active');
		const $tabId = $btn.attr('data-tab');
		const tabContent = $("[id='" + $tabId + "']");
		$("[data-tab='" + $tabId + "']").addClass('tab--active');
		tabContent.addClass('tab-content--active');
	});

	/* Credit toggle checkbox */
	$('.js-credit-toggle').change(function(e) {
		const $check = $(e.currentTarget);
		const $notCheckedLbl = $check.parents('.toggle').find('.js-credit-toggle-label-not-checked');
		const $checkedLbl = $check.parents('.toggle').find('.js-credit-toggle-label-checked');
		if ($check.is(':checked')) {
			$('.js-hidden-by-toggle').removeClass('hidden');
			$notCheckedLbl.removeClass('toggle__label--checked');
			$checkedLbl.addClass('toggle__label--checked');
		} else {
			$('.js-hidden-by-toggle').addClass('hidden');
			$notCheckedLbl.addClass('toggle__label--checked');
			$checkedLbl.removeClass('toggle__label--checked');
		}
	});

	/* Close popup */
	if (mm('tabletLandscapeUp')) {
		$(document).mouseup((e) => {
			if (!$('.popup').is(e.target) && $('.popup').has(e.target).length === 0) {
				$('.popup-box').removeClass('popup-box--active');
				$('body').removeClass('locked');
			}
		});
	}
	$('.js-close-popup-btn').click(function() {
		$('.popup-box').removeClass('popup-box--active');
		$('body').removeClass('locked');
		toggleMobilePopup();
	});

	const toggleMobilePopup = function() {
		$('.header').toggleClass('header--active');
		$('.desktop-logo').toggleClass('hidden');
		$('.mobile-logo').toggleClass('hidden');
		$('.js-mobile-options').toggleClass('hidden');
	};

	/* Offer popup */
	$('.js-open-offer-popup-btn').click(function() {
		$('.js-offer-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			toggleMobilePopup();
		}
	});

	/* Credit calculator popup */
	$('.js-open-credit-calculator-popup-btn').click(function() {
		$('.js-credit-calculator-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			toggleMobilePopup();
		}
	});

	/* Leasing calculator popup */
	$('.js-open-leasing-calculator-popup-btn').click(function() {
		$('.js-leasing-calculator-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			toggleMobilePopup();
		}
	});

	/* Request call popup */
	$('.js-open-request-call-btn').click(function() {
		$('.js-request-call-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			toggleMobilePopup();
		}
	});

	/* Sign up popup */
	$('.js-sign-up-btn').click(function() {
		$('.js-sign-up-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			toggleMobilePopup();
		}
	});

	/* Leasing application popup */
	$('.js-open-leasing-app-popup-btn').click(function(e) {
		e.preventDefault();
		$('.js-leasing-app-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			toggleMobilePopup();
		}
	});

	/* Credit application popup */
	$('.js-open-credit-app-popup-btn').click(function(e) {
		e.preventDefault();
		$('.js-credit-app-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			toggleMobilePopup();
		}
	});

	/* Feedback popup */
	$('.js-open-feedback-popup-btn').click(function(e) {
		e.preventDefault();
		$('.js-feedback-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			toggleMobilePopup();
		}
	});

	/* Give feedback popup */
	$('.js-open-give-feedback-popup-btn').click(function(e) {
		e.preventDefault();
		$('.js-give-feedback-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			toggleMobilePopup();
		}
	});

	/* Vacancy popup */
	$('.js-open-vacancy-popup-btn').click(function(e) {
		e.preventDefault();
		$('.js-vacancy-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			toggleMobilePopup();
		}
	});

	/* Feedback sent popup */
	$('.js-open-feedback-sent-popup-btn').click(function(e) {
		e.preventDefault();
		$('.popup-box').removeClass('popup-box--active');
		$('.js-feedback-sent-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			toggleMobilePopup();
		}
	});

	/* Application accepted popup */
	$('.js-application-accepted-btn').click(function(e) {
		e.preventDefault();
		$('.popup-box').removeClass('popup-box--active');
		$('.js-application-accepted-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			toggleMobilePopup();
		}
	});

	/* Vacancy accepted popup */
	$('.js-vacancy-accepted-btn').click(function(e) {
		e.preventDefault();
		$('.popup-box').removeClass('popup-box--active');
		$('.js-vacancy-accepted-popup').addClass('popup-box--active');
		$('body').addClass('locked');
		if (mm('tabletLandscapeDown')) {
			toggleMobilePopup();
		}
	});

	/* Request form */
	$('.js-leave-request-btn').click(function(e) {
		e.preventDefault();
		$('.js-request-form').addClass('hidden');
		$('.js-request-box').removeClass('hidden');
	});

	/* Leasing calculate form */
	$('.js-calculate-btn').click(function(e) {
		e.preventDefault();
		$('.js-calculated-img').removeClass('hidden');
		$('.js-no-price').addClass('hidden');
		$('.js-price').removeClass('hidden');
	});

	/* Give feedback */
	$('.js-star')
		.on('mouseover', function() {
			const onStar = parseInt($(this).data('value'), 10);
			$(this)
				.parent()
				.children('.js-star')
				.each(function(e) {
					if (e < onStar) {
						$(this).addClass('star--hover');
					} else {
						$(this).removeClass('star--hover');
					}
				});
		})
		.on('mouseout', function() {
			$(this)
				.parent()
				.children('.js-star')
				.each(function(e) {
					$(this).removeClass('star--hover');
				});
		});
	$('.js-star').on('click', function() {
		const onStar = parseInt($(this).data('value'), 10);
		const stars = $(this)
			.parent()
			.children('.js-star');
		for (let i = 0; i < stars.length; i++) {
			$(stars[i]).removeClass('star--active');
		}
		for (let i = 0; i < onStar; i++) {
			$(stars[i]).addClass('star--active');
		}
	});

	/* Accordion */
	$('.accordion-set > button').on('click', function() {
		if ($(this).hasClass('accordion-set__link--active')) {
			$(this).removeClass('accordion-set__link--active');
			$(this)
				.parents('.accordion-set')
				.removeClass('accordion-set--active');
			$(this)
				.siblings('.accordion-set__content')
				.slideUp(200);
		} else {
			$('.accordion-set > button').removeClass('accordion-set__link--active');
			$('.accordion-set').removeClass('accordion-set--active');
			$(this)
				.parents('.accordion-set')
				.addClass('accordion-set--active');
			$(this).addClass('accordion-set__link--active');
			$('.accordion-set__content').slideUp(200);
			$(this)
				.siblings('.accordion-set__content')
				.slideDown(200);
		}
	});

	// IE11 detection
	if (/MSIE/.test(window.navigator.userAgent) || /Trident/.test(window.navigator.userAgent)) {
		$('body').addClass('msie');
	}

	// Polyfills
	objectFitImages();

	// https://www.thecssninja.com/javascript/pointer-events-60fps
	let peTimer;
	$(window).on('scroll', () => {
		if (peTimer) {
			clearTimeout(peTimer);
			peTimer = undefined;
		}
		const $body = $('body');
		if (!$body.hasClass('disable-hover')) {
			$body.addClass('disable-hover');
		}
		peTimer = setTimeout(() => {
			$body.removeClass('disable-hover');
		}, 100);
	});
});

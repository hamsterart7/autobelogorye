var gulp = require('gulp');
var sass = require('gulp-sass');
var combineMq = require('gulp-combine-mq');

gulp.task('sass', function () {
    gulp.src('./scss/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(sass({outputStyle: 'compressed'}))
        .pipe(gulp.dest('./css'));
});

gulp.task('sass:watch', function () {
    gulp.watch('./scss/**/*.scss', ['sass']);
});

gulp.task('combineMq', function () {
    return gulp.src('css/styles.css')
        .pipe(combineMq({
            beautify: false
        }))
        .pipe(gulp.dest('css'));
});
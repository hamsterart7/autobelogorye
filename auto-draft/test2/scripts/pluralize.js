export default (count, words) => {
	/* eslint-disable no-magic-numbers */
	count = Math.abs(count) % 100;
	const count10 = count % 10;
	if (count > 10 && count < 20) {
		return words[2];
	}
	if (count10 > 1 && count10 < 5) {
		return words[1];
	}
	if (count10 === 1) {
		return words[0];
	}
	return words[2];
	/* eslint-enable no-magic-numbers */
};

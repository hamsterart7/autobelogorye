/* global $, i18n */

import pluralize from '../pluralize.js';

$(() => {
	$('body').on('change', '.file-input input[type="file"]', (e) => {
		const $input = $(e.currentTarget);
		const $label = $input.parents('.file-input').find('.file-input__label');
		if ($input.attr('multiple')) {
			const count = e.target.files.length;
			$label.text(`${count} ${pluralize(count, i18n.fileInput)}`);
		} else {
			$label.text(e.target.files[0].name);
		}
	});
});

/* global $ */

import animate from '../animate.js';

$(() => {
	$('body').on('click', '[data-scroll]', (e) => {
		e.preventDefault();
		const $link = $(e.currentTarget);
		let index = $link.data('scrollIndex');
		if (index === undefined) {
			index = 0;
		}
		const $el = $($link.data('scroll')).eq(index);
		$('html, body').animate(
			{
				// eslint-disable-next-line no-magic-numbers
				scrollTop: $el.offset().top - ($(window).height() * 10) / 100,
			},
			{
				duration: animate.duration.effect,
			}
		);
	});
});

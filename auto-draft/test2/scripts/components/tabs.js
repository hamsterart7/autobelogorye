/* global $ */

$(() => {
	$('.tabs').each((i, el) => {
		const $tabs = $(el);
		$tabs.find('.tabs__button').on('click', (e) => {
			const $button = $(e.currentTarget);
			const index = $button.parents('.tabs__nav-item').index();
			$tabs.find('.tabs__button').prop('disabled', false);
			$button.prop('disabled', true);
			$tabs.find('.tabs__content').each((i, el) => {
				$(el).prop('hidden', i !== index);
			});
		});
	});
});

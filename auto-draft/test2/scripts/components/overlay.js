/* global $ */

import anime from 'animejs';
import animate from '../animate.js';

const show = ($overlay) => {
	$('body').css({
		overflow: 'hidden',
	});
	$overlay.prop('hidden', false);
	anime({
		targets: $overlay.get(0),
		opacity: [0, 1],
		duration: animate.duration.enter,
		easing: animate.easing.deceleration,
	}).finished.then(() => {
		const $required = $overlay.find('[required]');
		if ($required.length > 0) {
			$required.first().trigger('focus');
		}
	});
};

const hide = ($overlay) => {
	anime({
		targets: $overlay.get(0),
		opacity: [1, 0],
		duration: animate.duration.leave,
		easing: animate.easing.acceleration,
	}).finished.then(() => {
		$overlay.prop('hidden', true);
		$('body').css({
			overflow: '',
		});
	});
};

export const showOverlay = ($el) => {
	show($el);
};

export const hideOverlay = ($el) => {
	hide($el);
};

$(() => {
	$('body')
		.on('click', '[data-overlay]', (e) => {
			e.preventDefault();
			// Safari так же ищет по id иконок, поэтому фильтруем по .overlay
			const $overlay = $(`${$(e.currentTarget).data('overlay')}.overlay`);
			show($overlay);
		})
		.on('click', '[data-overlay-close]', (e) => {
			const $overlay = $(e.currentTarget).parents('.overlay');
			hide($overlay);
		})
		.on('click', (e) => {
			if ($(e.target).hasClass('overlay')) {
				hide($(e.target));
			}
		})
		.on('keyup', (e) => {
			const esc = 27;
			if (e.keyCode === esc) {
				$('.overlay').each((i, el) => {
					const $el = $(el);
					if (!$el.prop('hidden')) {
						hide($el);
					}
				});
			}
		});
});

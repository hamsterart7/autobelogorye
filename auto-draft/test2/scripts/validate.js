/* global $, i18n */

import template from './template.js';

const humanBytes = (bytes) => {
	/* eslint-disable no-magic-numbers */
	const kilobytes = bytes / 1024;
	if (kilobytes < 1) {
		return `${bytes} ${i18n.units.byte}`;
	}
	if (kilobytes < 1024) {
		return `${kilobytes} ${i18n.units.kilobyte}`;
	}
	return `${kilobytes / 1024} ${i18n.units.megabyte}`;
	/* eslint-enable */
};

export default ($form, droppedFiles) => {
	const report = {valid: true, fields: []};
	$form.find('input:not([type="hidden"]), textarea').each((i, el) => {
		const $el = $(el);
		const errors = [];
		// Проверка на атрибут required
		if (el.validity.valueMissing) {
			if (el.type === 'file') {
				if (!droppedFiles || droppedFiles.length === 0) {
					errors.push(template(i18n.validate.required));
				}
			} else {
				errors.push(template(i18n.validate.required));
			}
		}
		// Проверка на мин. кол-во символов
		if (el.validity.tooShort) {
			errors.push(template(i18n.validate.minlength, {min: $el.attr('minlength')}));
		}
		// Проверка на мин. кол-во символов для полей с масками
		const telLengthWithMask = 16;
		const dateLengthWithMask = 10;
		if (
			(!el.validity.valueMissing &&
				el.type === 'tel' &&
				el.value.length > 0 &&
				el.value.length < telLengthWithMask) ||
			(!el.validity.valueMissing &&
				el.type === 'text' &&
				$el.attr('type') === 'date' &&
				el.value.length > 0 &&
				el.value.length < dateLengthWithMask)
		) {
			errors.push(template(i18n.validate.minWithMask));
		}
		// Проверка на эл. почту
		if (el.type === 'email' && el.validity.typeMismatch) {
			errors.push(i18n.validate.email);
		}
		// Проверка на URL
		if (
			el.type === 'url' &&
			el.value.length > 0 &&
			!/^[a-zа-я0-9-]+\.[a-zа-я]{2,}$/i.test(el.value.replace(/https?:\/\//i, ''))
		) {
			errors.push(i18n.validate.domain);
		}
		// Проверка на минимальное значение для даты
		if (el.type === 'date' && el.validity.rangeUnderflow) {
			const min = new Intl.DateTimeFormat().format(new Date(el.min));
			errors.push(template(i18n.validate.minDate, {min}));
		}
		// Проверка на максимальное значение для даты
		if (el.type === 'date' && el.validity.rangeOverflow) {
			const max = new Intl.DateTimeFormat().format(new Date(el.max));
			errors.push(template(i18n.validate.maxDate, {max}));
		}
		// Проверка на минимальное значение для чисел
		if (el.type === 'number' && el.validity.rangeUnderflow) {
			errors.push(template(i18n.validate.minNumber, {min: $el.attr('min')}));
		}
		if (el.type === 'number' && el.validity.rangeOverflow) {
			errors.push(template(i18n.validate.maxNumber, {max: $el.attr('max')}));
		}
		// Проверка на максимальный размер файла
		if (el.type === 'file') {
			Object.values(el.files).forEach((file) => {
				if (file.size > $el.data('maxBytes')) {
					const max = humanBytes($el.data('maxBytes'));
					errors.push(template(i18n.validate.maxFile, {max}));
				}
			});
		}
		// Если есть хоть какие-то ошибки - форма не валидна
		if (errors.length > 0) {
			report.valid = false;
		}
		// Добавляем найденные ошибки в отчёт
		report.fields.push({
			name: el.name,
			errors,
		});
	});
	return report;
};

/* global $, i18n */

import 'formdata-polyfill';
import 'svgxuse';
import IMask from 'imask';
import objectFitImages from 'object-fit-images';
import './anitabs.min';

import './lazyload.js';
import animate from './animate';

const i18n = {
	masks: {
		tel: '+{7} 000 000-00-00',
	},
	units: {
		byte: 'Б',
		kilobyte: 'КБ',
		megabyte: 'МБ',
	},
	fileInput: ['файл', 'файла', 'файлов'],
	validate: {
		required: 'Обязательно для заполнения',
		minlength: 'Минимальное кол-во символов — {{min}}',
		minWithMask: 'Заполнено не до конца',
		email: 'Нужно ввести настоящую эл. почту',
		url: 'Нужно ввести настоящее имя домена',
		minDate: 'Минимальная дата — {{min}}',
		maxDate: 'Максимальная дата — {{max}}',
		minNumber: 'Минимум {{min}}',
		maxNumber: 'Максимум {{max}}',
		maxFile: 'Максимум {{max}}',
	},
};

document.addEventListener('DOMContentLoaded', function() {
	const lazyVideos = [].slice.call(document.querySelectorAll('video.lazy'));

	if ('IntersectionObserver' in window) {
		const lazyVideoObserver = new IntersectionObserver(function(entries, observer) {
			entries.forEach(function(video) {
				if (video.isIntersecting) {
					for (const source in video.target.children) {
						const videoSource = video.target.children[source];
						if (typeof videoSource.tagName === 'string' && videoSource.tagName === 'SOURCE') {
							videoSource.src = videoSource.dataset.src;
						}
					}

					video.target.load();
					/*video.target.play();*/
					video.target.classList.remove('lazy');
					lazyVideoObserver.unobserve(video.target);
				}
			});
		});

		lazyVideos.forEach(function(lazyVideo) {
			lazyVideoObserver.observe(lazyVideo);
		});
	}
});
document.addEventListener('DOMContentLoaded', function() {
	const lazyImages = [].slice.call(document.querySelectorAll('img.lazy'));

	if ('IntersectionObserver' in window) {
		const lazyImageObserver = new IntersectionObserver(function(entries, observer) {
			entries.forEach(function(entry) {
				if (entry.isIntersecting) {
					const lazyImage = entry.target;
					lazyImage.src = lazyImage.dataset.src;
					lazyImage.srcset = lazyImage.dataset.srcset;
					lazyImage.classList.remove('lazy');
					lazyImageObserver.unobserve(lazyImage);
				}
			});
		});

		lazyImages.forEach(function(lazyImage) {
			lazyImageObserver.observe(lazyImage);
		});
	} else {
		// Possibly fall back to a more compatible method here
	}
});

const $window = $(window);
const mm = (device) => {
	let width = null;
	switch (device) {
		case 'tabletLandscapeUp':
			width = '(min-width: 960px)';
			break;
		case 'tabletLandscapeDown':
			width = '(max-width: 959px)';
			break;
	}
	return window.matchMedia(width).matches;
};

const section5Anim = {
	init: function() {
		const $section = $('.advantages');

		const leftBlock = $('.advantages__info');

		function func() {
			const winH = $window.height();
			leftBlock.outerHeight(winH);
			const sectH = $section.outerHeight();

			const sectTop = $section.offset().top;

			const scrollT = $window.scrollTop();

			const endPos = sectTop + sectH;

			const $block1 = $('.advantages__item--first');

			const $block2 = $('.advantages__item--second');
			const progress = (scrollT + winH / 2 - sectTop) / sectH;
			if (scrollT >= sectTop && scrollT + winH < endPos) {
				leftBlock.addClass('fixed');
				$block1.stop(true, false);
				$block2.stop(true, false);
				if (progress > 0.25 && progress < 0.5) {
					$block2.hide();
					$block1.show().css('opacity', 1 - (progress - 0.25) / 0.25);
				} else if (progress > 0.5 && progress <= 0.75) {
					$block1.hide();
					$block2.show().css('opacity', (progress - 0.5) / 0.25);
				}
			} else if (scrollT >= sectTop && scrollT + winH >= endPos) {
				leftBlock.removeClass('fixed').addClass('bottom');
				$block1.hide();
				$block2.show().css('opacity', 1);
			} else {
				leftBlock.removeClass('fixed bottom');
				$block2.hide();
				$block1.show().css('opacity', 1);
			}
		}

		func();
		$window.resize(func);
		$window.scroll(func);
	},
};
$(window).on('load', function() {
	setTimeout(function() {
		$('.preloader').addClass('hidden');
	}, 300);
});
/*$(window).load(function() {
    setTimeout(function() {
        $('.preloader').addClass('hidden');
    }, 300);
});*/
$(document).ready(function() {
	const $flatType = $('.flat-type');
	const $openFlatBtn = $('.open-flat-btn');
	const $toggle = $('.toggle__input');
	const $toggleLevel = $('.toggle-level');
	const $toggleFurniture = $('.toggle-furniture');
	const $btnFood = $('.btn-food');
	const $btnToy = $('.btn-toy');
	const $btnHeart = $('.btn-heart');
	const $btnBook = $('.btn-book');
	const bookMarkers = [];
	const foodMarkers = [];
	const toyMarkers = [];
	const heartMarkers = [];
	section5Anim.init();

	const $container = $('.content');

	const controller = new ScrollMagic.Controller();

	const responsiveGallery = () => {
		if (mm('tabletLandscapeUp')) {
			if ($container.find('.large-gallery-hor').length > 0) {
				$container.find('.large-gallery-hor').each((i, el) => {
					const $slider = $(el);
					const $slides = $slider.find('.large-gallery__item');
					const wipeAnimation = new TimelineMax();
					$slides.each((i, el) => {
						if ($(el).index() !== 0) {
							wipeAnimation.fromTo(
								$(el),
								1,
								{x: '100%'},
								{
									x: '0%',
									ease: Linear.easeNone,
									onStart: function() {
										$slides[i - 1].classList.add('large-gallery__item--changed');
										$slides[i - 1].classList.add('large-gallery__item--changed-delay');
									},
									onComplete: function() {
										$slides[i - 1].classList.remove('large-gallery__item--changed');
										$slides[i - 1].classList.remove('large-gallery__item--changed-delay');
									},
									onReverseComplete: function() {
										$slides[i - 1].classList.remove('large-gallery__item--changed');
										$slides[i - 1].classList.remove('large-gallery__item--changed-delay');
									},
								}
							);
						}
					});
					new ScrollMagic.Scene({
						triggerElement: el,
						triggerHook: 'onLeave',
						duration: '100%',
					})
						.setPin(el)
						.setTween(wipeAnimation)
						.addTo(controller);
				});
			}

			if ($container.find('.large-gallery-ver').length > 0) {
				$container.find('.large-gallery-ver').each((i, el) => {
					const $slider = $(el);
					const $slides = $slider.find('.large-gallery__item');
					const wipeAnimation = new TimelineMax();
					$slides.each((i, el) => {
						if ($(el).index() !== 0) {
							wipeAnimation.fromTo(
								$(el),
								1,
								{y: '100%'},
								{
									y: '0%',
									ease: Linear.easeNone,
									onStart: function() {
										$slides[i - 1].classList.add('large-gallery__item--changed');
										$slides[i - 1].classList.add('large-gallery__item--changed-delay');
									},
									onComplete: function() {
										$slides[i - 1].classList.remove('large-gallery__item--changed');
										$slides[i - 1].classList.remove('large-gallery__item--changed-delay');
									},
									onReverseComplete: function() {
										$slides[i - 1].classList.remove('large-gallery__item--changed');
										$slides[i - 1].classList.remove('large-gallery__item--changed-delay');
									},
								}
							);
						}
					});
					new ScrollMagic.Scene({
						triggerElement: el,
						triggerHook: 'onLeave',
						duration: '100%',
					})
						.setPin(el)
						.setTween(wipeAnimation)
						.addTo(controller);
				});
			}
		}
		if (mm('tabletLandscapeDown')) {
			if ($container.find('.large-gallery').length > 0) {
				$container.find('.large-gallery').each((i, el) => {
					const $slider = $(el);
					new Swiper($slider.find('.large-gallery__container').get(0), {
						direction: 'horizontal',
						slidesPerView: 'auto',
						pagination: {
							el: '.swiper-pagination',
							clickable: true,
						},
					});
				});
			}
		}
	};

	responsiveGallery();
	$(window).on('resize', () => {
		responsiveGallery();
	});

	const countDownDate = new Date('Dec 25, 2019 00:00:00').getTime();
	const x = setInterval(function() {
		const $minuteOld = $('.minute').text();
		const $hourOld = $('.hour').text();
		const $dayOld = $('.day').text();
		$('.minute').removeClass('changed');
		$('.hour').removeClass('changed');
		$('.day').removeClass('changed');
		const now = new Date().getTime();
		const distance = countDownDate - now;
		const days = Math.floor(distance / (1000 * 60 * 60 * 24));
		const hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
		const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
		if ($container.find('.countdown').length > 0) {
			$('.day').text(days);
			$('.hour').text(hours);
			$('.minute').text(minutes);

			if (distance < 0) {
				clearInterval(x);
				$('.day').text('00');
				$('.hour').text('00');
				$('.minute').text('00');
			}
		}
		if ($minuteOld !== $('.minute').text()) {
			$('.minute').addClass('changed');
		}
		if ($hourOld !== $('.hour').text()) {
			$('.hour').addClass('changed');
		}
		if ($dayOld !== $('.day').text()) {
			$('.day').addClass('changed');
		}
	}, 1000);

	$('.js-tabs-link').aniTabs({
		animation: 'fade',
		autoHeight: true,
	});

	$('.flats-tabs__link').click(function() {
		$toggle.prop('checked', true);
		if ($('#tab-1').hasClass('active')) {
			$flatType.text('двухкомнатным квартирам');
			$toggleLevel.prop('disabled', true);
			$openFlatBtn.text('ВЫБРАТЬ КВАРТИРУ');
		} else if ($('#tab-2').hasClass('active')) {
			$flatType.text('трехкомнатным квартирам');
			$toggleLevel.prop('disabled', true);
			$openFlatBtn.text('ВЫБРАТЬ КВАРТИРУ');
		} else if ($('#tab-3').hasClass('active')) {
			$flatType.text('четырехкомнатным квартирам');
			$toggleLevel.prop('disabled', true);
			$openFlatBtn.text('ВЫБРАТЬ КВАРТИРУ');
		} else if ($('#tab-4').hasClass('active')) {
			$flatType.text('пентхаусам');
			$toggleLevel.prop('disabled', false);
			$openFlatBtn.text('СДЕЛАТЬ ЗАПРОС');
		}
	});

	if ($container.find('.offer-slider').length > 0) {
		$container.find('.offer-slider').each((i, el) => {
			const $slider = $(el);
			new Swiper($slider.find('.offer-slider__container').get(0), {
				direction: 'horizontal',
				slidesPerView: 1,
				autoplay: true,
				effect: 'fade',
				speed: 2000,
			});
		});
	}
	if ($container.find('.a-slider').length > 0) {
		$container.find('.a-slider').each((i, el) => {
			const $slider = $(el);
			new Swiper($slider.find('.a-slider__container').get(0), {
				direction: 'horizontal',
				slidesPerView: 'auto',
				spaceBetween: 50,
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				},
				breakpoints: {
					960: {
						spaceBetween: 15,
					},
				},
			});
		});
	}
	if ($container.find('.h-slider').length > 0) {
		$container.find('.h-slider').each((i, el) => {
			const $slider = $(el);
			new Swiper($slider.find('.h-slider__container').get(0), {
				direction: 'horizontal',
				slidesPerView: 'auto',
				noSwiping: true,
				allowSlidePrev: true,
				allowSlideNext: true,
				breakpoints: {
					678: {
						noSwiping: false,
						allowSlidePrev: false,
						allowSlideNext: false,
						autoplay: false,
						keyboard: false,
					},
				},
			});
		});
	}
	if ($container.find('.flat-slider').length > 0) {
		$container.find('.flat-slider').each((i, el) => {
			const $slider = $(el);
			const flatsSlider = new Swiper($slider.find('.flat-slider__container').get(0), {
				slidesPerView: 1,
				/*effect: 'fade',*/
				navigation: {
					nextEl: '.swiper-button-next',
					prevEl: '.swiper-button-prev',
				},
				pagination: {
					el: '.swiper-pagination',
					clickable: true,
				},
			});
			flatsSlider.on('slideChange', function() {
				if ($toggle !== undefined) {
					$toggle.prop('checked', true);
				}
			});
		});
	}
	let galleryThumbs;
	if ($container.find('.gallery').length > 0) {
		$container.find('.gallery').each((i, el) => {
			const $slider = $(el);
			galleryThumbs = new Swiper($slider.find('.gallery__thumbs').get(0), {
				slidesPerView: 2,
				freeMode: true,
				watchSlidesVisibility: true,
				watchSlidesProgress: true,
			});
		});
	}
	if ($container.find('.gallery').length > 0) {
		$container.find('.gallery').each((i, el) => {
			const $slider = $(el);
			new Swiper($slider.find('.gallery__top').get(0), {
				direction: 'horizontal',
				slidesPerView: 1,
				effect: 'fade',
				pagination: {
					el: '.swiper-pagination',
					clickable: true,
				},
				thumbs: {
					swiper: galleryThumbs,
				},
			});
		});
	}
	if ($container.find('.step-thumbs').length > 0) {
		$container.find('.step-thumbs').each((i, el) => {
			const $slider = $(el);
			new Swiper($slider.find('.step-thumbs__container').get(0), {
				slidesPerView: 'auto',
				watchSlidesVisibility: true,
				watchSlidesProgress: true,
				centeredSlides: true,
			});
		});
	}
	if ($container.find('.map-box').length > 0) {
		const map = new google.maps.Map($('.map').get(0), {
			zoom: 16,
			center: {lat: 53.2039842, lng: 50.1121963},
			scrollwheel: false,
			disableDefaultUI: true,
			styles: [
				{
					featureType: 'all',
					elementType: 'labels.text.fill',
					stylers: [
						{
							saturation: 36,
						},
						{
							color: '#333333',
						},
						{
							lightness: 40,
						},
					],
				},
				{
					featureType: 'all',
					elementType: 'labels.text.stroke',
					stylers: [
						{
							visibility: 'on',
						},
						{
							color: '#ffffff',
						},
						{
							lightness: 16,
						},
					],
				},
				{
					featureType: 'all',
					elementType: 'labels.icon',
					stylers: [
						{
							visibility: 'off',
						},
					],
				},
				{
					featureType: 'administrative',
					elementType: 'geometry.fill',
					stylers: [
						{
							color: '#fefefe',
						},
						{
							lightness: 20,
						},
					],
				},
				{
					featureType: 'administrative',
					elementType: 'geometry.stroke',
					stylers: [
						{
							color: '#fefefe',
						},
						{
							lightness: 17,
						},
						{
							weight: 1.2,
						},
					],
				},
				{
					featureType: 'landscape',
					elementType: 'geometry',
					stylers: [
						{
							color: '#f5f5f5',
						},
						{
							lightness: 20,
						},
					],
				},
				{
					featureType: 'poi',
					elementType: 'geometry',
					stylers: [
						{
							color: '#f5f5f5',
						},
						{
							lightness: 21,
						},
					],
				},
				{
					featureType: 'poi.park',
					elementType: 'geometry',
					stylers: [
						{
							color: '#dedede',
						},
						{
							lightness: 21,
						},
					],
				},
				{
					featureType: 'road.highway',
					elementType: 'geometry.fill',
					stylers: [
						{
							color: '#ffffff',
						},
						{
							lightness: 17,
						},
					],
				},
				{
					featureType: 'road.highway',
					elementType: 'geometry.stroke',
					stylers: [
						{
							color: '#ffffff',
						},
						{
							lightness: 29,
						},
						{
							weight: 0.2,
						},
					],
				},
				{
					featureType: 'road.arterial',
					elementType: 'geometry',
					stylers: [
						{
							color: '#ffffff',
						},
						{
							lightness: 18,
						},
					],
				},
				{
					featureType: 'road.local',
					elementType: 'geometry',
					stylers: [
						{
							color: '#ffffff',
						},
						{
							lightness: 16,
						},
					],
				},
				{
					featureType: 'transit',
					elementType: 'geometry',
					stylers: [
						{
							color: '#f2f2f2',
						},
						{
							lightness: 19,
						},
					],
				},
				{
					featureType: 'water',
					elementType: 'geometry',
					stylers: [
						{
							color: '#e9e9e9',
						},
						{
							lightness: 17,
						},
					],
				},
			],
		});
		const food = [
			{lat: 53.2035635, lng: 50.1048914},
			{lat: 53.2049802, lng: 50.1072963},
			{lat: 53.2057382, lng: 50.1068783},
			{lat: 53.2060177, lng: 50.1110244},
			{lat: 53.2075732, lng: 50.1110817},
			{lat: 53.2085112, lng: 50.1136863},
			{lat: 53.2098162, lng: 50.1162173},
			{lat: 53.2110502, lng: 50.1188783},
			{lat: 53.2009772, lng: 50.1047823},
			{lat: 53.1985922, lng: 50.1066163},
			{lat: 53.1996125, lng: 50.1107709},
			{lat: 53.2023402, lng: 50.1136153},
			{lat: 53.2047372, lng: 50.1137731},
			{lat: 53.2037912, lng: 50.1239953},
			{lat: 53.2039642, lng: 50.1257973},
		];
		for (let i = 0; i < food.length; i++) {
			foodMarkers[i] = new google.maps.Marker({
				position: food[i],
				map: map,
				icon: 'images/food-pin.png',
			});
		}
		const toy = [
			{lat: 53.2016092, lng: 50.1075603},
			{lat: 53.1977592, lng: 50.1120603},
			{lat: 53.1987362, lng: 50.1143453},
			{lat: 53.2049772, lng: 50.1196273},
			{lat: 53.2051892, lng: 50.1179433},
			{lat: 53.2056712, lng: 50.1183723},
		];
		for (let i = 0; i < toy.length; i++) {
			toyMarkers[i] = new google.maps.Marker({
				position: toy[i],
				map: map,
				icon: 'images/toy-pin.png',
			});
		}
		const heart = [
			{lat: 53.2024237, lng: 50.1116425},
			{lat: 53.2021812, lng: 50.1167473},
			{lat: 53.2018899, lng: 50.122916},
		];
		for (let i = 0; i < heart.length; i++) {
			heartMarkers[i] = new google.maps.Marker({
				position: heart[i],
				map: map,
				icon: 'images/heart-pin.png',
			});
		}
		const book = [
			{lat: 53.2037542, lng: 50.1224153},
			{lat: 53.2011862, lng: 50.1201423},
			{lat: 53.2013992, lng: 50.1123093},
		];
		for (let i = 0; i < book.length; i++) {
			bookMarkers[i] = new google.maps.Marker({
				position: book[i],
				map: map,
				icon: 'images/book-pin.png',
			});
		}
		new google.maps.Marker({
			position: {lat: 53.204726, lng: 50.119775},
			map: map,
			icon: 'images/large-pin.png',
		});
		new google.maps.Marker({
			position: {lat: 53.203884, lng: 50.118144},
			map: map,
			icon: 'images/pin.png',
		});
		const largePolygonCoords = [
			{lat: 53.2036967, lng: 50.1201814},
			{lat: 53.2045321, lng: 50.1190656},
			{lat: 53.2051779, lng: 50.1204711},
			{lat: 53.2043232, lng: 50.1216298},
			{lat: 53.2036967, lng: 50.1201814},
		];
		const largePolygonInnerCoords = [
			{lat: 53.2041304, lng: 50.1201975},
			{lat: 53.2044935, lng: 50.1197094},
			{lat: 53.2047634, lng: 50.1202941},
			{lat: 53.2043875, lng: 50.1207823},
			{lat: 53.2041304, lng: 50.1202029},
		];
		const polygonCoords = [
			{lat: 53.2044967, lng: 50.1189584},
			{lat: 53.2035456, lng: 50.116877},
			{lat: 53.2032565, lng: 50.117231},
			{lat: 53.2038284, lng: 50.1185238},
			{lat: 53.203475, lng: 50.1189208},
			{lat: 53.20325, lng: 50.118438},
			{lat: 53.203038, lng: 50.118717},
			{lat: 53.2036453, lng: 50.1200527},
			{lat: 53.2044967, lng: 50.1189584},
		];

		const largePolygon = new google.maps.Polygon({
			paths: largePolygonCoords,
			strokeColor: '#b4987a',
			strokeWeight: 1,
			fillColor: '#b4987a',
			fillOpacity: 0.25,
		});
		const largePolygonInner = new google.maps.Polygon({
			paths: largePolygonInnerCoords,
			strokeColor: '#fefefe',
			strokeWeight: 2,
			fillColor: '#fefefe',
			fillOpacity: 1,
		});
		const polygon = new google.maps.Polygon({
			paths: polygonCoords,
			strokeColor: '#b4987a',
			strokeWeight: 1,
			fillColor: '#b4987a',
			fillOpacity: 0.25,
		});
		largePolygon.setMap(map);
		largePolygonInner.setMap(map);
		polygon.setMap(map);
	}
	$('body')
		.on('click', '.btn-food', (e) => {
			e.preventDefault();
			$btnFood.addClass('map-info__btn--hidden');
			$btnFood.addClass('btn-food-hidden');
			for (let i = 0; i < foodMarkers.length; i++) {
				foodMarkers[i].setVisible(false);
			}
		})
		.on('click', '.btn-food-hidden', (e) => {
			e.preventDefault();
			$btnFood.removeClass('map-info__btn--hidden');
			$btnFood.removeClass('btn-food-hidden');
			for (let i = 0; i < foodMarkers.length; i++) {
				foodMarkers[i].setVisible(true);
			}
		});
	$('body')
		.on('click', '.btn-toy', (e) => {
			e.preventDefault();
			$btnToy.addClass('map-info__btn--hidden');
			$btnToy.addClass('btn-toy-hidden');
			for (let i = 0; i < toyMarkers.length; i++) {
				toyMarkers[i].setVisible(false);
			}
		})
		.on('click', '.btn-toy-hidden', (e) => {
			e.preventDefault();
			$btnToy.removeClass('map-info__btn--hidden');
			$btnToy.removeClass('btn-toy-hidden');
			for (let i = 0; i < toyMarkers.length; i++) {
				toyMarkers[i].setVisible(true);
			}
		});
	$('body')
		.on('click', '.btn-heart', (e) => {
			e.preventDefault();
			$btnHeart.addClass('map-info__btn--hidden');
			$btnHeart.addClass('btn-heart-hidden');
			for (let i = 0; i < heartMarkers.length; i++) {
				heartMarkers[i].setVisible(false);
			}
		})
		.on('click', '.btn-heart-hidden', (e) => {
			e.preventDefault();
			$btnHeart.removeClass('map-info__btn--hidden');
			$btnHeart.removeClass('btn-heart-hidden');
			for (let i = 0; i < heartMarkers.length; i++) {
				heartMarkers[i].setVisible(true);
			}
		});
	$('body')
		.on('click', '.btn-book', (e) => {
			e.preventDefault();
			$btnBook.addClass('map-info__btn--hidden');
			$btnBook.addClass('btn-book-hidden');
			for (let i = 0; i < bookMarkers.length; i++) {
				bookMarkers[i].setVisible(false);
			}
		})
		.on('click', '.btn-book-hidden', (e) => {
			e.preventDefault();
			$btnBook.removeClass('map-info__btn--hidden');
			$btnBook.removeClass('btn-book-hidden');
			for (let i = 0; i < bookMarkers.length; i++) {
				bookMarkers[i].setVisible(true);
			}
		});

	jQuery(function() {
		$('.toggle__input').change(function() {
			if ($('.flats-content').hasClass('active')) {
				const $slide = $('.flats-content.active').find('.swiper-slide-active');
				const $imgF1 = $slide.find('.img-furniture-one');
				const $imgF2 = $slide.find('.img-furniture-two');
				const $imgE1 = $slide.find('.img-empty-one');
				const $imgE2 = $slide.find('.img-empty-two');
				$('.toggle__input').each(function() {
					if ($toggleFurniture[0].checked && $toggleLevel[0].checked) {
						$imgE1.css('display', 'none');
						$imgE2.css('display', 'none');
						$imgF1.css('display', 'inline-block');
						$imgF2.css('display', 'none');
					} else if (!$toggleFurniture[0].checked && $toggleLevel[0].checked) {
						$imgE1.css('display', 'inline-block');
						$imgE2.css('display', 'none');
						$imgF1.css('display', 'none');
						$imgF2.css('display', 'none');
					} else if ($toggleFurniture[0].checked && !$toggleLevel[0].checked) {
						$imgE1.css('display', 'none');
						$imgE2.css('display', 'none');
						$imgF1.css('display', 'none');
						$imgF2.css('display', 'inline-block');
					} else if (!$toggleFurniture[0].checked && !$toggleLevel[0].checked) {
						$imgE1.css('display', 'none');
						$imgE2.css('display', 'inline-block');
						$imgF1.css('display', 'none');
						$imgF2.css('display', 'none');
					}
				});
			}
		});
	});
});

$(() => {
	$('body')
		.on('click', '.menu-btn', (e) => {
			e.preventDefault();
			$('.menu').addClass('menu--active');
			$('.menu-btn').addClass('menu-btn--close');
		})
		.on('click', '.menu-btn--close', (e) => {
			e.preventDefault();
			$('.menu').removeClass('menu--active');
			$('.menu-btn').removeClass('menu-btn--close');
		});
	const $header = $('.header');
	const $menu = $('.menu');
	$(document).mouseup((e) => {
		if (!$header.is(e.target) && $header.has(e.target).length === 0) {
			$menu.removeClass('menu--active');
			$('.menu-btn').removeClass('menu-btn--close');
		}
	});
	$('body').on('click', '[data-scroll]', (e) => {
		e.preventDefault();
		$('.menu').removeClass('menu--active');
		$('.menu-btn').removeClass('menu-btn--close');
		const $link = $(e.currentTarget);
		let index = $link.data('scrollIndex');
		if (index === undefined) {
			index = 0;
		}
		const $el = $($link.data('scroll')).eq(index);
		$('html, body').animate(
			{
				// eslint-disable-next-line no-magic-numbers
				scrollTop: $el.offset().top - ($(window).height() * 1) / 100,
			},
			{
				duration: animate.duration.effect,
			}
		);
	});

	const $popupNews = $('.news-popup');
	const $popupBell = $('.popup-bell');
	const $popupFlat = $('.popup-flat');
	const $popupOffer = $('.popup-offer');
	$('.open-flat-btn').click(function() {
		$popupFlat.addClass('popup--active');
		$('body').css('overflow', 'hidden');
	});
	$('.popup-flat__close').click(function() {
		$popupFlat.removeClass('popup--active');
		$('body').css('overflow', '');
	});
	$('.open-offer-btn').click(function() {
		$popupOffer.addClass('popup--active');
		$('body').css('overflow', 'hidden');
	});
	$('.popup-offer__close').click(function() {
		$popupOffer.removeClass('popup--active');
		$('body').css('overflow', '');
	});
	$('.open-bell-btn').click(function(e) {
		$popupBell.addClass('popup--active');
		$('body').css('overflow', 'hidden');
	});
	$('.popup-bell__close').click(function() {
		$popupBell.removeClass('popup--active');
		$('body').css('overflow', '');
	});
	$('.send-request-btn').click(function(e) {
		e.preventDefault();
		const $button = $(e.currentTarget);
		const $popup = $button.parents('.popup');
		const $check = $button.parents('.form').find('.checkbox__hidden-input');
		const $checkbox = $button.parents('.form').find('.checkbox');
		const $field = $button.parents('.form').find('.phone-field');
		const $fieldBox = $button.parents('.form').find('.phone-field-box');
		if ($field.val().length !== 16 && !$check.prop('checked')) {
			$fieldBox.addClass('field--error');
			$button.addClass('btn--red');
			$button.text('ВВЕДИТЕ НОМЕР ТЕЛЕФОНА');
			$field.attr('placeholder', 'Ошибка при вводе данных');
			$checkbox.addClass('checkbox--error');
		} else if ($field.val().length === 16 && !$check.prop('checked')) {
			$checkbox.addClass('checkbox--error');
			$fieldBox.removeClass('field--error');
			$button.removeClass('btn--red');
			$button.text('ОТПРАВИТЬ');
		} else if ($field.val().length !== 16 && $check.prop('checked')) {
			$checkbox.removeClass('checkbox--error');
			$fieldBox.addClass('field--error');
			$button.addClass('btn--red');
			$button.text('ВВЕДИТЕ НОМЕР ТЕЛЕФОНА');
			$field.attr('placeholder', 'Ошибка при вводе данных');
		} else if ($field.val().length === 16 && $check.prop('checked')) {
			$fieldBox.removeClass('field--error');
			$button.removeClass('btn--red');
			$button.addClass('btn--grey');
			$button.text('ОТПРАВЛЕНО');
			$button.prop('disabled', true);
			$checkbox.removeClass('checkbox--error');
			setTimeout(function() {
				$popup.removeClass('popup--active');
				$('body').css('overflow', '');
			}, 3000);
		}
	});
	$(document).mouseup(function(e) {
		const container = $('.popup--active .popup__inner');
		if (!container.is(e.target) && container.has(e.target).length === 0) {
			$('.popup').removeClass('popup--active');
			$('body').css('overflow', '');
		}
	});
	$('.load-more').click(function(e) {
		e.preventDefault();
		const $button = $(e.currentTarget);
		const $buttonText = $button.find('.load-more__text');
		const $spinner = $button.find('.spinner');
		$buttonText.css('display', 'none');
		$button.addClass('btn--ocher');
		$spinner.addClass('spinner--active');
		setTimeout(function() {
			$buttonText.css('display', 'inline-block');
			$button.removeClass('btn--ocher');
			$spinner.removeClass('spinner--active');
		}, 1000);
	});
	$('body')
		.on('click', '.mobile-dropdown', (e) => {
			e.preventDefault();
			const $button = $(e.currentTarget);
			const $dropdown = $button.parents('.h-slider').find('.d-tabs-nav');
			$button.addClass('mobile-dropdown--open');
			$dropdown.addClass('d-tabs-nav--active');
		})
		.on('click', '.mobile-dropdown--open', (e) => {
			e.preventDefault();
			const $button = $(e.currentTarget);
			const $dropdown = $button.parents('.h-slider').find('.d-tabs-nav');
			$button.removeClass('mobile-dropdown--open');
			$dropdown.removeClass('d-tabs-nav--active');
		})
		.on('click', '.flats-tabs__link', (e) => {
			e.preventDefault();
			const $button = $(e.currentTarget);
			const $type = $button.parents('.flats-tabs__item').find('.flats-tabs__type');
			const $area = $button.parents('.flats-tabs__item').find('.flats-tabs__area');
			const $typeD = $button.parents('.h-slider').find('.mobile-dropdown__type');
			const $areaD = $button.parents('.h-slider').find('.mobile-dropdown__area');
			const $dropdown = $button.parents('.h-slider').find('.d-tabs-nav');
			$dropdown.removeClass('d-tabs-nav--active');
			$typeD.text($type.text());
			$areaD.text($area.text());
		})
		.on('click', '.docs-tabs__link', (e) => {
			e.preventDefault();
			const $button = $(e.currentTarget);
			const $typeD = $button.parents('.h-slider').find('.mobile-dropdown__text');
			const $dropdown = $button.parents('.h-slider').find('.d-tabs-nav');
			$dropdown.removeClass('d-tabs-nav--active');
			$typeD.text($button.text());
		});
	$('.news-slider__item').click(function() {
		$popupNews.addClass('news-popup--active');
		$('body').css('overflow', 'hidden');
	});
	$('.news-popup__close').click(function() {
		$popupNews.removeClass('news-popup--active');
		$('body').css('overflow', '');
	});

	if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
		$('.video').attr('controls', true);
		$('.step-thumbs').addClass('step-thumbs--mobile');
		$('body')
			.on('click', '.video__back-btn', (e) => {
				e.preventDefault();
				const $button = $(e.currentTarget);
				const $videoBack = $button.parents('.video').find('.video__back');
				const $movieBox = $button.parents('.video').find('.video__inner');
				const $closeBtn = $button.parents('.video').find('.video__close-btn');
				const $videoBox = $button.parents('.video').find('.video__movie');
				const $video = $videoBox.get(0);
				$video.controls = true;
				$videoBack.css('display', 'none');
				$closeBtn.css('display', 'none');
				$movieBox.addClass('video__inner--active');
				$video.play();
			})
			.on('click', '.video__btn', (e) => {
				e.preventDefault();
				const $button = $(e.currentTarget);
				const $videoBox = $button.parents('.video').find('.video__movie');
				const $video = $videoBox.get(0);
				if ($video.paused) {
					$video.play();
					$button.text('ПАУЗА');
					$button.css('opacity', '0');
				} else {
					$video.pause();
					$button.text('СМОТРЕТЬ');
					$button.css('opacity', '1');
				}
			});
	} else {
		$('body')
			.on('click', '.video__back-btn', (e) => {
				e.preventDefault();
				const $button = $(e.currentTarget);
				const $movieBox = $button.parents('.video').find('.video__inner');
				const $movie = $movieBox.get(0);
				const $videoBox = $button.parents('.video').find('.video__movie');
				const $video = $videoBox.get(0);
				$movieBox.addClass('video__inner--active');
				$video.play();
				if ($movie.requestFullscreen) {
					$movie.requestFullscreen();
				} else if ($movie.mozRequestFullScreen) {
					$movie.mozRequestFullScreen();
				} else if ($movie.webkitRequestFullscreen) {
					$movie.webkitRequestFullscreen();
				} else if ($movie.msRequestFullscreen) {
					$movie.msRequestFullscreen();
				}
			})
			.on('click', '.video__btn', (e) => {
				e.preventDefault();
				const $button = $(e.currentTarget);
				const $videoBox = $button.parents('.video').find('.video__movie');
				const $video = $videoBox.get(0);
				if ($video.paused) {
					$video.play();
					$button.text('ПАУЗА');
					$button.css('opacity', '');
				} else {
					$video.pause();
					$button.text('СМОТРЕТЬ');
					$button.css('opacity', '1');
				}
			})
			.on('click', '.video__close-btn', (e) => {
				e.preventDefault();
				const $button = $(e.currentTarget);
				const $movie = $button.parents('.video').find('.video__inner');
				const $movieBtn = $button.parents('.video').find('.video__btn');
				if (document.exitFullscreen) {
					document.exitFullscreen();
				} else if (document.webkitExitFullscreen) {
					document.webkitExitFullscreen();
				} else if (document.mozCancelFullScreen) {
					document.mozCancelFullScreen();
				} else if (document.msExitFullscreen) {
					document.msExitFullscreen();
				}
				$movie.removeClass('video__inner--active');
				$movieBtn.removeClass('video__btn--pause');
				$movieBtn.text('СМОТРЕТЬ');
			});
		document.addEventListener('fullscreenchange', exitHandler);
		document.addEventListener('webkitfullscreenchange', exitHandler);
		document.addEventListener('mozfullscreenchange', exitHandler);
		document.addEventListener('MSFullscreenChange', exitHandler);

		function exitHandler() {
			if (
				!document.fullscreenElement &&
				!document.webkitIsFullScreen &&
				!document.mozFullScreen &&
				!document.msFullscreenElement
			) {
				$('.video__inner').removeClass('video__inner--active');
				$('.video__btn').removeClass('video__btn--pause');
				$('.video__btn').text('СМОТРЕТЬ');
			}
		}
	}

	// IE11 detection
	if (/MSIE/.test(window.navigator.userAgent) || /Trident/.test(window.navigator.userAgent)) {
		$('body').addClass('msie');
	}

	// Polyfills
	objectFitImages();

	// https://www.thecssninja.com/javascript/pointer-events-60fps
	let peTimer;
	$(window).on('scroll', () => {
		if (peTimer) {
			clearTimeout(peTimer);
			peTimer = undefined;
		}
		const $body = $('body');
		if (!$body.hasClass('disable-hover')) {
			$body.addClass('disable-hover');
		}
		peTimer = setTimeout(() => {
			$body.removeClass('disable-hover');
		}, 100);
	});

	// Маска для номеров телефонов
	$('input[type="tel"]').each((i, el) => {
		new IMask(el, {
			mask: i18n.masks.tel,
		});
	});
});

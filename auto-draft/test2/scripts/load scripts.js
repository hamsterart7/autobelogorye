const scripts = [
	'//cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js',
	'//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.3/ScrollMagic.js',
	'//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/debug.addIndicators.min.js',
	'//cdnjs.cloudflare.com/ajax/libs/ScrollMagic/2.0.7/plugins/animation.gsap.js',
];

function loadScripts(urls, final_callback, index = 0) {
	if (typeof urls[index + 1] === 'undefined') {
		loadScript(urls[index], final_callback);
	} else {
		loadScript(urls[index], function() {
			loadScripts(urls, final_callback, index + 1);
		});
	}
}

function loadScript(url, callback) {
	const script = document.createElement('script');
	script.type = 'text/javascript';
	if (script.readyState) {
		script.onreadystatechange = function() {
			if (script.readyState === 'loaded' || script.readyState === 'complete') {
				script.onreadystatechange = null;
				callback();
			}
		};
	} else {
		script.onload = function() {
			callback();
		};
	}
	script.src = url;
	document.getElementsByTagName('head')[0].appendChild(script);
}

const gallerySlider = function() {};

$(document).ready(function() {
	loadScripts(scripts, gallerySlider);
});

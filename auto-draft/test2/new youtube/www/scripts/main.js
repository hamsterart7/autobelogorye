/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./node_modules/formdata-polyfill/formdata.min.js":
/*!********************************************************!*\
  !*** ./node_modules/formdata-polyfill/formdata.min.js ***!
  \********************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

;

(function () {
  var k;

  function l(a) {
    var b = 0;
    return function () {
      return b < a.length ? {
        done: !1,
        value: a[b++]
      } : {
        done: !0
      };
    };
  }

  var m = "function" == typeof Object.defineProperties ? Object.defineProperty : function (a, b, d) {
    a != Array.prototype && a != Object.prototype && (a[b] = d.value);
  },
      n = "undefined" != typeof window && window === this ? this : "undefined" != typeof global && null != global ? global : this;

  function p() {
    p = function p() {};

    n.Symbol || (n.Symbol = r);
  }

  var r = function () {
    var a = 0;
    return function (b) {
      return "jscomp_symbol_" + (b || "") + a++;
    };
  }();

  function u() {
    p();
    var a = n.Symbol.iterator;
    a || (a = n.Symbol.iterator = n.Symbol("iterator"));
    "function" != typeof Array.prototype[a] && m(Array.prototype, a, {
      configurable: !0,
      writable: !0,
      value: function value() {
        return v(l(this));
      }
    });

    u = function u() {};
  }

  function v(a) {
    u();
    a = {
      next: a
    };

    a[n.Symbol.iterator] = function () {
      return this;
    };

    return a;
  }

  function x(a) {
    var b = "undefined" != typeof Symbol && Symbol.iterator && a[Symbol.iterator];
    return b ? b.call(a) : {
      next: l(a)
    };
  }

  var y;
  if ("function" == typeof Object.setPrototypeOf) y = Object.setPrototypeOf;else {
    var z;

    a: {
      var A = {
        o: !0
      },
          B = {};

      try {
        B.__proto__ = A;
        z = B.o;
        break a;
      } catch (a) {}

      z = !1;
    }

    y = z ? function (a, b) {
      a.__proto__ = b;
      if (a.__proto__ !== b) throw new TypeError(a + " is not extensible");
      return a;
    } : null;
  }
  var C = y;

  function D() {
    this.g = !1;
    this.c = null;
    this.m = void 0;
    this.b = 1;
    this.l = this.s = 0;
    this.f = null;
  }

  function E(a) {
    if (a.g) throw new TypeError("Generator is already running");
    a.g = !0;
  }

  D.prototype.h = function (a) {
    this.m = a;
  };

  D.prototype.i = function (a) {
    this.f = {
      u: a,
      v: !0
    };
    this.b = this.s || this.l;
  };

  D.prototype["return"] = function (a) {
    this.f = {
      "return": a
    };
    this.b = this.l;
  };

  function F(a, b, d) {
    a.b = d;
    return {
      value: b
    };
  }

  function G(a) {
    this.w = a;
    this.j = [];

    for (var b in a) {
      this.j.push(b);
    }

    this.j.reverse();
  }

  function H(a) {
    this.a = new D();
    this.A = a;
  }

  H.prototype.h = function (a) {
    E(this.a);
    if (this.a.c) return I(this, this.a.c.next, a, this.a.h);
    this.a.h(a);
    return J(this);
  };

  function K(a, b) {
    E(a.a);
    var d = a.a.c;
    if (d) return I(a, "return" in d ? d["return"] : function (a) {
      return {
        value: a,
        done: !0
      };
    }, b, a.a["return"]);
    a.a["return"](b);
    return J(a);
  }

  H.prototype.i = function (a) {
    E(this.a);
    if (this.a.c) return I(this, this.a.c["throw"], a, this.a.h);
    this.a.i(a);
    return J(this);
  };

  function I(a, b, d, c) {
    try {
      var e = b.call(a.a.c, d);
      if (!(e instanceof Object)) throw new TypeError("Iterator result " + e + " is not an object");
      if (!e.done) return a.a.g = !1, e;
      var f = e.value;
    } catch (g) {
      return a.a.c = null, a.a.i(g), J(a);
    }

    a.a.c = null;
    c.call(a.a, f);
    return J(a);
  }

  function J(a) {
    for (; a.a.b;) {
      try {
        var b = a.A(a.a);
        if (b) return a.a.g = !1, {
          value: b.value,
          done: !1
        };
      } catch (d) {
        a.a.m = void 0, a.a.i(d);
      }
    }

    a.a.g = !1;

    if (a.a.f) {
      b = a.a.f;
      a.a.f = null;
      if (b.v) throw b.u;
      return {
        value: b["return"],
        done: !0
      };
    }

    return {
      value: void 0,
      done: !0
    };
  }

  function L(a) {
    this.next = function (b) {
      return a.h(b);
    };

    this["throw"] = function (b) {
      return a.i(b);
    };

    this["return"] = function (b) {
      return K(a, b);
    };

    u();

    this[Symbol.iterator] = function () {
      return this;
    };
  }

  function M(a, b) {
    var d = new L(new H(b));
    C && C(d, a.prototype);
    return d;
  }

  if ("undefined" === typeof FormData || !FormData.prototype.keys) {
    var N = function N(a, b) {
      for (var d = 0; d < a.length; d++) {
        b(a[d]);
      }
    },
        O = function O(a) {
      "string" === typeof a && (a = a.replace(/\r\n/g, "\n").replace(/\n/g, "\r\n"));
      return a;
    },
        P = function P(a, b, d) {
      if (2 > arguments.length) throw new TypeError("2 arguments required, but only " + arguments.length + " present.");
      return b instanceof Blob ? [a + "", b, void 0 !== d ? d + "" : "string" === typeof b.name ? b.name : "blob"] : [a + "", b + ""];
    },
        Q = function Q(a) {
      if (!arguments.length) throw new TypeError("1 argument required, but only 0 present.");
      return [a + ""];
    },
        R = function R(a) {
      var b = x(a);
      a = b.next().value;
      b = b.next().value;
      a instanceof Blob && (a = new File([a], b, {
        type: a.type,
        lastModified: a.lastModified
      }));
      return a;
    },
        S = "object" === (typeof window === "undefined" ? "undefined" : _typeof(window)) ? window : "object" === (typeof self === "undefined" ? "undefined" : _typeof(self)) ? self : this,
        T = S.FormData,
        U = S.XMLHttpRequest && S.XMLHttpRequest.prototype.send,
        V = S.Request && S.fetch;

    p();

    var W = S.Symbol && Symbol.toStringTag,
        X = new WeakMap(),
        Y = Array.from || function (a) {
      return [].slice.call(a);
    };

    W && (Blob.prototype[W] || (Blob.prototype[W] = "Blob"), "File" in S && !File.prototype[W] && (File.prototype[W] = "File"));

    try {
      new File([], "");
    } catch (a) {
      S.File = function (b, d, c) {
        b = new Blob(b, c);
        c = c && void 0 !== c.lastModified ? new Date(c.lastModified) : new Date();
        Object.defineProperties(b, {
          name: {
            value: d
          },
          lastModifiedDate: {
            value: c
          },
          lastModified: {
            value: +c
          },
          toString: {
            value: function value() {
              return "[object File]";
            }
          }
        });
        W && Object.defineProperty(b, W, {
          value: "File"
        });
        return b;
      };
    }

    p();
    u();

    var Z = function Z(a) {
      X.set(this, Object.create(null));
      if (!a) return this;
      var b = this;
      N(a.elements, function (a) {
        if (a.name && !a.disabled && "submit" !== a.type && "button" !== a.type) if ("file" === a.type) N(a.files || [], function (c) {
          b.append(a.name, c);
        });else if ("select-multiple" === a.type || "select-one" === a.type) N(a.options, function (c) {
          !c.disabled && c.selected && b.append(a.name, c.value);
        });else if ("checkbox" === a.type || "radio" === a.type) a.checked && b.append(a.name, a.value);else {
          var c = "textarea" === a.type ? O(a.value) : a.value;
          b.append(a.name, c);
        }
      });
    };

    k = Z.prototype;

    k.append = function (a, b, d) {
      var c = X.get(this);
      c[a] || (c[a] = []);
      c[a].push([b, d]);
    };

    k["delete"] = function (a) {
      delete X.get(this)[a];
    };

    k.entries = function b() {
      var d = this,
          c,
          e,
          f,
          g,
          h,
          q;
      return M(b, function (b) {
        switch (b.b) {
          case 1:
            c = X.get(d), f = new G(c);

          case 2:
            var t;

            a: {
              for (t = f; 0 < t.j.length;) {
                var w = t.j.pop();

                if (w in t.w) {
                  t = w;
                  break a;
                }
              }

              t = null;
            }

            if (null == (e = t)) {
              b.b = 0;
              break;
            }

            g = x(c[e]);
            h = g.next();

          case 5:
            if (h.done) {
              b.b = 2;
              break;
            }

            q = h.value;
            return F(b, [e, R(q)], 6);

          case 6:
            h = g.next(), b.b = 5;
        }
      });
    };

    k.forEach = function (b, d) {
      for (var c = x(this), e = c.next(); !e.done; e = c.next()) {
        var f = x(e.value);
        e = f.next().value;
        f = f.next().value;
        b.call(d, f, e, this);
      }
    };

    k.get = function (b) {
      var d = X.get(this);
      return d[b] ? R(d[b][0]) : null;
    };

    k.getAll = function (b) {
      return (X.get(this)[b] || []).map(R);
    };

    k.has = function (b) {
      return b in X.get(this);
    };

    k.keys = function d() {
      var c = this,
          e,
          f,
          g,
          h,
          q;
      return M(d, function (d) {
        1 == d.b && (e = x(c), f = e.next());

        if (3 != d.b) {
          if (f.done) {
            d.b = 0;
            return;
          }

          g = f.value;
          h = x(g);
          q = h.next().value;
          return F(d, q, 3);
        }

        f = e.next();
        d.b = 2;
      });
    };

    k.set = function (d, c, e) {
      X.get(this)[d] = [[c, e]];
    };

    k.values = function c() {
      var e = this,
          f,
          g,
          h,
          q,
          w;
      return M(c, function (c) {
        1 == c.b && (f = x(e), g = f.next());

        if (3 != c.b) {
          if (g.done) {
            c.b = 0;
            return;
          }

          h = g.value;
          q = x(h);
          q.next();
          w = q.next().value;
          return F(c, w, 3);
        }

        g = f.next();
        c.b = 2;
      });
    };

    Z.prototype._asNative = function () {
      for (var c = new T(), e = x(this), f = e.next(); !f.done; f = e.next()) {
        var g = x(f.value);
        f = g.next().value;
        g = g.next().value;
        c.append(f, g);
      }

      return c;
    };

    Z.prototype._blob = function () {
      for (var c = "----formdata-polyfill-" + Math.random(), e = [], f = x(this), g = f.next(); !g.done; g = f.next()) {
        var h = x(g.value);
        g = h.next().value;
        h = h.next().value;
        e.push("--" + c + "\r\n");
        h instanceof Blob ? e.push('Content-Disposition: form-data; name="' + g + '"; filename="' + h.name + '"\r\n', "Content-Type: " + (h.type || "application/octet-stream") + "\r\n\r\n", h, "\r\n") : e.push('Content-Disposition: form-data; name="' + g + '"\r\n\r\n' + h + "\r\n");
      }

      e.push("--" + c + "--");
      return new Blob(e, {
        type: "multipart/form-data; boundary=" + c
      });
    };

    Z.prototype[Symbol.iterator] = function () {
      return this.entries();
    };

    Z.prototype.toString = function () {
      return "[object FormData]";
    };

    W && (Z.prototype[W] = "FormData");
    [["append", P], ["delete", Q], ["get", Q], ["getAll", Q], ["has", Q], ["set", P]].forEach(function (c) {
      var e = Z.prototype[c[0]];

      Z.prototype[c[0]] = function () {
        return e.apply(this, c[1].apply(this, Y(arguments)));
      };
    });
    U && (XMLHttpRequest.prototype.send = function (c) {
      c instanceof Z ? (c = c._blob(), this.setRequestHeader("Content-Type", c.type), U.call(this, c)) : U.call(this, c);
    });

    if (V) {
      var aa = S.fetch;

      S.fetch = function (c, e) {
        e && e.body && e.body instanceof Z && (e.body = e.body._blob());
        return aa(c, e);
      };
    }

    S.FormData = Z;
  }

  ;
})();
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./node_modules/object-fit-images/dist/ofi.common-js.js":
/*!**************************************************************!*\
  !*** ./node_modules/object-fit-images/dist/ofi.common-js.js ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/*! npm.im/object-fit-images 3.2.4 */


var OFI = 'bfred-it:object-fit-images';
var propRegex = /(object-fit|object-position)\s*:\s*([-.\w\s%]+)/g;
var testImg = typeof Image === 'undefined' ? {
  style: {
    'object-position': 1
  }
} : new Image();
var supportsObjectFit = 'object-fit' in testImg.style;
var supportsObjectPosition = 'object-position' in testImg.style;
var supportsOFI = 'background-size' in testImg.style;
var supportsCurrentSrc = typeof testImg.currentSrc === 'string';
var nativeGetAttribute = testImg.getAttribute;
var nativeSetAttribute = testImg.setAttribute;
var autoModeEnabled = false;

function createPlaceholder(w, h) {
  return "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='" + w + "' height='" + h + "'%3E%3C/svg%3E";
}

function polyfillCurrentSrc(el) {
  if (el.srcset && !supportsCurrentSrc && window.picturefill) {
    var pf = window.picturefill._; // parse srcset with picturefill where currentSrc isn't available

    if (!el[pf.ns] || !el[pf.ns].evaled) {
      // force synchronous srcset parsing
      pf.fillImg(el, {
        reselect: true
      });
    }

    if (!el[pf.ns].curSrc) {
      // force picturefill to parse srcset
      el[pf.ns].supported = false;
      pf.fillImg(el, {
        reselect: true
      });
    } // retrieve parsed currentSrc, if any


    el.currentSrc = el[pf.ns].curSrc || el.src;
  }
}

function getStyle(el) {
  var style = getComputedStyle(el).fontFamily;
  var parsed;
  var props = {};

  while ((parsed = propRegex.exec(style)) !== null) {
    props[parsed[1]] = parsed[2];
  }

  return props;
}

function setPlaceholder(img, width, height) {
  // Default: fill width, no height
  var placeholder = createPlaceholder(width || 1, height || 0); // Only set placeholder if it's different

  if (nativeGetAttribute.call(img, 'src') !== placeholder) {
    nativeSetAttribute.call(img, 'src', placeholder);
  }
}

function onImageReady(img, callback) {
  // naturalWidth is only available when the image headers are loaded,
  // this loop will poll it every 100ms.
  if (img.naturalWidth) {
    callback(img);
  } else {
    setTimeout(onImageReady, 100, img, callback);
  }
}

function fixOne(el) {
  var style = getStyle(el);
  var ofi = el[OFI];
  style['object-fit'] = style['object-fit'] || 'fill'; // default value
  // Avoid running where unnecessary, unless OFI had already done its deed

  if (!ofi.img) {
    // fill is the default behavior so no action is necessary
    if (style['object-fit'] === 'fill') {
      return;
    } // Where object-fit is supported and object-position isn't (Safari < 10)


    if (!ofi.skipTest && // unless user wants to apply regardless of browser support
    supportsObjectFit && // if browser already supports object-fit
    !style['object-position'] // unless object-position is used
    ) {
        return;
      }
  } // keep a clone in memory while resetting the original to a blank


  if (!ofi.img) {
    ofi.img = new Image(el.width, el.height);
    ofi.img.srcset = nativeGetAttribute.call(el, "data-ofi-srcset") || el.srcset;
    ofi.img.src = nativeGetAttribute.call(el, "data-ofi-src") || el.src; // preserve for any future cloneNode calls
    // https://github.com/bfred-it/object-fit-images/issues/53

    nativeSetAttribute.call(el, "data-ofi-src", el.src);

    if (el.srcset) {
      nativeSetAttribute.call(el, "data-ofi-srcset", el.srcset);
    }

    setPlaceholder(el, el.naturalWidth || el.width, el.naturalHeight || el.height); // remove srcset because it overrides src

    if (el.srcset) {
      el.srcset = '';
    }

    try {
      keepSrcUsable(el);
    } catch (err) {
      if (window.console) {
        console.warn('https://bit.ly/ofi-old-browser');
      }
    }
  }

  polyfillCurrentSrc(ofi.img);
  el.style.backgroundImage = "url(\"" + (ofi.img.currentSrc || ofi.img.src).replace(/"/g, '\\"') + "\")";
  el.style.backgroundPosition = style['object-position'] || 'center';
  el.style.backgroundRepeat = 'no-repeat';
  el.style.backgroundOrigin = 'content-box';

  if (/scale-down/.test(style['object-fit'])) {
    onImageReady(ofi.img, function () {
      if (ofi.img.naturalWidth > el.width || ofi.img.naturalHeight > el.height) {
        el.style.backgroundSize = 'contain';
      } else {
        el.style.backgroundSize = 'auto';
      }
    });
  } else {
    el.style.backgroundSize = style['object-fit'].replace('none', 'auto').replace('fill', '100% 100%');
  }

  onImageReady(ofi.img, function (img) {
    setPlaceholder(el, img.naturalWidth, img.naturalHeight);
  });
}

function keepSrcUsable(el) {
  var descriptors = {
    get: function get(prop) {
      return el[OFI].img[prop ? prop : 'src'];
    },
    set: function set(value, prop) {
      el[OFI].img[prop ? prop : 'src'] = value;
      nativeSetAttribute.call(el, "data-ofi-" + prop, value); // preserve for any future cloneNode

      fixOne(el);
      return value;
    }
  };
  Object.defineProperty(el, 'src', descriptors);
  Object.defineProperty(el, 'currentSrc', {
    get: function get() {
      return descriptors.get('currentSrc');
    }
  });
  Object.defineProperty(el, 'srcset', {
    get: function get() {
      return descriptors.get('srcset');
    },
    set: function set(ss) {
      return descriptors.set(ss, 'srcset');
    }
  });
}

function hijackAttributes() {
  function getOfiImageMaybe(el, name) {
    return el[OFI] && el[OFI].img && (name === 'src' || name === 'srcset') ? el[OFI].img : el;
  }

  if (!supportsObjectPosition) {
    HTMLImageElement.prototype.getAttribute = function (name) {
      return nativeGetAttribute.call(getOfiImageMaybe(this, name), name);
    };

    HTMLImageElement.prototype.setAttribute = function (name, value) {
      return nativeSetAttribute.call(getOfiImageMaybe(this, name), name, String(value));
    };
  }
}

function fix(imgs, opts) {
  var startAutoMode = !autoModeEnabled && !imgs;
  opts = opts || {};
  imgs = imgs || 'img';

  if (supportsObjectPosition && !opts.skipTest || !supportsOFI) {
    return false;
  } // use imgs as a selector or just select all images


  if (imgs === 'img') {
    imgs = document.getElementsByTagName('img');
  } else if (typeof imgs === 'string') {
    imgs = document.querySelectorAll(imgs);
  } else if (!('length' in imgs)) {
    imgs = [imgs];
  } // apply fix to all


  for (var i = 0; i < imgs.length; i++) {
    imgs[i][OFI] = imgs[i][OFI] || {
      skipTest: opts.skipTest
    };
    fixOne(imgs[i]);
  }

  if (startAutoMode) {
    document.body.addEventListener('load', function (e) {
      if (e.target.tagName === 'IMG') {
        fix(e.target, {
          skipTest: opts.skipTest
        });
      }
    }, true);
    autoModeEnabled = true;
    imgs = 'img'; // reset to a generic selector for watchMQ
  } // if requested, watch media queries for object-fit change


  if (opts.watchMQ) {
    window.addEventListener('resize', fix.bind(null, imgs, {
      skipTest: opts.skipTest
    }));
  }
}

fix.supportsObjectFit = supportsObjectFit;
fix.supportsObjectPosition = supportsObjectPosition;
hijackAttributes();
module.exports = fix;

/***/ }),

/***/ "./node_modules/svgxuse/svgxuse.js":
/*!*****************************************!*\
  !*** ./node_modules/svgxuse/svgxuse.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/*!
 * @copyright Copyright (c) 2017 IcoMoon.io
 * @license   Licensed under MIT license
 *            See https://github.com/Keyamoon/svgxuse
 * @version   1.2.6
 */

/*jslint browser: true */

/*global XDomainRequest, MutationObserver, window */
(function () {
  "use strict";

  if (typeof window !== "undefined" && window.addEventListener) {
    var cache = Object.create(null); // holds xhr objects to prevent multiple requests

    var checkUseElems;
    var tid; // timeout id

    var debouncedCheck = function debouncedCheck() {
      clearTimeout(tid);
      tid = setTimeout(checkUseElems, 100);
    };

    var unobserveChanges = function unobserveChanges() {
      return;
    };

    var observeChanges = function observeChanges() {
      var observer;
      window.addEventListener("resize", debouncedCheck, false);
      window.addEventListener("orientationchange", debouncedCheck, false);

      if (window.MutationObserver) {
        observer = new MutationObserver(debouncedCheck);
        observer.observe(document.documentElement, {
          childList: true,
          subtree: true,
          attributes: true
        });

        unobserveChanges = function unobserveChanges() {
          try {
            observer.disconnect();
            window.removeEventListener("resize", debouncedCheck, false);
            window.removeEventListener("orientationchange", debouncedCheck, false);
          } catch (ignore) {}
        };
      } else {
        document.documentElement.addEventListener("DOMSubtreeModified", debouncedCheck, false);

        unobserveChanges = function unobserveChanges() {
          document.documentElement.removeEventListener("DOMSubtreeModified", debouncedCheck, false);
          window.removeEventListener("resize", debouncedCheck, false);
          window.removeEventListener("orientationchange", debouncedCheck, false);
        };
      }
    };

    var createRequest = function createRequest(url) {
      // In IE 9, cross origin requests can only be sent using XDomainRequest.
      // XDomainRequest would fail if CORS headers are not set.
      // Therefore, XDomainRequest should only be used with cross origin requests.
      function getOrigin(loc) {
        var a;

        if (loc.protocol !== undefined) {
          a = loc;
        } else {
          a = document.createElement("a");
          a.href = loc;
        }

        return a.protocol.replace(/:/g, "") + a.host;
      }

      var Request;
      var origin;
      var origin2;

      if (window.XMLHttpRequest) {
        Request = new XMLHttpRequest();
        origin = getOrigin(location);
        origin2 = getOrigin(url);

        if (Request.withCredentials === undefined && origin2 !== "" && origin2 !== origin) {
          Request = XDomainRequest || undefined;
        } else {
          Request = XMLHttpRequest;
        }
      }

      return Request;
    };

    var xlinkNS = "http://www.w3.org/1999/xlink";

    checkUseElems = function checkUseElems() {
      var base;
      var bcr;
      var fallback = ""; // optional fallback URL in case no base path to SVG file was given and no symbol definition was found.

      var hash;
      var href;
      var i;
      var inProgressCount = 0;
      var isHidden;
      var Request;
      var url;
      var uses;
      var xhr;

      function observeIfDone() {
        // If done with making changes, start watching for chagnes in DOM again
        inProgressCount -= 1;

        if (inProgressCount === 0) {
          // if all xhrs were resolved
          unobserveChanges(); // make sure to remove old handlers

          observeChanges(); // watch for changes to DOM
        }
      }

      function attrUpdateFunc(spec) {
        return function () {
          if (cache[spec.base] !== true) {
            spec.useEl.setAttributeNS(xlinkNS, "xlink:href", "#" + spec.hash);

            if (spec.useEl.hasAttribute("href")) {
              spec.useEl.setAttribute("href", "#" + spec.hash);
            }
          }
        };
      }

      function onloadFunc(xhr) {
        return function () {
          var body = document.body;
          var x = document.createElement("x");
          var svg;
          xhr.onload = null;
          x.innerHTML = xhr.responseText;
          svg = x.getElementsByTagName("svg")[0];

          if (svg) {
            svg.setAttribute("aria-hidden", "true");
            svg.style.position = "absolute";
            svg.style.width = 0;
            svg.style.height = 0;
            svg.style.overflow = "hidden";
            body.insertBefore(svg, body.firstChild);
          }

          observeIfDone();
        };
      }

      function onErrorTimeout(xhr) {
        return function () {
          xhr.onerror = null;
          xhr.ontimeout = null;
          observeIfDone();
        };
      }

      unobserveChanges(); // stop watching for changes to DOM
      // find all use elements

      uses = document.getElementsByTagName("use");

      for (i = 0; i < uses.length; i += 1) {
        try {
          bcr = uses[i].getBoundingClientRect();
        } catch (ignore) {
          // failed to get bounding rectangle of the use element
          bcr = false;
        }

        href = uses[i].getAttribute("href") || uses[i].getAttributeNS(xlinkNS, "href") || uses[i].getAttribute("xlink:href");

        if (href && href.split) {
          url = href.split("#");
        } else {
          url = ["", ""];
        }

        base = url[0];
        hash = url[1];
        isHidden = bcr && bcr.left === 0 && bcr.right === 0 && bcr.top === 0 && bcr.bottom === 0;

        if (bcr && bcr.width === 0 && bcr.height === 0 && !isHidden) {
          // the use element is empty
          // if there is a reference to an external SVG, try to fetch it
          // use the optional fallback URL if there is no reference to an external SVG
          if (fallback && !base.length && hash && !document.getElementById(hash)) {
            base = fallback;
          }

          if (uses[i].hasAttribute("href")) {
            uses[i].setAttributeNS(xlinkNS, "xlink:href", href);
          }

          if (base.length) {
            // schedule updating xlink:href
            xhr = cache[base];

            if (xhr !== true) {
              // true signifies that prepending the SVG was not required
              setTimeout(attrUpdateFunc({
                useEl: uses[i],
                base: base,
                hash: hash
              }), 0);
            }

            if (xhr === undefined) {
              Request = createRequest(base);

              if (Request !== undefined) {
                xhr = new Request();
                cache[base] = xhr;
                xhr.onload = onloadFunc(xhr);
                xhr.onerror = onErrorTimeout(xhr);
                xhr.ontimeout = onErrorTimeout(xhr);
                xhr.open("GET", base);
                xhr.send();
                inProgressCount += 1;
              }
            }
          }
        } else {
          if (!isHidden) {
            if (cache[base] === undefined) {
              // remember this URL if the use element was not empty and no request was sent
              cache[base] = true;
            } else if (cache[base].onload) {
              // if it turns out that prepending the SVG is not necessary,
              // abort the in-progress xhr.
              cache[base].abort();
              delete cache[base].onload;
              cache[base] = true;
            }
          } else if (base.length && cache[base]) {
            setTimeout(attrUpdateFunc({
              useEl: uses[i],
              base: base,
              hash: hash
            }), 0);
          }
        }
      }

      uses = "";
      inProgressCount += 1;
      observeIfDone();
    };

    var _winLoad;

    _winLoad = function winLoad() {
      window.removeEventListener("load", _winLoad, false); // to prevent memory leaks

      tid = setTimeout(checkUseElems, 0);
    };

    if (document.readyState !== "complete") {
      // The load event fires when all resources have finished loading, which allows detecting whether SVG use elements are empty.
      window.addEventListener("load", _winLoad, false);
    } else {
      // No need to add a listener if the document is already loaded, initialize immediately.
      _winLoad();
    }
  }
})();

/***/ }),

/***/ "./node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

var g; // This works in non-strict mode

g = function () {
  return this;
}();

try {
  // This works if eval is allowed (see CSP)
  g = g || new Function("return this")();
} catch (e) {
  // This works if the window reference is available
  if ((typeof window === "undefined" ? "undefined" : _typeof(window)) === "object") g = window;
} // g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}


module.exports = g;

/***/ }),

/***/ "./scripts/animate.js":
/*!****************************!*\
  !*** ./scripts/animate.js ***!
  \****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ({
  duration: {
    timeout: 2500,
    effect: 500,
    enter: 250,
    leave: 200,
    micro: 100
  },
  easing: {
    linear: 'cubicBezier(0, 0, 1, 1)',
    standard: 'cubicBezier(0.4, 0, 0.2, 1)',
    deceleration: 'cubicBezier(0, 0, 0.2, 1)',
    acceleration: 'cubicBezier(0.4, 0, 1, 1)'
  }
});

/***/ }),

/***/ "./scripts/anitabs.min.js":
/*!********************************!*\
  !*** ./scripts/anitabs.min.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports) {

!function (s) {
  var t = {
    drag: !0,
    dragSpeed: 500,
    animation: 'fade',
    animationSpeed: 500,
    autoHeight: !0,
    slideDirection: 'left'
  };

  s.fn.aniTabs = function (i) {
    var a = s.extend({}, t, i);
    return a.drag || s(this).closest('.js-tabs-nav').find('.js-tabs-drag').css('display', 'none'), s(this).click(function (t) {
      if (t.preventDefault(), s(this).closest('.js-tabs-item.active').length < 1) {
        'fade' == a.animation && s(this).closest('.js-tabs').find('.js-tabs-wrap:first .js-tabs-content').css({
          '-webkit-transition-duration': a.animationSpeed / 1e3 + 's',
          '-ms-transition-duration': a.animationSpeed / 1e3 + 's',
          'transition-duration': a.animationSpeed / 1e3 + 's'
        }), 'slide' == a.animation && s(this).closest('.js-tabs').find('.js-tabs-wrap:first .js-tabs-content').addClass('slide').css({
          '-webkit-animation-duration': a.animationSpeed / 1e3 + 's',
          '-ms-animation-duration': a.animationSpeed / 1e3 + 's',
          'animation-duration': a.animationSpeed / 1e3 + 's'
        }), a.autoHeight && s(this).closest('.js-tabs').find('.js-tabs-wrap:first').css({
          '-webkit-transition-duration': a.animationSpeed / 1e3 + 's',
          '-ms-transition-duration': a.animationSpeed / 1e3 + 's',
          'transition-duration': a.animationSpeed / 1e3 + 's'
        });

        var _i = s(this).attr('href');

        s(this).closest('.js-tabs-item').siblings('.js-tabs-item').removeClass('active'), s(this).closest('.js-tabs-item').addClass('active');
        var e = s(_i).siblings('.js-tabs-content.active').height();
        s(_i).closest('.js-tabs-wrap').css('height', e + 'px'), 'slide' == a.animation && ('left' == a.slideDirection && (s(this).closest('.js-tabs').find('.js-tabs-wrap:first .js-tabs-content').removeClass('slideOutLeft slideInRight moved'), s(_i).siblings('.js-tabs-content.active').addClass('slideOutLeft moved')), 'right' == a.slideDirection && (s(this).closest('.js-tabs').find('.js-tabs-wrap:first .js-tabs-content').removeClass('slideOutRight slideInLeft moved'), s(_i).siblings('.js-tabs-content.active').addClass('slideOutRight moved'))), s(_i).addClass('active').siblings('.js-tabs-content').removeClass('active');
        var n = s(_i).height();

        if (s(_i).closest('.js-tabs-wrap').css('height', n + 'px'), 'slide' == a.animation && ('left' == a.slideDirection && s(_i).addClass('slideInRight'), 'right' == a.slideDirection && s(_i).addClass('slideInLeft')), setTimeout(function () {
          s(_i).closest('.js-tabs-wrap').css('height', 'auto'), 'slide' == a.animation && s(_i).siblings('.js-tabs-content').removeClass('slideOutLeft slideOutRight moved');
        }, a.animationSpeed), a.drag) {
          var o = s(this).closest('.js-tabs-item').position().top;
          var d = s(this).closest('.js-tabs-item').position().left;
          var r = s(this).closest('.js-tabs-item').width();
          s(this).closest('.js-tabs-nav').find('.js-tabs-drag').css({
            width: r + 'px',
            top: o + 'px',
            left: d + 'px',
            '-webkit-transition-duration': a.dragSpeed / 1e3 + 's',
            '-ms-transition-duration': a.dragSpeed / 1e3 + 's',
            'transition-duration': a.dragSpeed / 1e3 + 's'
          });
        }
      }
    }), this;
  };

  var i = function i() {
    s('.js-tabs-drag').each(function () {
      var t = s(this).closest('.js-tabs-nav').find('.js-tabs-item.active').position().top;
      var i = s(this).closest('.js-tabs-nav').find('.js-tabs-item.active').position().left;
      var a = s(this).closest('.js-tabs-nav').find('.js-tabs-item.active').width();
      s(this).css({
        width: a + 'px',
        top: t + 'px',
        left: i + 'px'
      });
    }).addClass('active');
  };

  s(window).bind('load', i), s(window).bind('orientationchange', i), s(window).bind('resize', i);
}(jQuery);

/***/ }),

/***/ "./scripts/main.js":
/*!*************************!*\
  !*** ./scripts/main.js ***!
  \*************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var formdata_polyfill__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! formdata-polyfill */ "./node_modules/formdata-polyfill/formdata.min.js");
/* harmony import */ var formdata_polyfill__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(formdata_polyfill__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var svgxuse__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! svgxuse */ "./node_modules/svgxuse/svgxuse.js");
/* harmony import */ var svgxuse__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(svgxuse__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var object_fit_images__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! object-fit-images */ "./node_modules/object-fit-images/dist/ofi.common-js.js");
/* harmony import */ var object_fit_images__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(object_fit_images__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _anitabs_min__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./anitabs.min */ "./scripts/anitabs.min.js");
/* harmony import */ var _anitabs_min__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_anitabs_min__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _animate__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./animate */ "./scripts/animate.js");
/* global $, i18n */





var i18n = {
  masks: {
    tel: '+{7} 000 000-00-00'
  },
  units: {
    byte: 'Б',
    kilobyte: 'КБ',
    megabyte: 'МБ'
  },
  fileInput: ['файл', 'файла', 'файлов'],
  validate: {
    required: 'Обязательно для заполнения',
    minlength: 'Минимальное кол-во символов — {{min}}',
    minWithMask: 'Заполнено не до конца',
    email: 'Нужно ввести настоящую эл. почту',
    url: 'Нужно ввести настоящее имя домена',
    minDate: 'Минимальная дата — {{min}}',
    maxDate: 'Максимальная дата — {{max}}',
    minNumber: 'Минимум {{min}}',
    maxNumber: 'Максимум {{max}}',
    maxFile: 'Максимум {{max}}'
  }
};
var preloader = $('.preloader');
document.addEventListener('DOMContentLoaded', function () {
  var lazyVideos = [].slice.call(document.querySelectorAll('video.lazy'));

  if ('IntersectionObserver' in window) {
    var lazyVideoObserver = new IntersectionObserver(function (entries, observer) {
      entries.forEach(function (video) {
        if (video.isIntersecting) {
          for (var source in video.target.children) {
            var videoSource = video.target.children[source];

            if (typeof videoSource.tagName === 'string' && videoSource.tagName === 'SOURCE') {
              videoSource.src = videoSource.dataset.src;
            }
          }

          video.target.load();
          video.target.play();
          video.target.classList.remove('lazy');
          lazyVideoObserver.unobserve(video.target);
        }
      });
    });
    lazyVideos.forEach(function (lazyVideo) {
      lazyVideoObserver.observe(lazyVideo);
    });
  }

  var lazyImages = [].slice.call(document.querySelectorAll('img.lazy'));

  if ('IntersectionObserver' in window) {
    var lazyImageObserver = new IntersectionObserver(function (entries, observer) {
      entries.forEach(function (entry) {
        if (entry.isIntersecting) {
          var lazyImage = entry.target;
          lazyImage.src = lazyImage.dataset.src;
          lazyImage.srcset = lazyImage.dataset.srcset;
          lazyImage.classList.remove('lazy');
          lazyImageObserver.unobserve(lazyImage);
        }
      });
    });
    lazyImages.forEach(function (lazyImage) {
      lazyImageObserver.observe(lazyImage);
    });
  } else {// Possibly fall back to a more compatible method here
  }
});
var $window = $(window);
var section5Anim = {
  init: function init() {
    var $section = $('.advantages');
    var leftBlock = $('.advantages__info');

    function func() {
      var winH = $window.height();
      leftBlock.outerHeight(winH);
      var sectH = $section.outerHeight();
      var sectTop = $section.offset().top;
      var scrollT = $window.scrollTop();
      var endPos = sectTop + sectH;
      var $block1 = $('.advantages__item--first');
      var $block2 = $('.advantages__item--second');
      var progress = (scrollT + winH / 2 - sectTop) / sectH;

      if (scrollT >= sectTop && scrollT + winH < endPos) {
        leftBlock.addClass('fixed');
        $block1.stop(true, false);
        $block2.stop(true, false);

        if (progress > 0.25 && progress < 0.5) {
          $block2.hide();
          $block1.show().css('opacity', 1 - (progress - 0.25) / 0.25);
        } else if (progress > 0.5 && progress <= 0.75) {
          $block1.hide();
          $block2.show().css('opacity', (progress - 0.5) / 0.25);
        }
      } else if (scrollT >= sectTop && scrollT + winH >= endPos) {
        leftBlock.removeClass('fixed').addClass('bottom');
        $block1.hide();
        $block2.show().css('opacity', 1);
      } else {
        leftBlock.removeClass('fixed bottom');
        $block2.hide();
        $block1.show().css('opacity', 1);
      }
    }

    func();
    $window.resize(func);
    $window.scroll(func);
  }
};
$(document).ready(function () {
  preloader.addClass('hidden');
  var $toggle = $('.toggle__input');
  var $toggleLevel = $('.toggle-level');
  var $toggleFurniture = $('.toggle-furniture');
  var $btnFood = $('.btn-food');
  var $btnToy = $('.btn-toy');
  var $btnHeart = $('.btn-heart');
  var $btnBook = $('.btn-book');
  var bookMarkers = [];
  var foodMarkers = [];
  var toyMarkers = [];
  var heartMarkers = [];
  var $container = $('.content');
  var $youtubeVideos = $('.youtube-video');
  var btnIdx;
  var $medias = [];
  var controller = new ScrollMagic.Controller();

  if ($container.find('.youtube-video').length > 0) {
    $youtubeVideos.each(function (i, el) {
      var $youtubeVideo = $(el);
      var idx = i;
      $youtubeVideo.mediaelementplayer({
        pluginPath: 'https://cdnjs.com/libraries/mediaelement/',
        shimScriptAccess: 'always',
        enableAutosize: true,
        pauseOtherPlayers: true,
        success: function success(media, domElement, player) {
          $medias[idx] = media;
        }
      });
    });
    $('body').on('click', '.js-youtube-watch-btn', function (e) {
      e.preventDefault();
      var $button = $(e.currentTarget);
      var $holder = $button.parents('.video');
      btnIdx = $holder.attr('data-video-index');
      var $movieBox = $button.parents('.video').find('.video__inner');
      var $movie = $movieBox.get(0);
      $movieBox.addClass('video__inner--active');
      $medias[btnIdx].play();

      if ($movie.requestFullscreen) {
        $movie.requestFullscreen();
      } else if ($movie.mozRequestFullScreen) {
        $movie.mozRequestFullScreen();
      } else if ($movie.webkitRequestFullscreen) {
        $movie.webkitRequestFullscreen();
      } else if ($movie.msRequestFullscreen) {
        $movie.msRequestFullscreen();
      }
    }).on('click', '.js-youtube-pause-btn', function (e) {
      e.preventDefault();
      var $button = $(e.currentTarget);

      if ($medias[btnIdx].paused) {
        $medias[btnIdx].play();
        $button.text('ПАУЗА');
        $button.css('opacity', '');
      } else {
        $medias[btnIdx].pause();
        $button.text('СМОТРЕТЬ');
        $button.css('opacity', '1');
      }
    }).on('click', '.js-youtube-close-btn', function (e) {
      e.preventDefault();
      var $button = $(e.currentTarget);
      var $movie = $button.parents('.video').find('.video__inner');
      var $movieBtn = $button.parents('.video').find('.video__btn');
      $medias[btnIdx].pause();

      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      }

      $movie.removeClass('video__inner--active');
      $movieBtn.removeClass('video__btn--pause');
      $movieBtn.text('СМОТРЕТЬ');
    });
    document.addEventListener('fullscreenchange', exitHandler);
    document.addEventListener('webkitfullscreenchange', exitHandler);
    document.addEventListener('mozfullscreenchange', exitHandler);
    document.addEventListener('MSFullscreenChange', exitHandler);

    function exitHandler() {
      if (!document.fullscreenElement && !document.webkitIsFullScreen && !document.mozFullScreen && !document.msFullscreenElement) {
        $('.video__inner').removeClass('video__inner--active');
        $('.video__btn').removeClass('video__btn--pause');
        $('.video__btn').text('СМОТРЕТЬ');
      }
    }
  }

  if ($container.find('.advantages').length > 0) {
    section5Anim.init();
  }

  if ($container.find('.large-gallery-first').length > 0) {
    var wipeAnimation = new TimelineMax().fromTo('.large-gallery-first__item--two', 1, {
      x: '100%'
    }, {
      x: '0%',
      ease: Linear.easeNone
    }).fromTo('.large-gallery-first__item--three', 1, {
      x: '100%'
    }, {
      x: '0%',
      ease: Linear.easeNone
    });
    new ScrollMagic.Scene({
      triggerElement: '.large-gallery-first',
      triggerHook: 'onLeave',
      duration: '100%'
    }).setPin('.large-gallery-first').setTween(wipeAnimation).addTo(controller);
  }

  if ($container.find('.large-gallery-second').length > 0) {
    var wipeAnimationSecond = new TimelineMax().fromTo('.large-gallery-second__item--two', 1, {
      y: '100%'
    }, {
      y: '0%',
      ease: Linear.easeNone
    }).fromTo('.large-gallery-second__item--three', 1, {
      y: '100%'
    }, {
      y: '0%',
      ease: Linear.easeNone
    });
    new ScrollMagic.Scene({
      triggerElement: '.large-gallery-second',
      triggerHook: 'onLeave',
      duration: '100%'
    }).setPin('.large-gallery-second').setTween(wipeAnimationSecond).addTo(controller);
  }

  if ($container.find('.large-gallery-third').length > 0) {
    var wipeAnimationThird = new TimelineMax().fromTo('.large-gallery-third__item--two', 1, {
      x: '100%'
    }, {
      x: '0%',
      ease: Linear.easeNone
    }).fromTo('.large-gallery-third__item--three', 1, {
      x: '100%'
    }, {
      x: '0%',
      ease: Linear.easeNone
    });
    new ScrollMagic.Scene({
      triggerElement: '.large-gallery-third',
      triggerHook: 'onLeave',
      duration: '100%'
    }).setPin('.large-gallery-third').setTween(wipeAnimationThird).addTo(controller);
  }

  if ($container.find('.large-gallery-fourth').length > 0) {
    var wipeAnimationFourth = new TimelineMax().fromTo('.large-gallery-fourth__item--two', 1, {
      x: '100%'
    }, {
      x: '0%',
      ease: Linear.easeNone
    }).fromTo('.large-gallery-fourth__item--three', 1, {
      x: '100%'
    }, {
      x: '0%',
      ease: Linear.easeNone
    });
    new ScrollMagic.Scene({
      triggerElement: '.large-gallery-fourth',
      triggerHook: 'onLeave',
      duration: '100%'
    }).setPin('.large-gallery-fourth', {
      pushFollowers: true
    }).setTween(wipeAnimationFourth).addTo(controller);
  }

  if ($container.find('.large-gallery-fifth').length > 0) {
    var wipeAnimationFifth = new TimelineMax().fromTo('.large-gallery-fifth__item--two', 1, {
      x: '100%'
    }, {
      x: '0%',
      ease: Linear.easeNone
    }).fromTo('.large-gallery-fifth__item--three', 1, {
      x: '100%'
    }, {
      x: '0%',
      ease: Linear.easeNone
    });
    new ScrollMagic.Scene({
      triggerElement: '.large-gallery-fifth',
      triggerHook: 'onLeave',
      duration: '100%'
    }).setPin('.large-gallery-fifth').setTween(wipeAnimationFifth).addTo(controller);
  }

  var dateInputValue = $('.countdown-value').val();
  var countDownDate = new Date(dateInputValue).getTime();
  var x = setInterval(function () {
    var $minuteOld = $('#minute').text();
    var $hourOld = $('#hour').text();
    var $dayOld = $('#day').text();
    $('#minute').removeClass('changed');
    $('#hour').removeClass('changed');
    $('#day').removeClass('changed');
    var now = new Date().getTime();
    var distance = countDownDate - now;
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor(distance % (1000 * 60 * 60 * 24) / (1000 * 60 * 60));
    var minutes = Math.floor(distance % (1000 * 60 * 60) / (1000 * 60));

    if ($container.find('.countdown').length > 0) {
      document.getElementById('day').innerHTML = days;
      document.getElementById('hour').innerHTML = hours;
      document.getElementById('minute').innerHTML = minutes;

      if (distance < 0) {
        clearInterval(x);
        document.getElementById('day').innerHTML = '00';
        document.getElementById('hour').innerHTML = '00';
        document.getElementById('minute').innerHTML = '00';
      }
    }

    if ($minuteOld !== $('#minute').text()) {
      $('#minute').addClass('changed');
    }

    if ($hourOld !== $('#hour').text()) {
      $('#hour').addClass('changed');
    }

    if ($dayOld !== $('#day').text()) {
      $('#day').addClass('changed');
    }
  }, 1000);
  $('.js-tabs-link').aniTabs({
    animation: 'fade',
    autoHeight: true
  });

  var setLink = function setLink() {
    var $slide = $('.flats-content.active').find('.swiper-slide-active');
    var $flatInput = $slide.find('.flat-link-val');
    $('.flat-3d-btn').attr('href', $flatInput.val());
  };

  $('.flats-tabs__link').click(function () {
    $toggle.prop('checked', true);
    setLink();

    if ($('#tab-4').hasClass('active')) {
      $toggleLevel.prop('disabled', false);
    } else {
      $toggleLevel.prop('disabled', true);
    }
  });

  if ($container.find('.a-slider').length > 0) {
    $container.find('.a-slider').each(function (i, el) {
      var $slider = $(el);
      new Swiper($slider.find('.a-slider__container').get(0), {
        direction: 'horizontal',
        slidesPerView: 'auto',
        spaceBetween: 50,
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev'
        },
        breakpoints: {
          960: {
            spaceBetween: 15
          }
        }
      });
    });
  }

  if ($container.find('.b-slider').length > 0) {
    $container.find('.b-slider').each(function (i, el) {
      var $slider = $(el);
      new Swiper($slider.find('.b-slider__container').get(0), {
        direction: 'horizontal',
        slidesPerView: 'auto',
        pagination: {
          el: '.swiper-pagination',
          clickable: true
        }
      });
    });
  }

  if ($container.find('.h-slider').length > 0) {
    $container.find('.h-slider').each(function (i, el) {
      var $slider = $(el);
      new Swiper($slider.find('.h-slider__container').get(0), {
        direction: 'horizontal',
        slidesPerView: 'auto',
        noSwiping: true,
        allowSlidePrev: true,
        allowSlideNext: true,
        breakpoints: {
          678: {
            noSwiping: false,
            allowSlidePrev: false,
            allowSlideNext: false,
            autoplay: false,
            keyboard: false
          }
        }
      });
    });
  }

  if ($container.find('.flat-slider').length > 0) {
    $container.find('.flat-slider').each(function (i, el) {
      var $slider = $(el);
      var flatsSlider = new Swiper($slider.find('.flat-slider__container').get(0), {
        slidesPerView: 1,
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev'
        },
        pagination: {
          el: '.swiper-pagination',
          clickable: true
        }
      });
      setLink();
      flatsSlider.on('slideChangeTransitionEnd', function () {
        setLink();
      });
      flatsSlider.on('slideChange', function () {
        if ($toggle !== undefined) {
          $toggle.prop('checked', true);
        }
      });
    });
  }

  var galleryThumbs;

  if ($container.find('.gallery').length > 0) {
    $container.find('.gallery').each(function (i, el) {
      var $slider = $(el);
      galleryThumbs = new Swiper($slider.find('.gallery__thumbs').get(0), {
        slidesPerView: 2,
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true
      });
    });
  }

  if ($container.find('.gallery').length > 0) {
    $container.find('.gallery').each(function (i, el) {
      var $slider = $(el);
      new Swiper($slider.find('.gallery__top').get(0), {
        direction: 'horizontal',
        slidesPerView: 1,
        effect: 'fade',
        pagination: {
          el: '.swiper-pagination',
          clickable: true
        },
        thumbs: {
          swiper: galleryThumbs
        }
      });
    });
  }

  if ($container.find('.step-thumbs').length > 0) {
    $container.find('.step-thumbs').each(function (i, el) {
      var $slider = $(el);
      new Swiper($slider.find('.step-thumbs__container').get(0), {
        slidesPerView: 'auto',
        freeMode: true,
        watchSlidesVisibility: true,
        watchSlidesProgress: true,
        centeredSlides: true,
        slideToClickedSlide: true
      });
    });
  }

  if ($container.find('.map-box').length > 0) {
    var map = new google.maps.Map($('.map').get(0), {
      zoom: 16,
      center: {
        lat: 53.2039842,
        lng: 50.1121963
      },
      scrollwheel: false,
      disableDefaultUI: true,
      styles: [{
        featureType: 'all',
        elementType: 'labels.text.fill',
        stylers: [{
          saturation: 36
        }, {
          color: '#333333'
        }, {
          lightness: 40
        }]
      }, {
        featureType: 'all',
        elementType: 'labels.text.stroke',
        stylers: [{
          visibility: 'on'
        }, {
          color: '#ffffff'
        }, {
          lightness: 16
        }]
      }, {
        featureType: 'all',
        elementType: 'labels.icon',
        stylers: [{
          visibility: 'off'
        }]
      }, {
        featureType: 'administrative',
        elementType: 'geometry.fill',
        stylers: [{
          color: '#fefefe'
        }, {
          lightness: 20
        }]
      }, {
        featureType: 'administrative',
        elementType: 'geometry.stroke',
        stylers: [{
          color: '#fefefe'
        }, {
          lightness: 17
        }, {
          weight: 1.2
        }]
      }, {
        featureType: 'landscape',
        elementType: 'geometry',
        stylers: [{
          color: '#f5f5f5'
        }, {
          lightness: 20
        }]
      }, {
        featureType: 'poi',
        elementType: 'geometry',
        stylers: [{
          color: '#f5f5f5'
        }, {
          lightness: 21
        }]
      }, {
        featureType: 'poi.park',
        elementType: 'geometry',
        stylers: [{
          color: '#dedede'
        }, {
          lightness: 21
        }]
      }, {
        featureType: 'road.highway',
        elementType: 'geometry.fill',
        stylers: [{
          color: '#ffffff'
        }, {
          lightness: 17
        }]
      }, {
        featureType: 'road.highway',
        elementType: 'geometry.stroke',
        stylers: [{
          color: '#ffffff'
        }, {
          lightness: 29
        }, {
          weight: 0.2
        }]
      }, {
        featureType: 'road.arterial',
        elementType: 'geometry',
        stylers: [{
          color: '#ffffff'
        }, {
          lightness: 18
        }]
      }, {
        featureType: 'road.local',
        elementType: 'geometry',
        stylers: [{
          color: '#ffffff'
        }, {
          lightness: 16
        }]
      }, {
        featureType: 'transit',
        elementType: 'geometry',
        stylers: [{
          color: '#f2f2f2'
        }, {
          lightness: 19
        }]
      }, {
        featureType: 'water',
        elementType: 'geometry',
        stylers: [{
          color: '#e9e9e9'
        }, {
          lightness: 17
        }]
      }]
    });
    var food = [{
      lat: 53.2035635,
      lng: 50.1048914
    }, {
      lat: 53.2049802,
      lng: 50.1072963
    }, {
      lat: 53.2057382,
      lng: 50.1068783
    }, {
      lat: 53.2060177,
      lng: 50.1110244
    }, {
      lat: 53.2075732,
      lng: 50.1110817
    }, {
      lat: 53.2085112,
      lng: 50.1136863
    }, {
      lat: 53.2098162,
      lng: 50.1162173
    }, {
      lat: 53.2110502,
      lng: 50.1188783
    }, {
      lat: 53.2009772,
      lng: 50.1047823
    }, {
      lat: 53.1985922,
      lng: 50.1066163
    }, {
      lat: 53.1996125,
      lng: 50.1107709
    }, {
      lat: 53.2023402,
      lng: 50.1136153
    }, {
      lat: 53.2047372,
      lng: 50.1137731
    }, {
      lat: 53.2037912,
      lng: 50.1239953
    }, {
      lat: 53.2039642,
      lng: 50.1257973
    }];

    for (var i = 0; i < food.length; i++) {
      foodMarkers[i] = new google.maps.Marker({
        position: food[i],
        map: map,
        icon: 'images/food-pin.png'
      });
    }

    var toy = [{
      lat: 53.2016092,
      lng: 50.1075603
    }, {
      lat: 53.1977592,
      lng: 50.1120603
    }, {
      lat: 53.1987362,
      lng: 50.1143453
    }, {
      lat: 53.2049772,
      lng: 50.1196273
    }, {
      lat: 53.2051892,
      lng: 50.1179433
    }, {
      lat: 53.2056712,
      lng: 50.1183723
    }];

    for (var _i = 0; _i < toy.length; _i++) {
      toyMarkers[_i] = new google.maps.Marker({
        position: toy[_i],
        map: map,
        icon: 'images/toy-pin.png'
      });
    }

    var heart = [{
      lat: 53.2024237,
      lng: 50.1116425
    }, {
      lat: 53.2021812,
      lng: 50.1167473
    }, {
      lat: 53.2018899,
      lng: 50.122916
    }];

    for (var _i2 = 0; _i2 < heart.length; _i2++) {
      heartMarkers[_i2] = new google.maps.Marker({
        position: heart[_i2],
        map: map,
        icon: 'images/heart-pin.png'
      });
    }

    var book = [{
      lat: 53.2037542,
      lng: 50.1224153
    }, {
      lat: 53.2011862,
      lng: 50.1201423
    }, {
      lat: 53.2013992,
      lng: 50.1123093
    }];

    for (var _i3 = 0; _i3 < book.length; _i3++) {
      bookMarkers[_i3] = new google.maps.Marker({
        position: book[_i3],
        map: map,
        icon: 'images/book-pin.png'
      });
    }

    new google.maps.Marker({
      position: {
        lat: 53.2027116,
        lng: 50.1147171
      },
      map: map,
      icon: 'images/large-pin.png'
    });
    new google.maps.Marker({
      position: {
        lat: 53.2038782,
        lng: 50.1128153
      },
      map: map,
      icon: 'images/pin.png'
    });
  }

  $('body').on('click', '.btn-food', function (e) {
    e.preventDefault();
    $btnFood.addClass('map-info__btn--hidden');
    $btnFood.addClass('btn-food-hidden');

    for (var _i4 = 0; _i4 < foodMarkers.length; _i4++) {
      foodMarkers[_i4].setVisible(false);
    }
  }).on('click', '.btn-food-hidden', function (e) {
    e.preventDefault();
    $btnFood.removeClass('map-info__btn--hidden');
    $btnFood.removeClass('btn-food-hidden');

    for (var _i5 = 0; _i5 < foodMarkers.length; _i5++) {
      foodMarkers[_i5].setVisible(true);
    }
  });
  $('body').on('click', '.btn-toy', function (e) {
    e.preventDefault();
    $btnToy.addClass('map-info__btn--hidden');
    $btnToy.addClass('btn-toy-hidden');

    for (var _i6 = 0; _i6 < toyMarkers.length; _i6++) {
      toyMarkers[_i6].setVisible(false);
    }
  }).on('click', '.btn-toy-hidden', function (e) {
    e.preventDefault();
    $btnToy.removeClass('map-info__btn--hidden');
    $btnToy.removeClass('btn-toy-hidden');

    for (var _i7 = 0; _i7 < toyMarkers.length; _i7++) {
      toyMarkers[_i7].setVisible(true);
    }
  });
  $('body').on('click', '.btn-heart', function (e) {
    e.preventDefault();
    $btnHeart.addClass('map-info__btn--hidden');
    $btnHeart.addClass('btn-heart-hidden');

    for (var _i8 = 0; _i8 < heartMarkers.length; _i8++) {
      heartMarkers[_i8].setVisible(false);
    }
  }).on('click', '.btn-heart-hidden', function (e) {
    e.preventDefault();
    $btnHeart.removeClass('map-info__btn--hidden');
    $btnHeart.removeClass('btn-heart-hidden');

    for (var _i9 = 0; _i9 < heartMarkers.length; _i9++) {
      heartMarkers[_i9].setVisible(true);
    }
  });
  $('body').on('click', '.btn-book', function (e) {
    e.preventDefault();
    $btnBook.addClass('map-info__btn--hidden');
    $btnBook.addClass('btn-book-hidden');

    for (var _i10 = 0; _i10 < bookMarkers.length; _i10++) {
      bookMarkers[_i10].setVisible(false);
    }
  }).on('click', '.btn-book-hidden', function (e) {
    e.preventDefault();
    $btnBook.removeClass('map-info__btn--hidden');
    $btnBook.removeClass('btn-book-hidden');

    for (var _i11 = 0; _i11 < bookMarkers.length; _i11++) {
      bookMarkers[_i11].setVisible(true);
    }
  });
  jQuery(function () {
    $('.toggle__input').change(function () {
      if ($('.flats-content').hasClass('active')) {
        var $slide = $('.flats-content.active').find('.swiper-slide-active');
        var $imgF1 = $slide.find('.img-furniture-one');
        var $imgF2 = $slide.find('.img-furniture-two');
        var $imgE1 = $slide.find('.img-empty-one');
        var $imgE2 = $slide.find('.img-empty-two');
        $('.toggle__input').each(function () {
          if ($toggleFurniture[0].checked && $toggleLevel[0].checked) {
            $imgE1.css('display', 'none');
            $imgE2.css('display', 'none');
            $imgF1.css('display', 'inline-block');
            $imgF2.css('display', 'none');
          } else if (!$toggleFurniture[0].checked && $toggleLevel[0].checked) {
            $imgE1.css('display', 'inline-block');
            $imgE2.css('display', 'none');
            $imgF1.css('display', 'none');
            $imgF2.css('display', 'none');
          } else if ($toggleFurniture[0].checked && !$toggleLevel[0].checked) {
            $imgE1.css('display', 'none');
            $imgE2.css('display', 'none');
            $imgF1.css('display', 'none');
            $imgF2.css('display', 'inline-block');
          } else if (!$toggleFurniture[0].checked && !$toggleLevel[0].checked) {
            $imgE1.css('display', 'none');
            $imgE2.css('display', 'inline-block');
            $imgF1.css('display', 'none');
            $imgF2.css('display', 'none');
          }
        });
      }
    });
  });
});
$(function () {
  $('body').on('click', '.menu-btn', function (e) {
    e.preventDefault();
    $('.menu').addClass('menu--active');
    $('.menu-btn').addClass('menu-btn--close');
  }).on('click', '.menu-btn--close', function (e) {
    e.preventDefault();
    $('.menu').removeClass('menu--active');
    $('.menu-btn').removeClass('menu-btn--close');
  });
  var $header = $('.header');
  var $menu = $('.menu');
  $(document).mouseup(function (e) {
    if (!$header.is(e.target) && $header.has(e.target).length === 0) {
      $menu.removeClass('menu--active');
      $('.menu-btn').removeClass('menu-btn--close');
    }
  });
  $('body').on('click', '[data-scroll]', function (e) {
    e.preventDefault();
    $('.menu').removeClass('menu--active');
    $('.menu-btn').removeClass('menu-btn--close');
    var $link = $(e.currentTarget);
    var index = $link.data('scrollIndex');

    if (index === undefined) {
      index = 0;
    }

    var $el = $($link.data('scroll')).eq(index);
    $('html, body').animate({
      // eslint-disable-next-line no-magic-numbers
      scrollTop: $el.offset().top - $(window).height() * 1 / 100
    }, {
      duration: _animate__WEBPACK_IMPORTED_MODULE_4__["default"].duration.effect
    });
  });
  var $popupNews = $('.news-popup');
  var $popupBell = $('.popup-bell');
  var $popupFlat = $('.popup-flat');
  var $popupOffer = $('.popup-offer');
  $('.open-flat-btn').click(function () {
    $popupFlat.addClass('popup--active');
    $('body').css('overflow', 'hidden');
  });
  $('.popup-flat__close').click(function () {
    $popupFlat.removeClass('popup--active');
    $('body').css('overflow', '');
  });
  $('.open-offer-btn').click(function () {
    $popupOffer.addClass('popup--active');
    $('body').css('overflow', 'hidden');
  });
  $('.popup-offer__close').click(function () {
    $popupOffer.removeClass('popup--active');
    $('body').css('overflow', '');
  });
  $('.open-bell-btn').click(function (e) {
    $popupBell.addClass('popup--active');
    $('body').css('overflow', 'hidden');
  });
  $('.popup-bell__close').click(function () {
    $popupBell.removeClass('popup--active');
    $('body').css('overflow', '');
  });
  $('.send-request-btn').click(function (e) {
    e.preventDefault();
    var $button = $(e.currentTarget);
    var $check = $button.parents('.form').find('.checkbox__hidden-input');
    var $checkbox = $button.parents('.form').find('.checkbox');
    var $field = $button.parents('.form').find('.phone-field');
    var $fieldBox = $button.parents('.form').find('.phone-field-box');

    if ($field.val().length !== 16 && !$check.prop('checked')) {
      $fieldBox.addClass('field--error');
      $button.addClass('btn--red');
      $button.text('ВВЕДИТЕ НОМЕР ТЕЛЕФОНА');
      $field.attr('placeholder', 'Ошибка при вводе данных');
      $checkbox.addClass('checkbox--error');
    } else if ($field.val().length === 16 && !$check.prop('checked')) {
      $checkbox.addClass('checkbox--error');
      $fieldBox.removeClass('field--error');
      $button.removeClass('btn--red');
      $button.text('ОТПРАВИТЬ');
    } else if ($field.val().length !== 16 && $check.prop('checked')) {
      $checkbox.removeClass('checkbox--error');
      $fieldBox.addClass('field--error');
      $button.addClass('btn--red');
      $button.text('ВВЕДИТЕ НОМЕР ТЕЛЕФОНА');
      $field.attr('placeholder', 'Ошибка при вводе данных');
    } else if ($field.val().length === 16 && $check.prop('checked')) {
      $fieldBox.removeClass('field--error');
      $button.removeClass('btn--red');
      $button.addClass('btn--grey');
      $button.text('ОТПРАВЛЕНО');
      $button.prop('disabled', true);
      $checkbox.removeClass('checkbox--error');
    }
  });
  $('.load-more').click(function (e) {
    e.preventDefault();
    var $button = $(e.currentTarget);
    var $buttonText = $button.find('.load-more__text');
    var $spinner = $button.find('.spinner');
    $buttonText.css('display', 'none');
    $button.addClass('btn--ocher');
    $spinner.addClass('spinner--active');
    setTimeout(function () {
      $buttonText.css('display', 'inline-block');
      $button.removeClass('btn--ocher');
      $spinner.removeClass('spinner--active');
    }, 1000);
  });
  $('body').on('click', '.mobile-dropdown', function (e) {
    e.preventDefault();
    var $button = $(e.currentTarget);
    var $dropdown = $button.parents('.h-slider').find('.d-tabs-nav');
    $button.addClass('mobile-dropdown--open');
    $dropdown.addClass('d-tabs-nav--active');
  }).on('click', '.mobile-dropdown--open', function (e) {
    e.preventDefault();
    var $button = $(e.currentTarget);
    var $dropdown = $button.parents('.h-slider').find('.d-tabs-nav');
    $button.removeClass('mobile-dropdown--open');
    $dropdown.removeClass('d-tabs-nav--active');
  }).on('click', '.flats-tabs__link', function (e) {
    e.preventDefault();
    var $button = $(e.currentTarget);
    var $type = $button.parents('.flats-tabs__item').find('.flats-tabs__type');
    var $area = $button.parents('.flats-tabs__item').find('.flats-tabs__area');
    var $typeD = $button.parents('.h-slider').find('.mobile-dropdown__type');
    var $areaD = $button.parents('.h-slider').find('.mobile-dropdown__area');
    var $dropdown = $button.parents('.h-slider').find('.d-tabs-nav');
    $dropdown.removeClass('d-tabs-nav--active');
    $typeD.text($type.text());
    $areaD.text($area.text());
  }).on('click', '.docs-tabs__link', function (e) {
    e.preventDefault();
    var $button = $(e.currentTarget);
    var $typeD = $button.parents('.h-slider').find('.mobile-dropdown__text');
    var $dropdown = $button.parents('.h-slider').find('.d-tabs-nav');
    $dropdown.removeClass('d-tabs-nav--active');
    $typeD.text($button.text());
  });
  $('.news-slider__item').click(function () {
    $popupNews.addClass('news-popup--active');
    $('body').css('overflow', 'hidden');
  });
  $('.news-popup__close').click(function () {
    $popupNews.removeClass('news-popup--active');
    $('body').css('overflow', '');
  });

  if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
    $('.video').attr('controls', true);
    $('.step-thumbs').addClass('step-thumbs--mobile');
    $('body').on('click', '.js-watch-video-btn', function (e) {
      e.preventDefault();
      var $button = $(e.currentTarget);
      var $videoBack = $button.parents('.video').find('.video__back');
      var $movieBox = $button.parents('.video').find('.video__inner');
      var $closeBtn = $button.parents('.video').find('.video__close-btn');
      var $videoBox = $button.parents('.video').find('.video__movie');
      var $video = $videoBox.get(0);
      $video.controls = true;
      $videoBack.css('display', 'none');
      $closeBtn.css('display', 'none');
      $movieBox.addClass('video__inner--active');
      $video.play();
    }).on('click', '.js-pause-video-btn', function (e) {
      e.preventDefault();
      var $button = $(e.currentTarget);
      var $videoBox = $button.parents('.video').find('.video__movie');
      var $video = $videoBox.get(0);

      if ($video.paused) {
        $video.play();
        $button.text('ПАУЗА');
        $button.css('opacity', '0');
      } else {
        $video.pause();
        $button.text('СМОТРЕТЬ');
        $button.css('opacity', '1');
      }
    });
  } else {
    $('body').on('click', '.js-watch-video-btn', function (e) {
      e.preventDefault();
      var $button = $(e.currentTarget);
      var $movieBox = $button.parents('.video').find('.video__inner');
      var $movie = $movieBox.get(0);
      var $videoBox = $button.parents('.video').find('.video__movie');
      var $video = $videoBox.get(0);
      $movieBox.addClass('video__inner--active');
      $video.play();

      if ($movie.requestFullscreen) {
        $movie.requestFullscreen();
      } else if ($movie.mozRequestFullScreen) {
        $movie.mozRequestFullScreen();
      } else if ($movie.webkitRequestFullscreen) {
        $movie.webkitRequestFullscreen();
      } else if ($movie.msRequestFullscreen) {
        $movie.msRequestFullscreen();
      }
    }).on('click', '.js-pause-video-btn', function (e) {
      e.preventDefault();
      var $button = $(e.currentTarget);
      var $videoBox = $button.parents('.video').find('.video__movie');
      var $video = $videoBox.get(0);

      if ($video.paused) {
        $video.play();
        $button.text('ПАУЗА');
        $button.css('opacity', '');
      } else {
        $video.pause();
        $button.text('СМОТРЕТЬ');
        $button.css('opacity', '1');
      }
    }).on('click', '.js-close-video-btn', function (e) {
      e.preventDefault();
      var $button = $(e.currentTarget);
      var $movie = $button.parents('.video').find('.video__inner');
      var $movieBtn = $button.parents('.video').find('.video__btn');

      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.webkitExitFullscreen) {
        document.webkitExitFullscreen();
      } else if (document.mozCancelFullScreen) {
        document.mozCancelFullScreen();
      } else if (document.msExitFullscreen) {
        document.msExitFullscreen();
      }

      $movie.removeClass('video__inner--active');
      $movieBtn.removeClass('video__btn--pause');
      $movieBtn.text('СМОТРЕТЬ');
    });
    document.addEventListener('fullscreenchange', exitHandler);
    document.addEventListener('webkitfullscreenchange', exitHandler);
    document.addEventListener('mozfullscreenchange', exitHandler);
    document.addEventListener('MSFullscreenChange', exitHandler);

    function exitHandler() {
      if (!document.fullscreenElement && !document.webkitIsFullScreen && !document.mozFullScreen && !document.msFullscreenElement) {
        $('.video__inner').removeClass('video__inner--active');
        $('.video__btn').removeClass('video__btn--pause');
        $('.video__btn').text('СМОТРЕТЬ');
      }
    }
  } // IE11 detection


  if (/MSIE/.test(window.navigator.userAgent) || /Trident/.test(window.navigator.userAgent)) {
    $('body').addClass('msie');
  } // Polyfills


  object_fit_images__WEBPACK_IMPORTED_MODULE_2___default()(); // https://www.thecssninja.com/javascript/pointer-events-60fps

  var peTimer;
  $(window).on('scroll', function () {
    if (peTimer) {
      clearTimeout(peTimer);
      peTimer = undefined;
    }

    var $body = $('body');

    if (!$body.hasClass('disable-hover')) {
      $body.addClass('disable-hover');
    }

    peTimer = setTimeout(function () {
      $body.removeClass('disable-hover');
    }, 100);
  }); // Маска для номеров телефонов

  $('input[type="tel"]').each(function (i, el) {
    new IMask(el, {
      mask: i18n.masks.tel
    });
  });
});

/***/ }),

/***/ 0:
/*!*******************************!*\
  !*** multi ./scripts/main.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./scripts/main.js */"./scripts/main.js");


/***/ })

/******/ });
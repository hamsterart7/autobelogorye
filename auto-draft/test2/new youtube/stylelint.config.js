module.exports = {
	plugins: ['stylelint-order'],
	rules: {
		'color-hex-length': 'long',
		'color-no-invalid-hex': true,
		'declaration-block-no-duplicate-properties': true,
		'declaration-no-important': true,
		'font-family-no-duplicate-names': true,
		'font-family-no-missing-generic-family-keyword': null,
		'font-weight-notation': 'numeric',
		'function-calc-no-unspaced-operator': true,
		'function-linear-gradient-no-nonstandard-direction': true,
		'function-url-quotes': 'always',
		'no-duplicate-at-import-rules': true,
		'no-duplicate-selectors': true,
		'no-extra-semicolons': true,
		'no-invalid-double-slash-comments': true,
		'order/order': [
			{
				type: 'at-rule',
				name: 'mixin',
			},
			'custom-properties',
			'declarations',
			'at-rules',
			'rules',
		],
		'order/properties-alphabetical-order': true,
		'property-blacklist': [
			'animation',
			'background',
			'border',
			'border-color',
			'border-style',
			'border-width',
			'border-bottom',
			'border-left',
			'border-right',
			'border-top',
			'columns',
			'flex',
			'font',
			'gap',
			'grid',
			'grid-column',
			'grid-row',
			'list-style',
			'margin',
			'padding',
			'transition',
		],
		'selector-max-universal': 0,
		'selector-pseudo-element-colon-notation': 'double',
		'string-no-newline': true,
	},
};

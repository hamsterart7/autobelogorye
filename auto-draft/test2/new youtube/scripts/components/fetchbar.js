/* global $ */

import anime from 'animejs';

$(() => {
	const $fetchbar = $('.fetchbar');
	let timer;
	let animation;
	const timeout = 100;

	$(document)
		.ajaxStart(() => {
			timer = setTimeout(() => {
				$fetchbar.prop('hidden', false);
				animation = anime({
					targets: $fetchbar.get(0),
					translateX: ['0%', '100%'],
					easing: 'cubicBezier(0, 0, 0, 1)',
					duration: 5000,
				});
			}, timeout);
		})
		.ajaxStop(() => {
			if (timer) {
				clearTimeout(timer);
				timer = undefined;
			}
			if (animation) {
				animation.seek(animation.duration);
				animation = undefined;
				setTimeout(() => {
					$fetchbar.prop('hidden', true);
				}, timeout);
			}
		});
});

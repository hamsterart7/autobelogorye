const cp = require('child_process');
const path = require('path');
const chalk = require('chalk');
const cheerio = require('cheerio');
const csso = require('csso');
const eslint = require('eslint');
const git = require('simple-git/promise');
const mozjpeg = require('mozjpeg');
const nunjucks = require('nunjucks');
const pngquant = require('pngquant-bin');
const postcss = require('postcss');
const prettier = require('prettier');
const Promise = require('bluebird');
const sharp = require('sharp');
const stylelint = require('stylelint');
const SVGSpriter = require('svg-sprite');
const vnu = require('vnu-jar');
const webpack = require('webpack');
const cpAsync = Promise.promisifyAll(cp);
const nunjucksAsync = Promise.promisifyAll(nunjucks);

module.exports = (grunt) => {
	grunt.initConfig({
		clean: {
			dist: {
				src: ['www/*.html', 'www/scripts/**/*.*', 'www/styles/**/*.*'],
			},
			icons: {
				src: ['www/*.png'],
			},
		},
		prettier: {
			json: {
				files: [
					{
						expand: true,
						src: ['*.json', '.babelrc', '.eslintrc', 'www/manifest.webmanifest'],
					},
				],
				options: {
					parser: 'json',
				},
			},
			scripts: {
				files: [
					{
						expand: true,
						src: ['*.js', 'scripts/**/*.js'],
					},
				],
				options: {
					parser: 'babel',
				},
			},
			styles: {
				files: [
					{
						expand: true,
						src: ['styles/**/*.css'],
					},
				],
				options: {
					parser: 'css',
				},
			},
		},
		eslint: {
			files: {
				expand: true,
				src: ['*.js', 'scripts/**/*.js'],
			},
		},
		stylelint: {
			files: {
				expand: true,
				src: ['styles/**/*.css'],
			},
			options: {
				formatter: 'verbose',
				fix: true,
			},
		},
		postcss: {
			files: {
				expand: true,
				cwd: 'styles/',
				src: ['main.css'],
				dest: 'www/styles',
			},
		},
		csso: {
			files: {
				expand: true,
				cwd: 'www/styles/',
				src: ['*.css'],
				dest: 'www/styles',
			},
		},
		nunjucks: {
			files: {
				expand: true,
				cwd: 'templates/',
				src: ['*.html', '!layout.html'],
				dest: 'www',
				ext: '.html',
			},
			options: {
				noCache: true,
			},
		},
		icons: {
			files: {
				expand: true,
				cwd: 'styles/icons',
				src: ['**/*.svg'],
			},
			options: {
				defaultCollection: 'icons',
				spriter: {
					mode: {
						symbol: true,
					},
				},
				dest: 'www/styles/icons',
			},
		},
		images: {
			files: {
				expand: true,
				cwd: 'styles/images',
				src: ['**/*.{gif,jpg,png,svg}'],
				dest: 'www/styles/images',
			},
		},
		fonts: {
			files: {
				expand: true,
				cwd: 'styles/fonts',
				src: ['**/*.{woff,woff2}'],
				dest: 'www/styles/fonts',
			},
		},
		watch: {
			options: {
				spawn: false,
			},
			scripts: {
				files: ['scripts/**/*.js'],
				tasks: ['webpack'],
			},
			styles: {
				files: ['styles/**/*.css'],
				tasks: ['postcss'],
			},
			templates: {
				files: ['templates/**/*.html'],
				tasks: ['nunjucks'],
			},
			icons: {
				files: ['styles/icons/*.svg'],
				tasks: ['icons'],
			},
			images: {
				files: ['styles/images/**/*'],
				tasks: ['images'],
			},
			fonts: {
				files: ['styles/fonts/**/*'],
				tasks: ['fonts'],
			},
		},
		browserSync: {
			dev: {
				bsFiles: {
					src: ['www/scripts/**/*.js', 'www/styles/**/*.css', 'www/**/*.html'],
				},
				options: {
					server: {
						baseDir: './www',
						index: 'index.html',
					},
					ghostMode: false,
					watchTask: true,
				},
			},
		},
	});

	grunt.loadNpmTasks('grunt-contrib-clean');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-browser-sync');

	grunt.registerMultiTask('prettier', function() {
		const options = Object.assign(require('./prettier.config.js'), this.options());
		this.files.forEach((file) => {
			const code = grunt.file.read(file.src[0]);
			const formatted = prettier.format(code, options);
			grunt.file.write(file.dest, formatted);
		});
	});

	grunt.registerMultiTask('eslint', function() {
		const files = this.files.map((file) => {
			return file.src[0];
		});
		const cli = new eslint.CLIEngine({
			fix: true,
		});
		const report = cli.executeOnFiles(files);
		eslint.CLIEngine.outputFixes(report);
		const results = cli.getFormatter()(report.results);
		if (report.errorCount > 0) {
			grunt.fail.warn(results);
		} else {
			grunt.log.ok(results);
		}
	});

	grunt.registerTask('webpack', function() {
		const done = this.async();
		const compiler = webpack(require('./webpack.config.js'));
		compiler.run((err, result) => {
			grunt.log.ok(result);
			done();
		});
	});

	grunt.registerMultiTask('stylelint', function() {
		const done = this.async();
		const files = this.files.map((file) => {
			return file.src[0];
		});
		stylelint
			.lint(Object.assign({files}, this.options()))
			.then((result) => {
				if (result.errored) {
					grunt.fail.warn(result.output);
				}
				return done();
			})
			.catch((err) => {
				grunt.fail.warn(err);
			});
	});

	grunt.registerMultiTask('postcss', function() {
		const done = this.async();
		Promise.map(this.files, (file) => {
			const css = grunt.file.read(file.src[0]);
			return postcss(require('./postcss.config.js').plugins).process(css, {
				from: file.src[0],
				to: file.dest,
			});
		})
			.then((result) => {
				result.forEach((file) => {
					grunt.file.write(file.opts.to, file.css);
				});
				done();
			})
			.catch((err) => {
				grunt.fail.warn(err);
			});
	});

	grunt.registerMultiTask('csso', function() {
		this.files.forEach((file) => {
			const code = grunt.file.read(file.src[0]);
			const minified = csso.minify(code);
			grunt.file.write(file.dest, minified.css);
		});
	});

	grunt.registerMultiTask('nunjucks', function() {
		const done = this.async();
		nunjucks.configure(this.options());
		Promise.map(this.files, (file) => {
			return nunjucksAsync
				.renderAsync(file.src[0])
				.then((result) => {
					grunt.file.write(file.dest, result);
				})
				.catch((err) => {
					grunt.fail.warn(err);
				});
		})
			.then(() => {
				done();
			})
			.catch((err) => {
				grunt.fail.warn(err);
			});
	});

	grunt.registerTask('validator', function() {
		const done = this.async();
		const dir = path.join(__dirname, 'www');
		const config = require('./validator.config.js');
		// eslint-disable-next-line no-magic-numbers
		const maxBuffer = 1024 * 2000;
		cp.exec(
			`java -jar ${vnu} --exit-zero-always --format json --skip-non-html ${dir}`,
			{maxBuffer},
			(err, stdout, stderr) => {
				if (err) {
					grunt.fail.fatal(err);
				} else if (stderr) {
					let reporter = '\n';
					let isErrors = false;
					// Формируем отчёт
					JSON.parse(stderr).messages.forEach((item) => {
						let ignore = false;
						config.ignore.forEach((rule) => {
							if (new RegExp(rule).test(item.message)) {
								ignore = true;
							}
						});
						if (!ignore) {
							// Тип сообщения
							let type;
							switch (item.type) {
								case 'info':
									type = chalk.yellow(item.type);
									break;
								case 'error':
									type = chalk.red(item.type);
									isErrors = true;
									break;
								default:
									type = chalk.blue(item.type);
							}
							// Имя файла
							const file = /\/([a-z0-9_-]+\.html)$/.exec(item.url)[1];
							// Строка
							let line = item.firstLine;
							if (line === undefined) {
								line = item.lastLine;
							}
							// Позиция в строке
							const column = item.firstColumn;
							// Добавляем строку в отчёт
							reporter += `${type} ${file} ${line}:${column} ${item.message}\n`;
						}
					});
					if (isErrors) {
						grunt.fail.warn(reporter);
					} else {
						grunt.log.write(reporter);
						done();
					}
				} else {
					done();
				}
			}
		);
	});

	grunt.registerMultiTask('icons', function() {
		const done = this.async();
		const options = this.options();
		const svgSpriterAsync = (files, options) => {
			return new Promise((resolve, reject) => {
				const spriter = new SVGSpriter(options);
				files.forEach((file) => {
					spriter.add(path.join(__dirname, file.src[0]), null, grunt.file.read(file.src[0]));
				});
				spriter.compile((err, result) => {
					if (err) {
						return reject(err);
					}
					return resolve(result);
				});
			});
		};
		const collections = {};
		this.files.forEach((file) => {
			let collection = options.defaultCollection;
			if (file.dest.split('/').length > 1) {
				collection = file.dest.split('/')[0];
			}
			if (collections[collection] === undefined) {
				collections[collection] = [];
			}
			collections[collection].push(file);
		});
		Promise.each(Object.keys(collections), (collection) => {
			return svgSpriterAsync(collections[collection], options.spriter).then((result) => {
				grunt.file.write(path.join(options.dest, `${collection}.svg`), result.symbol.sprite.contents);
			});
		}).then(() => {
			done();
		});
	});

	grunt.registerMultiTask('images', function() {
		const done = this.async();
		Promise.map(this.files, (file) => {
			if (process.env.NODE_ENV === 'production') {
				if (/\.jpg$/.test(file.src[0])) {
					return cpAsync.execFileAsync(mozjpeg, [
						'-outfile',
						path.join(__dirname, file.dest),
						path.join(__dirname, file.src[0]),
					]);
				}
				if (/\.png$/.test(file.src[0])) {
					return cpAsync.execFileAsync(pngquant, [
						'--strip',
						'-o',
						path.join(__dirname, file.dest),
						path.join(__dirname, file.src[0]),
					]);
				}
				if (/\.svg$/.test(file.src[0])) {
					return cpAsync.execFileAsync('./node_modules/.bin/svgo', [
						'-i',
						path.join(__dirname, file.src[0]),
						'-o',
						path.join(__dirname, file.dest),
					]);
				}
				grunt.file.copy(file.src[0], file.dest);
			}
			grunt.file.copy(file.src[0], file.dest);
		})
			.then(() => {
				done();
			})
			.catch((err) => {
				grunt.fail.warn(err);
			});
	});

	grunt.registerMultiTask('fonts', function() {
		this.files.forEach((file) => {
			grunt.file.copy(file.src[0], file.dest);
		});
	});

	grunt.registerTask('favicon', function() {
		if (grunt.file.exists('favicon.png')) {
			const done = this.async();
			const favicon = grunt.file.read('favicon.png', {encoding: null});
			const files = [
				{
					name: 'favicon-16x16.png',
					size: 16,
				},
				{
					name: 'favicon-32x32.png',
					size: 32,
				},
				{
					name: 'favicon-192x192.png',
					size: 192,
				},
				{
					name: 'favicon-512x512.png',
					size: 512,
				},
				{
					name: 'apple-touch-icon.png',
					size: 180,
				},
			];
			Promise.each(files, (file) => {
				return sharp(favicon)
					.resize(file.size)
					.toFile(path.join(__dirname, file.name));
			})
				.then(() => {
					return Promise.each(files, (file) => {
						return cpAsync.execFileAsync(pngquant, [
							'--strip',
							'-o',
							path.join(__dirname, 'www', file.name),
							path.join(__dirname, file.name),
						]);
					});
				})
				.then(() => {
					files.forEach((file) => {
						grunt.file.delete(file.name);
					});
					const $ = cheerio.load(grunt.file.read('www/index.html'));
					const manifest = grunt.file.readJSON('www/manifest.webmanifest');
					manifest.name = $('title').text();
					grunt.file.write('www/manifest.webmanifest', JSON.stringify(manifest));
					done();
				});
		} else {
			grunt.fail.warn('Не найден favicon.png');
		}
	});

	grunt.registerTask('og', function() {
		if (grunt.file.exists('og.png')) {
			const done = this.async();
			cpAsync
				.execFileAsync(pngquant, [
					'--strip',
					'-o',
					path.join(__dirname, 'www/og.png'),
					path.join(__dirname, 'og.png'),
				])
				.then(() => {
					done();
				});
		} else {
			grunt.fail.warn('Не найден og.png');
		}
	});

	grunt.registerTask('resize', function(image, sizes) {
		const done = this.async();
		sizes = sizes.split(',');
		const fileinfo = path.parse(image);
		image = grunt.file.read(image, {encoding: null});
		Promise.each(sizes, (size) => {
			const filename = `${fileinfo.name}_${size}px${fileinfo.ext}`;
			return sharp(image)
				.resize(Number(size))
				.toFile(path.join(__dirname, fileinfo.dir, filename));
		}).then(() => {
			done();
		});
	});

	grunt.registerTask('build', function() {
		grunt.task.run([
			'clean:dist',
			'clean:icons',
			'eslint',
			'webpack',
			'stylelint',
			'postcss',
			'csso',
			'nunjucks',
			'validator',
			'icons',
			'images',
			'fonts',
			'favicon',
			'og',
			'prettier',
		]);
	});

	grunt.registerTask('dev', function() {
		const done = this.async();
		git()
			.branchLocal()
			.then((result) => {
				if (result.branches.master !== undefined && result.branches.master.current) {
					grunt.fail.warn('Нельзя разрабатывать на master-ветке');
				} else {
					grunt.task.run([
						'clean:dist',
						'webpack',
						'postcss',
						'nunjucks',
						'icons',
						'images',
						'fonts',
						'browserSync',
						'watch',
					]);
					done();
				}
			});
	});
};

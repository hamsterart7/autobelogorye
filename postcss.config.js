module.exports = {
	plugins: [
		require('postcss-easy-import'),
		require('postcss-nested'),
		require('postcss-object-fit-images'),
		require('postcss-preset-env')({
			stage: 0,
			features: {
				'color-mod-function': true,
			},
		}),
	],
};

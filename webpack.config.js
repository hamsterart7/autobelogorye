const path = require('path');

let devtool = false;
if (process.env.NODE_ENV === 'production') {
	devtool = 'source-map';
}

module.exports = {
	mode: process.env.NODE_ENV,
	entry: ['./scripts/main.js'],
	module: {
		rules: [
			{
				test: /\.js$/,
				exclude: /node_modules\/(?!(dom7|ssr-window|swiper)\/).*/,
				use: [
					{
						loader: 'babel-loader',
						options: {
							presets: ['@babel/preset-env'],
						},
					},
				],
			},
		],
	},
	output: {
		filename: '[name].js',
		path: path.resolve(__dirname, 'www/scripts'),
	},
	devtool,
	performance: {
		hints: false,
	},
};

/* global $ */

import animate from './animate.js';

export default ($el) => {
	$('html, body').animate(
		{
			// eslint-disable-next-line no-magic-numbers
			scrollTop: $el.offset().top - ($(window).height() * 10) / 100,
		},
		{
			duration: animate.duration.effect,
		}
	);
};

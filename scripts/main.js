/* global $, i18n */

import 'formdata-polyfill';
import 'svgxuse';
import IMask from 'imask';
import objectFitImages from 'object-fit-images';

import './lazyload.js';

// Подключаем все модули из ./components
const importAll = (r) => {
	r.keys().forEach(r);
};
importAll(require.context('./components', false, /\.js$/));

$(() => {
	// IE11 detection
	if (/MSIE/.test(window.navigator.userAgent) || /Trident/.test(window.navigator.userAgent)) {
		$('body').addClass('msie');
	}

	// Polyfills
	objectFitImages();

	// https://www.thecssninja.com/javascript/pointer-events-60fps
	let peTimer;
	$(window).on('scroll', () => {
		if (peTimer) {
			clearTimeout(peTimer);
			peTimer = undefined;
		}
		const $body = $('body');
		if (!$body.hasClass('disable-hover')) {
			$body.addClass('disable-hover');
		}
		peTimer = setTimeout(() => {
			$body.removeClass('disable-hover');
		}, 100);
	});

	// Маска для номеров телефонов
	$('input[type="tel"]').each((i, el) => {
		new IMask(el, {
			mask: i18n.masks.tel,
		});
	});

	// Маска для браузеров, не поддерживающих поля дат
	$('input[type="date"]').each((i, el) => {
		const $el = $(el);
		if (el.type !== 'date') {
			$el.attr('placeholder', new Intl.DateTimeFormat().format(new Date()));
			new IMask(el, {
				mask: Date,
				min: new Date(el.min),
				max: new Date(el.max),
			});
		}
	});
});

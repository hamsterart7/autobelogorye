module.exports = {
	ignore: [
		// Правила шаблона
		'^Attribute “v-',
		'^Bad value “.+” for attribute “media” on element “source”',
		'Element “img” is missing required attribute “src”.',
		'Element “source” is missing required attribute “srcset”.',
		'The “date” input type is not supported in all browsers. Please be sure to test, and consider using a polyfill.',
		// Правила проекта
	],
};
